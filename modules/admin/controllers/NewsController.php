<?php

namespace app\modules\admin\controllers;

use app\controllers\ContentController;
use app\models\NewsImages;
use app\models\ProductImage;
use app\models\Subscription;
use app\models\User;
use Yii;
use app\models\News;
use app\models\search\NewsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends BackendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $images = NewsImages::findAll(['news_id' => $id]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'images' => $images
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new News();
        $images = new NewsImages();
        if ($model->load(Yii::$app->request->post())) {
            if($model->date == null) $model->date = date('Y-m-d');
            $image = UploadedFile::getInstance($model, 'image');
            if($image != null) {
                $time = time();
                $image->saveAs($model->path . $time . '_' . $image->baseName . '.' . $image->extension);
                $model->image = $time . '_' . $image->baseName . '.' . $image->extension;
            }
            if($model->save()) {

                // UPLOAD IMAGES
                if ($images->load(Yii::$app->request->post())) {
                    $file = UploadedFile::getInstances($images, 'image');
                    foreach ($file as $v) {
                        $images = new NewsImages();
                        $time = time();
                        $v->saveAs($images->path . $time . '_' . $v->baseName . '.' . $v->extension);
                        $images->image = $time . '_' . $v->baseName . '.' . $v->extension;
                        $images->news_id = $model->id;
                        $images->save(false);
                    }
                }
                // END UPLOAD IMAGES


                return $this->redirect(['view', 'id' => $model->id]);
            }

        }

        return $this->render('create', [
            'model' => $model,
            'images' => $images
        ]);
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $images = new NewsImages();
        $oldImage = $model->image;

        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            if($image == null){
                $model->image = $oldImage;
            }else{
                $time = time();
                $image->saveAs($model->path . $time . '_' . $image->baseName . '.' . $image->extension);
                $model->image = $time . '_' . $image->baseName . '.' . $image->extension;
                if(!($oldImage == null)){
                    unlink($model->path . $oldImage);
                }
            }
            if($model->save()) {
                // UPLOAD IMAGES
                if ($images->load(Yii::$app->request->post())) {
                    $file = UploadedFile::getInstances($images, 'image');
                    if ($file != null) {
                        NewsImages::deleteImages($model->id);
                        foreach ($file as $v) {
                            $images = new NewsImages();
                            $time = time();
                            $v->saveAs($images->path . $time . '_' . $v->baseName . '.' . $v->extension);
                            $images->image = $time . '_' . $v->baseName . '.' . $v->extension;
                            $images->news_id = $model->id;
                            $images->save(false);
                        }
                    }
                }
                // END UPLOAD IMAGES
            }


            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'images' => $images
        ]);
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionSendAll($id)
    {
        $model = $this->findModel($id);
        $this->SendAllNotification($model);

        return $this->redirect('index');
    }

    private function SendAllNotification($model){
        $subscription = Subscription::getAll();
        $users = User::find()->where('id > 1')->all();
        $type = $model->type == 1 ? "news":"article";
        $url = Yii::$app->request->hostInfo.'/'.$type.'/view?id='.$model->id;
        $check = array();
        foreach ($subscription as $v){
            array_push($check, $v->email);
            $content = ContentController::cutStr($model->content,800);
            $this->sendToSubscribes($v->email,$model->name,$content,$url);
        }

        foreach ($users as $v){
            if(!in_array($v->email, $check)) {
                $content = ContentController::cutStr($model->content, 800);
                $this->sendToSubscribes($v->email, $model->name, $content, $url);
            }
        }
    }



    private function sendToSubscribes($email, $title, $content, $url){
        $emailSend = Yii::$app->mailer->compose()
            ->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->name . ' robot'])
            ->setTo($email)
            ->setSubject($title)

            ->setHtmlBody("<p>$content</p>
                             <a href='$url'>Посмотреть все</a>");
        return $emailSend->send();

    }



}
