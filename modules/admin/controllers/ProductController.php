<?php

namespace app\modules\admin\controllers;

use app\models\Catalog;
use app\models\Color;
use app\models\ProductAlsoBuys;
use app\models\ProductImage;
use app\models\ProductQuantity;
use app\models\ProductRecomend;
use app\models\Size;
use Yii;
use app\models\Product;
use app\models\search\ProductSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends BackendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $images = ProductImage::findAll(['product_id' => $id]);
        $alsoBuys_products = ProductAlsoBuys::getAvailableProductsAsArray($id);
        $recomend_products = ProductRecomend::getAvailableProductsAsArray($id);
        $quantity = ProductQuantity::findAll(['product_id' => $id]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'images' => $images,
            'alsoBuys_products' => $alsoBuys_products,
            'recomend_products' => $recomend_products,
            'quantity' => $quantity,
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();
        $alsoBuys = new ProductAlsoBuys();
        $recomend = new ProductRecomend();
        $qua  = new ProductQuantity();

        $alsoBuys_products = array();
        $recomend_products = array();

        $products = Product::getAll();

        if ($model->load(Yii::$app->request->post())) {

            // CHECK QUANTITY PRODUCTS
            if($qua->load(Yii::$app->request->post())){
                if($_SESSION['quantity_product'] == null){
                    Yii::$app->session->setFlash('quantity_error','Необходимо указать количества товара.');
                    return $this->render('create', compact('model','products','alsoBuys_products',
                        'recomend_products','color','size','quantity'));
                }
            }
            // END CHECK QUANTITY PRODUCTS

            $image = UploadedFile::getInstance($model, 'image');
            if($image != null) {
                $time = time();
                $image->saveAs($model->path . $time . '_' . $image->baseName . '.' . $image->extension);
                $model->image = $time . '_' . $image->baseName . '.' . $image->extension;
            }

            if($model->save()) {

                // ALSO BUYS PRODUCTS
                if ($alsoBuys->load(Yii::$app->request->post())) {
                    if ($alsoBuys->also_buys_product_id != null) {
                        $products = $alsoBuys->also_buys_product_id;
                        foreach ($products as $v) {
                            $product = new ProductAlsoBuys();
                            $product->product_id = $model->id;
                            $product->also_buys_product_id = $v;
                            $product->save(false);
                        }
                    }
                }
                //END ALSO BUYS PRODUCTS


                // RECOMMENDED PRODUCTS
                if ($recomend->load(Yii::$app->request->post())) {
                    if ($recomend->recoment_product_id != null) {
                        $products = $recomend->recoment_product_id;
                        foreach ($products as $v) {
                            $product = new ProductRecomend();
                            $product->product_id = $model->id;
                            $product->recoment_product_id = $v;
                            $product->save(false);
                        }
                    }
                }
                //END RECOMMENDED PRODUCTS


                // ADD QUANTITY PRODUCTS
                foreach ($_SESSION['quantity_product'] as $v) {
                    if ($v['size'] != null) {
                        $sizes = explode(',', $v['size']);
                        foreach ($sizes as $size) {
                            $quantity = new ProductQuantity();
                            $quantity->product_id = $model->id;
                            if ($v["color"] != null) $quantity->color_id = $v["color"];
                            $quantity->size_id = $size;
                            $quantity->quantity = $v['quantity'];
                            $quantity->save(false);
                        }
                    } else {
                        $quantity = new ProductQuantity();
                        $quantity->product_id = $model->id;
                        if ($v["color"] != null) $quantity->color_id = $v["color"];
                        $quantity->quantity = $v['quantity'];
                        $quantity->save(false);
                    }
                }
                unset($_SESSION['quantity_product']);
                // END ADD QUANTITY PRODUCTS

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        $_SESSION['quantity_product'] = array();

        return $this->render('create', compact('model','products','alsoBuys_products',
            'recomend_products','color','size','quantity'));
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $alsoBuys = new ProductAlsoBuys();
        $recomend = new ProductRecomend();
        $qua  = new ProductQuantity();

        $products = Product::find()->where('id != '.$id)->all();
        $alsoBuys_products = ProductAlsoBuys::getAvailableProductsAsArray($id);
        $recomend_products = ProductRecomend::getAvailableProductsAsArray($id);

        $color = Color::getAll();
        $size = Size::getAllByCatalog($model->catalog->lastParent->id);

        // CHECK QUANTITY PRODUCTS
        if($qua->load(Yii::$app->request->post())){
        $s = Product::findAll([$id]);
            if($_SESSION['quantity_product'] == null || $s == null){
                Yii::$app->session->setFlash('quantity_error','Необходимо указать количества товара.');
                return $this->render('update', compact('model','products','alsoBuys_products',
                    'recomend_products','color','size','quantity'));
            }
        }
        // END CHECK QUANTITY PRODUCTS



        $oldImage = $model->image;
        if ($model->load(Yii::$app->request->post())) {

            $image = UploadedFile::getInstance($model, 'image');
            if($image == null){
                $model->image = $oldImage;
            }else{
                $time = time();
                $image->saveAs($model->path . $time . '_' . $image->baseName . '.' . $image->extension);
                $model->image = $time . '_' . $image->baseName . '.' . $image->extension;
                if(!($oldImage == null)){
                    unlink($model->path . $oldImage);
                }
            }
            if($model->save()) {

                // ALSO BUYS PRODUCTS
                if ($alsoBuys->load(Yii::$app->request->post())) {
                    if ($alsoBuys->also_buys_product_id != null) {
                        ProductAlsoBuys::deleteAll(['product_id' => $model->id]);
                        $products = $alsoBuys->also_buys_product_id;
                        foreach ($products as $v) {
                            $product = new ProductAlsoBuys();
                            $product->product_id = $model->id;
                            $product->also_buys_product_id = $v;
                            $product->save(false);
                        }
                    }
                }
                //END ALSO BUYS PRODUCTS


                // RECOMMENDED PRODUCTS
                if ($recomend->load(Yii::$app->request->post())) {
                    if ($recomend->recoment_product_id != null) {
                        ProductRecomend::deleteAll(['product_id' => $model->id]);
                        $products = $recomend->recoment_product_id;
                        foreach ($products as $v) {
                            $product = new ProductRecomend();
                            $product->product_id = $model->id;
                            $product->recoment_product_id = $v;
                            $product->save(false);
                        }
                    }
                }
                //END RECOMMENDED PRODUCTS


                // ADD QUANTITY PRODUCTS
                ProductQuantity::deleteAll(['product_id' => $id]);
                foreach ($_SESSION['quantity_product'] as $v) {
                    if ($v['size'] != null) {
                        $sizes = explode(',', $v['size']);
                        foreach ($sizes as $size) {
                            $quantity = new ProductQuantity();
                            $quantity->product_id = $id;
                            if ($v["color"] != null) $quantity->color_id = $v["color"];
                            $quantity->size_id = $size;
                            $quantity->quantity = $v['quantity'];
                            $quantity->save(false);
                        }
                    } else {
                        $quantity = new ProductQuantity();
                        $quantity->product_id = $id;
                        if ($v["color"] != null) $quantity->color_id = $v["color"];
                        $quantity->quantity = $v['quantity'];
                        $quantity->save(false);
                    }
                }
                unset($_SESSION['quantity_product']);

                // END ADD QUANTITY PRODUCTS


                return $this->redirect(['view', 'id' => $model->id]);
            }
        }


        $_SESSION['quantity_product'] = array();
        $quantity = ProductQuantity::findAll(['product_id' => $id]);
        foreach ($quantity as $v) {
            $_SESSION['quantity_product'][$v->id] = array(
                "id" => $v->id,
                "color" => $v->color_id,
                "size" => $v->size_id,
                "quantity" => $v->quantity
            );
        }


        return $this->render('update', compact('model','products','alsoBuys_products',
            'recomend_products','color','size','quantity'));
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }



    public function actionAddQuantity(){


        if($_GET['color'] == null && $_GET['size'] == null){
            unset($_SESSION["quantity_product"]);
            $_SESSION["quantity_product"] = array();
        }


        if($_GET['size'] == null && $_GET['color'] != null){
            foreach ($_SESSION["quantity_product"] as $k=>$v){
                if(in_array($_GET['color'], $v)){
                    unset($_SESSION["quantity_product"][$k]);
                }
            }
        }

        if($_GET['size'] != null && $_GET['color'] != null){
            foreach ($_SESSION["quantity_product"] as $k=>$v){
                $sizes = explode(',', $_GET['size']);
                foreach ($sizes as $size) {
                    if ($v["color"] == $_GET['color'] && $v["size"] == $size) {
                        unset($_SESSION["quantity_product"][$k]);
                    }
                }
            }
        }

        $id = time();
        $_SESSION["quantity_product"][$id] = array(
            "id" => $id,
            "color" => $_GET['color'],
            "size" => $_GET['size'],
            "quantity" => $_GET['quantity']
        );
        $quantityData =  $_SESSION["quantity_product"];

        return $this->renderAjax('quantity', compact('quantityData'));
    }




    public function actionAddImage($id){
        $model = new ProductImage();
        $product = Product::findOne($id);
        if ($model->load(Yii::$app->request->post())) {
            if($product->catalog->lastParentAttribute == 2 || $product->catalog->lastParentAttribute == 3) ProductImage::deleteImagesByColor($id, $model->color_id);
            else ProductImage::deleteImages($id);
            $file = UploadedFile::getInstances($model, 'image');
            foreach ($file as $v) {
                $image = new ProductImage();
                $time = time();
                $v->saveAs($image->path . $time . '_' . $v->baseName . '.' . $v->extension);
                $image->image = $time . '_' . $v->baseName . '.' . $v->extension;
                $image->product_id = $id;
                $image->color_id = $model->color_id;
                $image->save(false);
            }

            return $this->redirect(['view', 'id' => $id]);
        }

        return $this->render('image_create', [
            'model' => $model,
            'product' => $product
        ]);
    }


    public function actionRemoveQuantity(){

        unset($_SESSION["quantity_product"][$_GET["id"]]);
        $quantityData =  $_SESSION["quantity_product"];

        return $this->renderAjax('quantity', compact('quantityData'));
    }

    public function actionGetSize(){
        $catalog =  Catalog::findOne($_GET['id']);
        $size = Size::getAllByCatalog($catalog->lastParent->id);
        return $this->renderAjax('size',compact('catalog','size'));
    }

    public function actionGetColor(){
        $_SESSION['quantity_product'] = array();
        $catalog =  Catalog::findOne($_GET['id']);
        $color = Color::getAll();
        return $this->renderAjax('color',compact('catalog','color'));
    }
}
