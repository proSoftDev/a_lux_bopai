<?php

use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Catalog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="catalog-form" >

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-12 pl-0 pr-0">
        <div class="form-group" style="float: right;margin-top:7px;">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
        <ul id="myTab" role="tablist" class="nav nav-tabs">
            <li class="nav-item active">
                <a id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" class="nav-link active">Общие</a>
            </li>
            <li class="nav-item">
                <a id="ru-tab" data-toggle="tab" href="#ru" role="tab" aria-controls="ru" aria-selected="false" class="nav-link">RU</a>
            </li>
            <li class="nav-item">
                <a id="kz-tab" data-toggle="tab" href="#kz" role="tab" aria-controls="kz" aria-selected="false" class="nav-link">KZ</a>
            </li>
        </ul>
        <div id="myTabContent" class="tab-content bg-white box-shadow p-4 mb-4">
            <div id="ru" role="tabpanel" aria-labelledby="ru-tab" class="tab-pane fade">

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            </div>

            <div id="kz" role="tabpanel" aria-labelledby="kz-tab" class="tab-pane fade">

                <?= $form->field($model, 'name_kz')->textInput(['maxlength' => true]) ?>

            </div>
            <div id="home" role="tabpanel" aria-labelledby="home-tab" class="tab-pane fade show active in">


                <?= $form->field($model, 'parent_id')->dropDownList(\app\models\Catalog::getList(),["prompt" => ""]) ?>

                <?= $form->field($model, 'attribute')->dropDownList(\app\modules\admin\status\LabelAttribute::statusList()) ?>

                <?= $form->field($model, 'status')->dropDownList(\app\modules\admin\status\Label::statusList()) ?>


            </div>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>

    if($('#catalog-parent_id').val() != ""){
        $('.field-catalog-attribute').hide();
    }else{
        $('.field-catalog-attribute').show();
    }

    $('#catalog-parent_id').change(function(){
        if($(this).val() != ""){
            $('.field-catalog-attribute').remove();
        }else{
            $('.field-catalog-attribute').add();
        }

    });
</script>
