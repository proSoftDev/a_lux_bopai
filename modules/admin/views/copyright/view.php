<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Copyright */

$this->title = "Копирайт";
$this->params['breadcrumbs'][] = ['label' => 'Копирайт', 'url' => ['index']];
\yii\web\YiiAsset::register($this);
?>
<div class="copyright-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update'], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'text:ntext',
            'text_kz:ntext',
        ],
    ]) ?>

</div>
