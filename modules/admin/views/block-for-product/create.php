<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BlockForProduct */

$this->title = 'Создание блока';
$this->params['breadcrumbs'][] = ['label' => 'Блоки для продукты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="block-for-product-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
