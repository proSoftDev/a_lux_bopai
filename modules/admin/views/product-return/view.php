<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ProductReturn */

$this->title = 'Возврат товара';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
\yii\web\YiiAsset::register($this);
?>
<div class="product-return-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактирование', ['update'], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title',
            'subtitle',
            [
                'format' => 'raw',
                'attribute' => 'content',
                'value' => function($data){
                    return $data->content;
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'condition',
                'value' => function($data){
                    return $data->condition;
                }
            ],

            'title_kz',
            'subtitle_kz',
            [
                'format' => 'raw',
                'attribute' => 'content_kz',
                'value' => function($data){
                    return $data->content_kz;
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'condition_kz',
                'value' => function($data){
                    return $data->condition_kz;
                }
            ],
        ],
    ]) ?>

</div>
