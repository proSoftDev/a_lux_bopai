<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductReturn */

$this->title = 'Редактирвание ';
$this->params['breadcrumbs'][] = ['label' => 'Возврат товара', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирвание';
?>
<div class="product-return-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
