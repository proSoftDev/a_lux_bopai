<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Статьи и новости', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="news-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',

            [
                'attribute' => 'type',
                'filter' => \app\modules\admin\status\LabelNewsAndArticle::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\status\LabelNewsAndArticle::statusLabel($model->type);
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'status',
                'filter' => \app\modules\admin\status\Label::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\status\Label::statusLabel($model->status);
                },
                'format' => 'raw',
            ],
            'date',
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width'=>100]);
                }
            ],
            'name',

            [
                'format' => 'raw',
                'attribute' => 'content',
                'value' => function($data){
                    return $data->content;
                }
            ],

            'metaName',
            'metaDesc:ntext',
            'metaKey:ntext',

            'name_kz',

            [
                'format' => 'raw',
                'attribute' => 'content_kz',
                'value' => function($data){
                    return $data->content_kz;
                }
            ],

            'metaName_kz',
            'metaDesc_kz:ntext',
            'metaKey_kz:ntext',


        ],
    ]) ?>



    <tr>

        <? foreach ($images as $v):?>
            <td>
                <img src="<?=$v->getImage();?>" width="300">
            </td>
        <? endforeach;?>

    </tr>

</div>
