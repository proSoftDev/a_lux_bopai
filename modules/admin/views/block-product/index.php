<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\BlockProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ' Продукты блока';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="block-product-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',


            [
                'attribute'=>'product_id',
                'format' => 'html',
                'filter'=>\app\models\Product::getList(),
                'value'=>function($model){
                    return Html::img($model->product->getImage(), ['width'=>50]).' '.$model->product->name;
                },
            ],

            [
                'attribute'=>'block_id',
                'filter'=>\app\models\BlockForProduct::getList(),
                'value'=>function($model){
                    return $model->block->content;
                },
                'format' => 'raw',
            ],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
