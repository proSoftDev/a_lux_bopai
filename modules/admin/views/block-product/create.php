<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BlockProduct */

$this->title = 'Создание продукты блока';
$this->params['breadcrumbs'][] = ['label' => 'Продукты блока', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="block-product-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'product' => $product
    ]) ?>

</div>
