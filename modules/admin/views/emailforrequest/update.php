<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Emailforrequest */

$this->title = 'Редактирование';
$this->params['breadcrumbs'][] = ['label' => 'Эл. почта для связи', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="emailforrequest-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
