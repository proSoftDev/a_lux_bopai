<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SocialNetwork */

$this->title = 'Социальные сети';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
\yii\web\YiiAsset::register($this);
?>
<div class="social-network-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update'], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'instagram:url',
            'telegram:url',
        ],
    ]) ?>

</div>
