<? if($catalog->lastParentAttribute == 3):?>
    <div class="form-group">
        <select class="form-control select2 size" multiple="multiple" data-placeholder="Выберите размера" style="width: 100%;" name="ProductQuantity[size_id][]">
            <? foreach ($size as $v):?>
                <option value="<?=$v->id;?>" ><?=$v->name;?></option>
            <? endforeach;?>
        </select>
    </div>
<? endif;?>