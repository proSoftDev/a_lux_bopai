<div class="box-header">
    <h3 class="box-title">Количества товары</h3>
</div>
<!-- /.box-header -->
<div class="box-body">
    <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Цвет</th>
            <th>Размер</th>
            <th>Количества</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
            <? use app\models\ProductQuantity;
            foreach ($quantityData as $v):?>
            <? $color = ProductQuantity::getColorById($v["color"]);$size = ProductQuantity::getSizeById($v["size"]);?>
            <tr>
                <td><?=$color != null?$color->name:"Нет";?></td>
                <td><?=$size;?></td>
                <td><?=$v["quantity"];?></td>
                <td>
                    <button type="button" class="btn btn-sm removeQuantity" data-id="<?=$v["id"];?>">
                        <i class="fa fa-times"></i>
                    </button>
                </td>
            </tr>
            <? endforeach;?>
        </tbody>
    </table>
</div>
<!-- /.box-body -->