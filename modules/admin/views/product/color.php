<? if($catalog->lastParentAttribute == 3 || $catalog->lastParentAttribute == 2):?>
    <div class="form-group field-productQuantity-color required has-success">
        <label class="control-label" for="productQuantity-color">Цвет</label>
        <select id="productQuantity-color" class="form-control" name="ProductQuantity[color_id]" aria-required="true" aria-invalid="false">
            <? foreach ($color as $v):?>
                <option value="<?=$v->id;?>" ><?=$v->name;?></option>
            <? endforeach;?>
        </select>
        <div class="help-block"></div>
    </div>
<? endif;?>