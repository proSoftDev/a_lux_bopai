<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Product */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Продукты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Добавить картинку', ['add-image', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>


    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width'=>100]);
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'price',
                'value' => function($data){
                    return $data->price." KZT";
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'newPrice',
                'value' => function($data){
                    return $data->newPrice != null?$data->newPrice." KZT":"";
                }
            ],
            'manufacturer',
            'sales_quantity',
            [
                'attribute' => 'isNew',
                'value' => function ($model) {
                    return \app\modules\admin\status\LabelNew::statusLabel($model->isNew);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'isPopular',
                'value' => function ($model) {
                    return \app\modules\admin\status\LabelPopular::statusLabel($model->isPopular);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'isExclusive',
                'filter' => \app\modules\admin\status\LabelExclusive::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\status\LabelExclusive::statusLabel($model->isExclusive);
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'isBackInStock',
                'filter' => \app\modules\admin\status\LabelBackInStock::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\status\LabelBackInStock::statusLabel($model->isBackInStock);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'isHit',
                'filter' => \app\modules\admin\status\LabelHit::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\status\LabelHit::statusLabel($model->isHit);
                },
                'format' => 'raw',
            ],


            [
                'attribute'=>'catalog_id',
                'value' => function ($model) {
                    return
                        Html::a($model->catalog->name, ['/admin/catalog/view', 'id' => $model->catalog->id]);
                },
                'format' => 'raw',
            ],

            [
                'format' => 'raw',
                'attribute' => 'content',
                'value' => function($data){
                    return $data->content;
                }
            ],

            [
                'format' => 'raw',
                'attribute' => 'description',
                'value' => function($data){
                    return $data->description;
                }
            ],

            'metaName',
            'metaDesc:ntext',
            'metaKey:ntext',

            'name_kz',
            'manufacturer_kz',
            [
                'format' => 'raw',
                'attribute' => 'content_kz',
                'value' => function($data){
                    return $data->content_kz;
                }
            ],

            [
                'format' => 'raw',
                'attribute' => 'description_kz',
                'value' => function($data){
                    return $data->description_kz;
                }
            ],

            'metaName_kz',
            'metaDesc_kz:ntext',
            'metaKey_kz:ntext',




        ],
    ]) ?>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
            <tr>
                <th>Рекомендация</th>
                <td>
                    <? foreach ($recomend_products as $k => $v):?>
                        <a href="/admin/product/view?id=<?=$k;?>"><?=$v?></a><br>
                    <? endforeach;?>
                </td>
            </tr>

            <tr>
                <th>С этим также покупает</th>
                <td>
                    <? foreach ($alsoBuys_products as $k => $v):?>
                        <a href="/admin/product/view?id=<?=$k;?>"><?=$v?></a><br>
                    <? endforeach;?>
                </td>
            </tr>
        </tbody>
    </table>


    <div class="box" id = 'result'>
        <div class="box-header">
            <h3 class="box-title">Количества товара</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Цвет</th>
                    <th>Размер</th>
                    <th>Количества</th>
                </tr>
                </thead>
                <tbody>
                <? if($quantity != null):?>
                    <? foreach ($quantity as $v):?>
                        <tr>
                            <td><?=$v->color != null ? $v->color->name:"Нет";?></td>
                            <td><?=$v->size;?></td>
                            <td><?=$v->quantity;?></td>
                        </tr>
                    <? endforeach;?>
                <? endif;?>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>


    <? if($model->catalog->lastParentAttribute == 3 || $model->catalog->lastParentAttribute == 2):?>
    <div class="box" id = 'result'>
        <div class="box-header">
            <h3 class="box-title">Внутренные картинки</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Цвет</th>
                    <th>Картинка</th>
                </tr>
                </thead>
                <tbody>
                <? if($images != null):?>
                    <? foreach ($images as $v):?>
                        <tr>
                            <td><?=$v->getColor();?></td>
                            <td>
                                <img src="<?=$v->getImage();?>" width="70">
                            </td>
                        </tr>
                    <? endforeach;?>
                <? endif;?>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <? endif;?>


    <? if($model->catalog->lastParentAttribute == 1):?>
    <tr>
        <? foreach ($images as $v):?>
            <td>
                <img src="/uploads/images/product/<?=$v->image?>" width="100">
            </td>
        <? endforeach;?>
    </tr>
    <? endif;?>



</div>
