<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */

$this->title = $model->fio;
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="orders-view" style="padding-bottom: 600px;">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Печатать', ['print-order', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот заказ?',
                'method' => 'post',
            ],
        ]) ?>

        <?= Html::a('Возврат заказ', ['return-order', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите возврат этот заказ?',
                'method' => 'post',
            ],
        ]) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'fio',
            'telephone',
            [
                'attribute' => 'email',
                'value' => function ($model) {
                    return $model->getEmail();
                },
                'format' => 'email',
            ],

            [
                'attribute' => 'locality',
                'filter' => \app\models\DeliveryPrice::getList(),
                'value' => function ($model) {
                    return $model->delivery->city;
                },
                'format' => 'raw',
            ],
            'address',
            [
                'attribute' => 'payment_method',
                'filter' => \app\modules\admin\status\LabelPaymentMethod::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\status\LabelPaymentMethod::statusLabel($model->payment_method);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'status_delivery',
                'filter' => \app\modules\admin\status\LabelDelivery::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\status\LabelDelivery::statusLabel($model->status_delivery);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'status_payment',
                'filter' => \app\modules\admin\status\LabelPayment::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\status\LabelPayment::statusLabel($model->status_payment);
                },
                'format' => 'raw',
            ],

            ['attribute'=>'sum', 'value'=>function($model){ return $model->sum." тг";}],
            'order_date',
            'comment:ntext',
        ],
    ]) ?>




    <br>


    <div class="box" id = 'result'>
        <div class="box-header">
            <h3 class="box-title">Продукты заказа:</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Товар</th>
                    <th>Цвет</th>
                    <th>Размер</th>
                    <th>Количества</th>
                </tr>
                </thead>
                <tbody>
                <? if($products != null):?>
                    <? foreach ($products as $v):?>
                        <tr>
                            <td><a href="/admin/product/view?id=<?=$v->product->id;?>"><img src="<?=$v->product->getImage();?>" width="50">  <?=$v->product->name;?></a></td>
                            <td><?=$v->color_id != null ? $v->product->getColor($v->color_id):"Нет";?></td>
                            <td><?=$v->size_id != null ? $v->product->getSize($v->size_id):"Нет";?></td>
                            <td><?=$v->count;?></td>
                        </tr>
                    <? endforeach;?>
                <? endif;?>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

</div>
