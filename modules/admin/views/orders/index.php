<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--    <p>-->
<!--        --><?//= Html::a('Create Orders', ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions'=>function($model){
            if($model->is_readed == 0){
                return ['class' => 'success'];
            }
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'fio',
            'telephone',
//            'email:email',
//            'address',
//            'locality',
            [
                'attribute' => 'payment_method',
                'filter' => \app\modules\admin\status\LabelPaymentMethod::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\status\LabelPaymentMethod::statusLabel($model->payment_method);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'status_delivery',
                'filter' => \app\modules\admin\status\LabelDelivery::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\status\LabelDelivery::statusLabel($model->status_delivery);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'status_payment',
                'filter' => \app\modules\admin\status\LabelPayment::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\status\LabelPayment::statusLabel($model->status_payment);
                },
                'format' => 'raw',
            ],

            //'comment:ntext',
            //'sum',
            'order_date',
            //'date_of_receiving',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
