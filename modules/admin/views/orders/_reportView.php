<?php
use yii\helpers\Html;

?>

<div class="pdf-dealer container">
    <p><span>ФИО:</span> <?=$model->fio;?></p>
    <p><span>Мобильный телефон:</span> <?=$model->telephone;?></p>
    <p><span>E-mail:</span> <?=$model->getEmail();?></p>
    <p><span>Населенный пункт:</span> <?=$model->delivery->city;?></p>
    <p><span>Адрес доставки:</span> <?=$model->address;?></p>
    <p><span>Способы оплаты:</span> <?=\app\modules\admin\status\LabelPaymentMethod::statusLabel($model->payment_method);?></p>
    <p><span>Статус доставки:</span> <?=\app\modules\admin\status\LabelDelivery::statusLabel($model->status_delivery);;?></p>
    <p><span>Статус оплаты:</span> <?=\app\modules\admin\status\LabelPayment::statusLabel($model->status_payment);?></p>
    <p><span>Сумма заказа:</span> <?=$model->sum." тг";?></p>
    <p><span>Время заказа:</span> <?=$model->order_date;?></p>
    <p><span>Комментарий к заказу:</span> <?=$model->comment;?>

    <br/><br/><br/>
    <h4>Продукты заказа:</h4>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Товар</td>
            <th>Цвет</td>
            <th>Размер</td>
            <th>Количества</td>
        </tr>
        </thead>
        <tbody>
        <? foreach ($model->products as $v):?>
            <tr>
                <td><img src="<?=$v->product->getColorImage($v->product_id, $v->color_id);?>" width="50">
                    <?=$v->product->name;?>
                </td>
                <td><?=$v->color_id != null ? $v->product->getColor($v->color_id):"Нет";?></td>
                <td><?=$v->size_id != null ? $v->product->getSize($v->size_id):"Нет";?></td>
                <td><?=$v->count;?></td>
            </tr>
        <? endforeach;?>
        </tbody>
    </table>
</div>