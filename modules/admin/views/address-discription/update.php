<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AddressDiscription */

$this->title = 'Редактирование ';
$this->params['breadcrumbs'][] = ['label' => 'Содержание', 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="address-discription-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
