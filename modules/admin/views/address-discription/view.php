<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AddressDiscription */

$this->title = 'Содержание';
$this->params['breadcrumbs'][] = ['label' => 'Содержание', 'url' => ['index']];
\yii\web\YiiAsset::register($this);
?>
<div class="address-discription-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update'], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title',
            'content:ntext',
            'title_kz',
            'content_kz:ntext',
        ],
    ]) ?>

</div>
