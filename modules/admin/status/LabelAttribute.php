<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 21.02.2019
 * Time: 22:32
 */

namespace app\modules\admin\status;


use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class LabelAttribute
{
    public static function statusList()
    {
        return [
            1 => 'Количества',
            2 => 'Количества, Цвет',
            3 => 'Количества, Цвет, Размер',
        ];
    }

    public static function statusLabel($status)
    {
        switch ($status) {
            case 1:
                $class = 'label label-default';
                break;
            case 2:
                $class = 'label label-default';
                break;
            case 3:
                $class = 'label label-default';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }


}