<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ordered_product".
 *
 * @property int $id
 * @property int $order_id
 * @property int $product_id
 * @property int $count
 * @property int $color_id
 * @property int $size_id
 */
class OrderedProduct extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ordered_product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'product_id', 'count'], 'required'],
            [['order_id', 'product_id', 'count','color_id', 'size_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'product_id' => 'Product ID',
            'count' => 'Count',
        ];
    }

    public function saveProduct($order_id, $product_id, $quantity, $color_id, $size_id){
        $this->order_id = $order_id;
        $this->product_id = $product_id;
        $this->count = $quantity;
        $this->color_id = $color_id;
        $this->size_id = $size_id;
        return $this->save(false);
    }

    public function getProduct(){
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }




}
