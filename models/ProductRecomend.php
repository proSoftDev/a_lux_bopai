<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_recomend".
 *
 * @property int $id
 * @property int $product_id
 * @property int $recoment_product_id
 */
class ProductRecomend extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_recomend';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'recoment_product_id'], 'required'],
            [['product_id', 'recoment_product_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'recoment_product_id' => 'Recoment Product ID',
        ];
    }

    public static function getAvailableProductsAsArray($product_id){

        $availableProducts = array();
        $products = ProductRecomend::findAll(['product_id' => $product_id]);
        foreach($products as $v){
            $product = Product::findOne($v->recoment_product_id);
            $availableProducts[$v->recoment_product_id] = $product->name;
        }
        return $availableProducts;
    }

    public function getProduct(){
        return Product::findOne($this->recoment_product_id);
    }

    public static function deleteRecomend($product_id){
        ProductRecomend::deleteAll(['product_id' => $product_id]);
    }


}
