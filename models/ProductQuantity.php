<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "product_quantity".
 *
 * @property int $product_id
 * @property int $color_id
 * @property int $size_id
 * @property int $quantity
 */
class ProductQuantity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    public static function tableName()
    {
        return 'product_quantity';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['quantity'], 'required'],
            [['quantity','size_id','color_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'color_id' => 'Цвет',
            'size_id' => 'Размеры',
            'quantity' => 'Количества товара',
        ];
    }





    public function getColor()
    {
        return $this->hasOne(Color::className(), ['id' => 'color_id']);
    }


    public function getSize()
    {
        if($this->size_id != null) {
            $sizes = "(" . $this->size_id . ")";
            $sizes = Size::find()->where('id in ' . $sizes)->all();
            $res = "";
            foreach ($sizes as $v) {
                $res .= $v->name . " ";
            }
        }else{
            $res = "Нет";
        }

        return $res;
    }

    public static function getColorById($id)
    {
        if($id != null){
            $color = Color::findOne((int)$id);
        }else{
            $color = null;
        }
        return $color;
    }

    public static function getSizeByID($id)
    {
        if($id != null){
            $sizes = "(".$id.")";
            $sizes = Size::find()->where('id in '.$sizes)->all();
            $res = "";
            foreach ($sizes as $v){
                $res .= $v->name.", ";
            }
            substr_replace($res ,"",-1);
        }else{
            $res = "Нет";
        }

        return $res;
    }

    public function updateQuantity($newQuantity){
        $this->quantity = $newQuantity;
        return $this->save(false);
    }


    public static function deleteQuantity($product_id){
       ProductQuantity::deleteAll(['product_id' => $product_id]);
    }


}
