<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "faq".
 *
 * @property int $id
 * @property string $question
 * @property string $answer
 * @property int $sort
 */
class Faq extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'faq';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['question', 'answer','question_kz', 'answer_kz'], 'required'],
            [['answer','answer_kz'], 'string'],
            [['sort'], 'integer'],
            [['question','question_kz'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question' => 'Вопрос',
            'answer' => 'Ответ',
            'question_kz' => 'Вопрос (KZ)',
            'answer_kz' => 'Ответ (KZ)',
            'sort' => 'Сортировка',
        ];
    }
    public function getQuestion(){
        $question = "question".Yii::$app->session["lang"];
        return $this->$question;
    }

    public function getContent(){
        $content = "answer".Yii::$app->session["lang"];
        return $this->$content;
    }

    public function updateSort(){
        $this->sort = $this->id;
        return $this->save(false);
    }

    public static function getAll(){
        return Faq::find()->orderBy('sort ASC')->all();
    }
}
