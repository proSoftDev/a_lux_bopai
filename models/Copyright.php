<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "copyright".
 *
 * @property int $id
 * @property string $text
 */
class Copyright extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'copyright';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text','text_kz'], 'required'],
            [['text','text_kz'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Содержание',
            'text_kz' => 'Содержание (KZ)',
        ];
    }

    public function getText(){
        $text = "text".Yii::$app->session["lang"];
        return $this->$text;
    }

    public static function getCopyright(){
        return Copyright::find()->one();
    }


}
