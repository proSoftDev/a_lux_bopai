<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_rating".
 *
 * @property int $id
 * @property int $product_id
 * @property int $user_id
 * @property int $rating
 */
class ProductRating extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_rating';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'user_id', 'rating'], 'required'],
            [['product_id', 'user_id', 'rating'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'user_id' => 'User ID',
            'rating' => 'Rating',
        ];
    }

    public function saveRating($user_id, $product_id, $rating){
        $this->user_id = $user_id;
        $this->product_id = $product_id;
        $this->rating = $rating;
        return $this->save(false);
    }

    public function updateRating($rating){
        $this->rating = $rating;
        return $this->save(false);
    }
}
