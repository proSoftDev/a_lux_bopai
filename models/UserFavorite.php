<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_favorite".
 *
 * @property int $user_id
 * @property int $product_id
 */
class UserFavorite extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_favorite';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'product_id'], 'required'],
            [['user_id', 'product_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'product_id' => 'Product ID',
        ];
    }

    public static function getAll(){
        return UserFavorite::findAll(['user_id'=>Yii::$app->user->identity->id]);
    }


    public static function findByUserAndProduct($user_id, $product_id){
        return UserFavorite::find()->where('user_id='.$user_id.' AND product_id='.$product_id)->one();
    }

    public function saveFavorite($user_id, $product_id){
        $this->user_id = $user_id;
        $this->product_id = $product_id;
        return $this->save(false);
    }
}
