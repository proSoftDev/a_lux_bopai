<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "social_network".
 *
 * @property int $id
 * @property string $instagram
 * @property string $telegram
 */
class SocialNetwork extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'social_network';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['instagram', 'telegram'], 'required'],
            [['instagram', 'telegram'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'instagram' => 'Instagram',
            'telegram' => 'Telegram',
        ];
    }

    public static function getAll(){
        return SocialNetwork::find()->one();
    }
}
