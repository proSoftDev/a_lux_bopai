<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_image".
 *
 * @property int $id
 * @property string $image
 * @property int $product_id
 * @property int $color_id
 */
class ProductImage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $path = 'uploads/images/product/';
    public static function tableName()
    {
        return 'product_image';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [['product_id','color_id'], 'integer'],
            [['image'], 'file', 'extensions' => 'png,jpg,jpeg','maxFiles' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Внутренняя картинка',
            'color_id' => 'Цвет',
            'product_id' => 'Продукт',
        ];
    }

    public static function deleteImages($product_id){
        $images = ProductImage::findAll(['product_id' => $product_id]);
        foreach($images as $v){
            unlink($v->path . $v->image);
            $v->delete();
        }
    }


    public static function deleteImagesByColor($product_id, $color_id){
        $images = ProductImage::findAll(['product_id' => $product_id,'color_id' => $color_id]);
        foreach($images as $v){
            unlink($v->path . $v->image);
            $v->delete();
        }
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/product/' . $this->image : '/no-image.png';
    }

    public function getColor()
    {
        if($this->color_id != null){
            $color = Color::findOne((int)$this->color_id)->name;
        }else{
            $color = null;
        }
        return $color;
    }



}
