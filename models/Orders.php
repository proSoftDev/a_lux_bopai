<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property int $user_id
 * @property string $access_token
 * @property string $fio
 * @property string $email
 * @property string $telephone
 * @property string $locality
 * @property string $address
 * @property string $comment
 * @property int $status_delivery
 * @property int $status_payment
 * @property int $payment_method
 * @property int $sum
 * @property string $order_date
 * @property string $date_of_receiving
 * @property int $is_readed
 */
class Orders extends \yii\db\ActiveRecord
{

    const READED = 1;
    const NOT_READED = 0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fio', 'telephone', 'locality', 'address', 'sum','status_delivery','status_payment','payment_method'], 'required'],
            [['user_id', 'status_delivery','status_payment','payment_method', 'sum', 'is_readed','locality'], 'integer'],
            [['comment'], 'string'],
            [['email'],'email'],
            [['order_date', 'date_of_receiving'], 'safe'],
            [['fio', 'email', 'telephone', 'address'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Номер заказа',
            'user_id' => 'User ID',
            'access_token' => 'Токен доступа',
            'fio' => 'ФИО',
            'email' => 'E-mail',
            'telephone' => 'Мобильный телефон',
            'locality' => 'Населенный пункт',
            'address' => 'Адрес доставки',
            'comment' => 'Комментарий к заказу',
            'status_delivery' => 'Статус доставки',
            'status_payment' => 'Статус оплаты',
            'payment_method' => 'Способы оплаты',
            'sum' => 'Сумма заказа',
            'order_date' => 'Время заказа',
            'date_of_receiving' => 'Date Of Receiving',
            'is_readed' => 'Is readed'
        ];
    }


    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }


    public function getProducts(){
        return $this->hasMany(OrderedProduct::className(), ['order_id' => 'id']);
    }

    public function beforeDelete()
    {
        OrderedProduct::deleteAll(['order_id'=>$this->id]);
        return parent::beforeDelete();
    }

    public function generateAccessToken()
    {
        $this->access_token = Yii::$app->security->generateRandomString();
    }

    public function saveOrder($fio, $email, $telephone, $locality, $address, $comment,$sum, $status_delivery,$status_payment, $payment_method){

        if(!Yii::$app->user->isGuest)
            $this->user_id = Yii::$app->user->identity->id;
        $this->generateAccessToken();
        $this->fio = $fio;
        $this->email = $email;
        $this->telephone = $telephone;
        $this->locality = $locality;
        $this->address = $address;
        $this->comment = $comment;
        $this->status_delivery = $status_delivery;
        $this->status_payment = $status_payment;
        $this->payment_method = $payment_method;
        $this->sum = $sum;
        $this->is_readed = 0;
        return $this->save(false);
    }

    public function updateAccessToken($token){
        $this->access_token = $token;
        return $this->save(false);
    }


    public function getCountProduct(){
        $product = OrderedProduct::find()->where('order_id = '.$this->id)->all();
        return count($product);
    }

    public function getDate(){
        Yii::$app->formatter->locale = 'ru-RU';
        return Yii::$app->formatter->asDate($this->order_date, 'long');;
    }

    public static function getOrders(){
        return Orders::find()->where('user_id = '.Yii::$app->user->id)->orderBy('id DESC')->all();
    }

    public function getEmail(){
        return $this->email != null ? $this->email : 'Нет';
    }

    public function isReaded(){
        $this->is_readed =  self::READED;
        return $this->save(false);
    }

    public function getDelivery(){
        return $this->hasOne(DeliveryPrice::className(), ['id' => 'locality']);
    }



}
