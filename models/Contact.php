<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contact".
 *
 * @property int $id
 * @property string $telephone
 * @property string $address
 * @property string $email
 */
class Contact extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['telephone', 'address','address_kz', 'email'], 'required'],
            [['telephone', 'address','address_kz', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'telephone' => 'Телефон',
            'address' => 'Адрес',
            'address_kz' => 'Адрес (KZ)',
            'email' => 'E-mail',
        ];
    }

    public static function getAll(){
        return Contact::find()->one();
    }

    public function getAddress(){
        $name = "address".Yii::$app->session["lang"];
        return $this->$name;
    }
}
