<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "about".
 *
 * @property int $id
 * @property string $title
 * @property string $content
 */
class About extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'about';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'content','title_kz', 'content_kz'], 'required'],
            [['content', 'content_kz'], 'string'],
            [['title', 'title_kz'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'content' => 'Содержание',
            'title_kz' => 'Заголовок (KZ)',
            'content_kz' => 'Содержание (KZ)',
        ];
    }

    public static function getAll(){
        return About::find()->all();
    }


    public function getTitle(){
        $title = "title".Yii::$app->session["lang"];
        return $this->$title;
    }

    public function getContent(){
        $content = "content".Yii::$app->session["lang"];
        return $this->$content;
    }
}
