<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "instagram".
 *
 * @property int $id
 * @property string $name
 * @property string $url
 */
class Instagram extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'instagram';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title','name', 'url', 'title_kz'], 'required'],
            [['url'], 'string'],
            [['title', 'title_kz', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'title_kz' => 'Заголовок (KZ)',
            'name' => 'Название',
            'url' => 'Ссылка',
        ];
    }

    public function getTitle(){
        $title = "title".Yii::$app->session["lang"];
        return $this->$title;
    }

    public static function getAll(){
        return Instagram::find()->all();
    }
}
