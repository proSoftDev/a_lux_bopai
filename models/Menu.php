<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property int $id
 * @property string $text
 * @property int $status
 * @property string $metaName
 * @property string $metaDesc
 * @property string $metaKey
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text', 'status', 'metaName', 'metaName_kz'], 'required'],
            [['status'], 'integer'],
            [['metaDesc', 'metaKey','metaDesc_kz', 'metaKey_kz'], 'string'],
            [['text','text_kz', 'metaName','metaName_kz'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => '	Заголовок',
            'status' => 'Статус',
            'metaName' => 'Мета Названия',
            'metaDesc' => 'Мета Описание',
            'metaKey' => 'Ключевые слова',
            'text_kz' => '	Заголовок (KZ)',
            'metaName_kz' => 'Мета Названия (KZ)',
            'metaDesc_kz' => 'Мета Описание (KZ)',
            'metaKey_kz' => 'Ключевые слова (KZ)',
        ];
    }

    public function getText(){
        $text = "text".Yii::$app->session["lang"];
        return $this->$text;
    }

    public function getMetaName(){
        $content = "metaName".Yii::$app->session["lang"];
        return $this->$content;
    }

    public function getMetaDescription(){
        $content = "metaDesc".Yii::$app->session["lang"];
        return $this->$content;
    }

    public function getMetaKeywords(){
        $content = "metaKey".Yii::$app->session["lang"];
        return $this->$content;
    }

    public static function getModel($url){
        return Menu::find()->where('url = "'.$url.'"')->one();
    }

    public static function getHeaderMenu(){
        return Menu::find()->where('status=1 && id < 6')->orderBy('sort ASC')->limit(4)->all();
    }

}
