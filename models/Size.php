<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "size".
 *
 * @property int $id
 * @property string $name
 * @property string $catalog_id
 * @property string $sort
 */
class Size extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'size';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name','catalog_id'], 'required'],
            [['name','catalog_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'catalog_id' => 'Каталог'
        ];
    }

    public static function getAllByCatalog($catalog_id){
        return Size::find()->where('catalog_id='.$catalog_id)->orderBy('sort ASC')->all();
    }

    public static function getAll(){
        return Size::find()->orderBy('sort ASC')->all();
    }

    public function getCatalog()
    {
        return $this->hasOne(Catalog::className(), ['id' => 'catalog_id']);
    }

    public function setSort(){
        $this->sort = $this->id;
        return $this->save(false);
    }
}
