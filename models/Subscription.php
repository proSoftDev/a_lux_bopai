<?php

namespace app\models;

use app\controllers\ContentController;
use app\modules\admin\controllers\SubscriptionController;
use Yii;

/**
 * This is the model class for table "subscription".
 *
 * @property int $id
 * @property string $email
 */
class Subscription extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subscription';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['email'], 'email'],
            [['email'], 'unique','message' => 'E-mail уже подписан.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'E-mail',
            'created_at' => 'Дата подписки'
        ];
    }

   public static function getAll(){
        return Subscription::find()->all();
   }


}
