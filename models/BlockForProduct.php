<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "block_for_product".
 *
 * @property int $id
 * @property string $image
 * @property string $content
 * @property string $metaName
 * @property string $metaDesc
 * @property string $metaKey
 */
class BlockForProduct extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $path = 'uploads/images/block-for-product/';
    public static function tableName()
    {
        return 'block_for_product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content', 'metaName','content_kz', 'metaName_kz'], 'required'],
            [['content','metaDesc', 'metaKey','content_kz','metaDesc_kz', 'metaKey_kz'], 'string'],
            [['metaName', 'metaName_kz'], 'string', 'max' => 255],
            [['image'], 'file', 'extensions' => 'png,jpg,jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Картинка',
            'content' => 'Содержание',
            'metaName' => 'Мета Названия',
            'metaDesc' => 'Мета Описание',
            'metaKey' => 'Ключевые слова',
            'content_kz' => 'Содержание (KZ)',
            'metaName_kz' => 'Мета Названия (KZ)',
            'metaDesc_kz' => 'Мета Описание (KZ)',
            'metaKey_kz' => 'Ключевые слова (KZ)',
        ];
    }

    public static function getList(){
        return \yii\helpers\ArrayHelper::map(BlockForProduct::find()->all(),'id','content');
    }

    public function getProducts(){
        return $this->hasMany(BlockProduct::className(), ['block_id' => 'id']);
    }

    public function getImage()
    {
        return ($this->image) ? '/'.$this->path . $this->image : '/no-image.png';
    }

    public function getContent(){
        $content = "content".Yii::$app->session["lang"];
        return $this->$content;
    }

    public function getMetaName(){
        $content = "metaName".Yii::$app->session["lang"];
        return $this->$content;
    }

    public function getMetaDescription(){
        $content = "metaDesc".Yii::$app->session["lang"];
        return $this->$content;
    }

    public function getMetaKeywords(){
        $content = "metaKey".Yii::$app->session["lang"];
        return $this->$content;
    }

    public static function getAll(){
        return BlockForProduct::find()->all();
    }

}
