<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_also_buys".
 *
 * @property int $id
 * @property int $product_id
 * @property int $also_buys_product_id
 */
class ProductAlsoBuys extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_also_buys';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'also_buys_product_id'], 'required'],
            [['product_id', 'also_buys_product_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Продукт',
            'also_buys_product_id' => 'С этим также покупает',
        ];
    }


    public static function getAvailableProductsAsArray($product_id){

        $availableProducts = array();
        $products = ProductAlsoBuys::findAll(['product_id' => $product_id]);
        foreach($products as $v){
            $product = Product::findOne($v->also_buys_product_id);
            $availableProducts[$v->also_buys_product_id] = $product->name;
        }
        return $availableProducts;
    }

    public function getProduct(){
        return Product::findOne($this->also_buys_product_id);
    }

    public static function deleteAlsoBuys($product_id){
        ProductAlsoBuys::deleteAll(['product_id' => $product_id]);
    }
}
