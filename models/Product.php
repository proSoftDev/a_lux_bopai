<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $name
 * @property int $price
 * @property int $newPrice
 * @property int $image
 * @property int $catalog_id
 * @property string $content
 * @property string $description
 * @property int $sales_quantity
 * @property int $isNew
 * @property int $isPopular
 * @property int $isExclusive
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $path = 'uploads/images/product/';
    public $count;
    public $color;
    public $size;

    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'price',
                'isNew', 'isPopular',
                'isExclusive','isBackInStock',
                'isHit','catalog_id'], 'required'],
            [['price', 'newPrice', 'sales_quantity', 'isNew', 'isPopular', 'isExclusive','isBackInStock','isHit','catalog_id'], 'integer'],
            [['content', 'description','metaDesc', 'metaKey','content_kz', 'description_kz','metaDesc_kz', 'metaKey_kz'], 'string'],
            [['name','manufacturer','metaName','name_kz','manufacturer_kz','metaName_kz'], 'string', 'max' => 255],
            [['image'], 'file', 'extensions' => 'png,jpg,jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'price' => 'Цена',
            'newPrice' => 'Цена со скидкой',
            'image' => 'Главная картинка',
            'content' => 'Содержание',
            'description' => 'Описание',
            'manufacturer' => 'Производитель',
            'sales_quantity' => 'Продано',
            'isNew' => 'Новинка',
            'isPopular' => 'Популярное',
            'isExclusive' => 'Эксклюзив',
            'isBackInStock' => 'Снова в наличие',
            'isHit' => 'Хит продаж',
            'metaName' => 'Мета Названия',
            'metaDesc' => 'Мета Описание',
            'metaKey' => 'Ключевые слова',
            'name_kz' => 'Название (KZ)',
            'content_kz' => 'Содержание (KZ)',
            'description_kz' => 'Описание (KZ)',
            'manufacturer_kz' => 'Производитель (KZ)',
            'metaName_kz' => 'Мета Названия (KZ)',
            'metaDesc_kz' => 'Мета Описание (KZ)',
            'metaKey_kz' => 'Ключевые слова (KZ)',
            'catalog_id' => 'Категория'
        ];
    }

    public function getName(){
        $text = "name".Yii::$app->session["lang"];
        return $this->$text;
    }

    public function getContent(){
        $content = "content".Yii::$app->session["lang"];
        return $this->$content;
    }

    public function getDescription(){
        $content = "description".Yii::$app->session["lang"];
        return $this->$content;
    }

    public function getManufacturer(){
        $content = "manufacturer".Yii::$app->session["lang"];
        return $this->$content;
    }

    public function getMetaName(){
        $content = "metaName".Yii::$app->session["lang"];
        return $this->$content;
    }

    public function getMetaDescription(){
        $content = "metaDesc".Yii::$app->session["lang"];
        return $this->$content;
    }

    public function getMetaKeywords(){
        $content = "metaKey".Yii::$app->session["lang"];
        return $this->$content;
    }

    public function afterDelete()
    {
        ProductImage::deleteImages($this->id);
        ProductQuantity::deleteQuantity($this->id);
        ProductAlsoBuys::deleteAlsoBuys($this->id);
        ProductRecomend::deleteRecomend($this->id);
        parent::afterDelete(); // TODO: Change the autogenerated stub
    }

    public function getCatalog()
    {
        return $this->hasOne(Catalog::className(), ['id' => 'catalog_id']);
    }


    public static function getList(){
        return \yii\helpers\ArrayHelper::map(Product::find()->all(),'id','name');
    }

    public function getImages(){
        return $this->hasMany(ProductImage::className(), ['product_id' => 'id']);
    }

    public function getRecommendedProducts(){
        return $this->hasMany(ProductRecomend::className(), ['product_id' => 'id']);
    }

    public function getAlsoBuysProducts(){
        return $this->hasMany(ProductAlsoBuys::className(), ['product_id' => 'id']);
    }

    public function getImage()
    {
        return ($this->image) ? '/'. $this->path . $this->image : '/no-image.png';
    }

    public function getFavoriteStatus(){
        if(UserFavorite::findOne(['user_id' => Yii::$app->user->id, 'product_id' => $this->id])) return 1;
        else return 0;
    }

    public function getProductColors()
    {
        $model = ProductQuantity::findAll(['product_id' => $this->id]);
        $arr = array();
        if($model != null){
            foreach ($model as $v){
                if(!in_array($v->color_id, $arr)) $arr[] = $v->color_id;
            }
            $response = Color::find()->where('id in ('.implode(",", $arr) . ')')->all();
        }else{
            $response = null;
        }
        return $response;
    }

    public function getProductSizes()
    {
        $model = ProductQuantity::findAll(['product_id' => $this->id]);
        $arr = array();
        if($model != null){
            foreach ($model as $v){
                if(!in_array($v->size_id, $arr)) $arr[] = $v->size_id;
            }
            $response = Size::find()->where('id in ('.implode(",", $arr) . ')')->all();
        }else{
            $response = null;
        }
        return $response;
    }

    public function getColorSizes($color_id)
    {
        return ProductQuantity::findAll(['product_id' => $this->id,'color_id' => $color_id]);
    }

    public function getSizeColors($size_id)
    {
        return ProductQuantity::findAll(['product_id' => $this->id,'size_id' => $size_id]);
    }

    public function getColor($id){
        $color =  Color::findOne($id);
        return $color->getName();
    }

    public function getSize($id){
        $size =  Size::findOne($id);
        return $size->name;
    }

    public function getColorImage($product_id, $color_id)
    {
        $model  = ProductImage::findOne(['product_id' => $product_id, 'color_id' => $color_id]);

        return ($model->image) ? '/'. $model->path . $model->image : '/no-image.png';
    }


    public function getQuantity(){
        $data = ProductQuantity::findAll(['product_id' => $this->id]);
        if($data){
            $response = 0;
            foreach ($data as $v){
                $response+= $v->quantity;
            }
        }else{
            $response = 0;
        }
        return $response;
    }


    public function getQuantityByColor($color_id){
        $data = ProductQuantity::findAll(['product_id' => $this->id, 'color_id' => $color_id]);
        if($data){
            $response = 0;
            foreach ($data as $v){
                $response += $v->quantity;
            }
        }else{
            $response = 0;
        }
        return $response;
    }

    public function getQuantityBySize($size_id){
        $data = ProductQuantity::findAll(['product_id' => $this->id, 'size_id' => $size_id]);
        if($data){
            $response = 0;
            foreach ($data as $v){
                $response += $v->quantity;
            }
        }else{
            $response = 0;
        }
        return $response;
    }


    public function getQuantityByColorAndSize($color_id, $size_id){
        $data = ProductQuantity::findOne(['product_id' => $this->id, 'color_id' => $color_id, 'size_id' => $size_id]);
        if($data){
            $response = $data->quantity;
        }else{
            $response = 0;
        }
        return $response;
    }


    public static function getSum(){
        $sum =0;
        if($_SESSION['basket'] != null){
            foreach ($_SESSION['basket'] as $v){
                if($v->newPrice != null){
                    $price = $v->newPrice;
                }else{
                    $price = $v->price;
                }
                $sum+=(int)$v->count*(int)$price;
            }
        }
        return $sum;
    }

    public function getPrice(){
        if($this->newPrice != null){
            $price = $this->newPrice;
        }else{
            $price = $this->price;
        }
        return $price;
    }

    public static function getCount(){
        $count =0;
        if($_SESSION['basket'] != null){
            foreach ($_SESSION['basket'] as $v){
                $count += $v->count;
            }
        }
        return $count;
    }



    public function getSumProduct(){
        $sum =0;
        if($_SESSION['basket'] != null){
            foreach ($_SESSION['basket'] as $v){
                if($this->catalog->lastParentAttribute == 3) {
                    if ($v->id == $this->id && $v->color == $this->color && $v->size == $this->size) {
                        if ($v->newPrice != null) {
                            $price = $v->newPrice;
                        } else {
                            $price = $v->price;
                        }
                        $sum += (int)$v->count * (int)$price;
                    }
                }elseif($this->catalog->lastParentAttribute == 2){
                     if ($v->id == $this->id && $v->color == $this->color) {
                         if ($v->newPrice != null) {
                             $price = $v->newPrice;
                         } else {
                             $price = $v->price;
                         }
                         $sum += (int)$v->count * (int)$price;
                     }
                }elseif($this->catalog->lastParentAttribute == 1){
                    if ($v->id == $this->id) {
                        if ($v->newPrice != null) {
                            $price = $v->newPrice;
                        } else {
                            $price = $v->price;
                        }
                        $sum += (int)$v->count * (int)$price;
                    }
                }
            }
        }

        return $sum;
    }



    public function getProduct_quantity(){
        return $this->hasOne(ProductQuantity::className(), ['product_id' => 'id']);
    }

    public function updateSalesQuantity($quantity){
        $this->sales_quantity =  $this->sales_quantity + $quantity;
        return $this->save(false);
    }

    public static function getAll(){
        return Product::find()->orderBy('id DESC')->all();
    }

    public static function getPopular(){
        return Product::findAll(['isPopular' => 1]);
    }

    public static function getNew(){
        return Product::findAll(['isNew' => 1]);
    }

    public static function getBackInStock(){
        return Product::findAll(['isBackInStock' => 1]);
    }

    public static function getHit(){
        return Product::findAll(['isHit' => 1]);
    }


    public static function getSearchResult($keyword){
        return Product::find()->where("name LIKE '%$keyword%' OR content LIKE '%$keyword%'")->all();
    }

    public function getRating(){
        $rating = ProductRating::find()->where(['product_id' => $this->id])->average('rating');
        if($rating == null) $rating = 0;
        return number_format((float)$rating, 1, '.', '');
    }

    public function getRatingCount(){
        return ProductRating::find()->where(['product_id' => $this->id])->count();
    }




}
