<?php

namespace app\models;

use app\models\search\NewsSearch;
use Yii;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property string $name
 * @property string $image
 * @property string $date
 * @property string $content
 * @property int $type
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * {@inheritdoc}
     */
    public $path = 'uploads/images/news/';
    public function rules()
    {
        return [
            [['name', 'content', 'type','status','metaName','name_kz', 'content_kz', 'metaName_kz'], 'required'],
            [['date'], 'safe'],
            [['content','metaDesc', 'metaKey','content_kz','metaDesc_kz', 'metaKey_kz'], 'string'],
            [['type'], 'integer'],
            [['name', 'metaName','name_kz', 'metaName_kz'], 'string', 'max' => 255],
            [['image'], 'file', 'extensions' => 'png,jpg,jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'image' => 'Картинка',
            'date' => 'Дата',
            'content' => 'Содержание',
            'type' => 'Тип',
            'status' => 'На главном',
            'metaName' => 'Мета Названия',
            'metaDesc' => 'Мета Описание',
            'metaKey' => 'Ключевые слова',
            'name_kz' => 'Название (KZ)',
            'content_kz' => 'Содержание (KZ)',
            'metaName_kz' => 'Мета Названия (KZ)',
            'metaDesc_kz' => 'Мета Описание (KZ)',
            'metaKey_kz' => 'Ключевые слова (KZ)',
        ];
    }

    public function getName(){
        $name = "name".Yii::$app->session["lang"];
        return $this->$name;
    }

    public function getContent(){
        $content = "content".Yii::$app->session["lang"];
        return $this->$content;
    }

    public function getMetaName(){
        $content = "metaName".Yii::$app->session["lang"];
        return $this->$content;
    }

    public function getMetaDescription(){
        $content = "metaDesc".Yii::$app->session["lang"];
        return $this->$content;
    }

    public function getMetaKeywords(){
        $content = "metaKey".Yii::$app->session["lang"];
        return $this->$content;
    }


    public function getImage(){
        return ($this->image) ? '/'. $this->path . $this->image : '/no-image.png';
    }

    public function getDate()
    {
        Yii::$app->formatter->locale = 'ru-RU';
        return Yii::$app->formatter->asDate($this->date, 'long');
    }


    public function getSliderImages(){
        return $this->hasMany(NewsImages::className(), ['news_id' => 'id']);
    }

    public function beforeDelete()
    {
        NewsImages::deleteImages($this->id);
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }

    public static function getAllNews(){
        return News::find()->where('type = 1')->orderBy('id DESC')->all();
    }

    public static function getAllArticles(){
        return News::find()->where('type = 2')->orderBy('id DESC')->all();
    }

    public static function getMainArticles(){
        return News::find()->where('type = 2 && status=1')->orderBy('id DESC')->all();
    }


    public static function getMainNews(){
        return News::find()->where('type = 1 && status=1')->orderBy('id DESC')->all();
    }
}
