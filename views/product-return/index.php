<div class="return">
    <div class="container mt-5 mb-5">
        <div class="row">
            <div class="col-sm-12 col-md-3">
                <nav class="switch-nav">
                    <ul>
                        <li><a href="<?=Yii::$app->view->params['footerMenu'][10]->url;?>"><?=Yii::$app->view->params['footerMenu'][10]->getText();?></a></li>
                        <li><a href="<?=Yii::$app->view->params['footerMenu'][2]->url?>"><?=Yii::$app->view->params['footerMenu'][2]->getText()?></a></li>
                        <li><a href="<?=Yii::$app->view->params['footerMenu'][3]->url?>"><?=Yii::$app->view->params['footerMenu'][3]->getText();?></a></li>
                        <li class="switch-nav_active"><a href="#"><?=Yii::$app->view->params['footerMenu'][4]->getText()?></a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-sm-12 col-md-5 ml-5">
                <div class="return__container">

                    <div>
                        <h1><?=$return->getTitle();?></h1>
                        <p><?=$return->getSubTitle();?></p>
                    </div>
                    <?=$return->getContent();?>
<!--                    <div class="mt-5">-->
<!--                        <input type="button" class="checkout" value="Оформить возврат товара">-->
<!--                    </div>-->
                </div>
            </div>
            <div class="col-sm-12 col-md-3">
                <div class="return__notification">
                    <?=$return->getCondition();?>
                </div>
            </div>
        </div>
    </div>
</div>