<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error" style="margin-top: 50px;margin-bottom: 50px;">



    <div class="alert alert-danger">
        <?=Yii::t('app', 'Page not found');?>
    </div>

    <p>
        <?=Yii::t('app', 'The above error occurred when the web server was processing your request.');?>
    </p>
    <p>
        <?=Yii::t('app', 'Please contact us if you think this is a server error. Thank.');?>
    </p>

</div>
