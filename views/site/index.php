

<div class="main-page">
    <section class="grid">
        <div class="wrap">
            <div class="sidebar">
                <div class="menu">
                    <ul>
                        <? foreach ($catalog as $v):?>
                            <? $category = $v->category;?>
                            <li>
                                <a href="/catalog?id=<?=$v->id;?>"><?=$v->getName();?></a>
                                <img src="/public/img/arrow-right.png" alt="" class="accordion-trigger">
                                <ul class="sub-menu">
                                    <? foreach ($category as $val):?>
                                        <li>
                                            <a href="/catalog?id=<?=$val->id;?>"><?=$val->getName();?></a>
                                        </li>
                                    <? endforeach;?>
                                </ul>
                            </li>
                        <? endforeach;?>
                    </ul>
                </div>

                <? if(count($article) > 0):?>
                <div class="news">
                    <div class="title"><?=Yii::t('app', 'Useful articles');?></div>
                    <div class="news_cont">
                        <? foreach ($article as $v):?>
                        <div class="news_item">
                            <div class="img">
                                <img src="<?=$v->getImage()?>" alt="">
                            </div>
                            <? $type = $model->type == 1 ? "news":"article";?>
                            <a href="<?='/'.$type.'/view?id='.$v->id?>"><?=$v->getName();?></a>
                        </div>
                        <? endforeach;?>
                    </div>
                </div>
                <? endif;?>

                <? if(count($news) > 0):?>
                    <div class="news">
                        <div class="title"><?=Yii::t('app', 'Latest news');?></div>
                        <div class="news_cont">
                            <? foreach ($news as $v):?>
                                <div class="news_item">
                                    <div class="img">
                                        <img src="<?=$v->getImage()?>" alt="">
                                    </div>
                                    <? $type = $model->type == 1 ? "news":"article";?>
                                    <a href="<?='/'.$type.'/view?id='.$v->id?>"><?=$v->getName();?></a>
                                </div>
                            <? endforeach;?>
                        </div>
                    </div>
                <? endif;?>

                <div class="payment">
                    <div class="title"><?=Yii::t('app', 'Payment Methods');?></div>
                    <div class="subtitle"><?=Yii::t('app', 'We accept payments from the following payment systems');?></div>
                    <div class="pay_cont">
                        <a href="">
                            <img src="/public/img/master card.png" alt="">
                        </a>
                        <a href="">
                            <img src="/public/img/yandex.png" alt="">
                        </a>
                        <a href="">
                            <img src="/public/img/webmoney.png" alt="">
                        </a>
                        <a href="">
                            <img src="/public/img/verified visa.png" alt="">
                        </a>
                        <a href="">
                            <img src="/public/img/visa.png" alt="">
                        </a>
                    </div>
                </div>
            </div>


            <div class="content">
                <div class="main_ban_cont">
                    <a href="/catalog/block?id=<?=Yii::$app->view->params['block'][0]->id;?>" class="ban_75"
                        style="background-image: url(<?=Yii::$app->view->params['block'][0]->getImage();?>);">
                        <?=Yii::$app->view->params['block'][0]->getContent();?>
                    </a>
                    <div class="ban_cont_35">
                        <a href="/catalog/block?id=<?=Yii::$app->view->params['block'][1]->id;?>" class="main_ban_item"
                           style="background-image: url(<?=Yii::$app->view->params['block'][1]->getImage();?>);">
                            <?=Yii::$app->view->params['block'][1]->getContent();?>
                        </a>
                        <a href="/catalog/block?id=<?=Yii::$app->view->params['block'][2]->id;?>" class="main_ban_item"
                           style="background-image: url(<?=Yii::$app->view->params['block'][2]->getImage();?>);">
                            <?=Yii::$app->view->params['block'][2]->getContent();?>
                        </a>
                    </div>
                </div>
                <div class="ban_cont">
                    <a href="/catalog/news" class="ban_item">
                        <?=Yii::t('app', 'NEWS!');?>
                    </a>
                    <a href="/catalog/block?id=<?=Yii::$app->view->params['block'][3]->id;?>" class="ban_item"
                       style="background-image: url(<?=Yii::$app->view->params['block'][3]->getImage();?>);">
                        <?=Yii::$app->view->params['block'][3]->getContent();?>
                    </a>
                    <a href="/catalog/back-in-stock" class="ban_item">
                        <?=Yii::t('app', 'Back in stock');?>
                    </a>
                    <a href="/catalog/hit" class="ban_item">
                        <?=Yii::t('app', 'Bestseller');?>
                    </a>
                </div>

                <div class="slider_title_nav">
                    <div class="title">
                        <p><?=Yii::t('app', 'Buyers Choice!');?></p>
                        <a href="/catalog/popular"><span><?=Yii::t('app', 'All popular products');?></span></a>
                    </div>
                    <div class="nav">
                        <div class="arrow-left">
                            <svg width="62" height="62" viewBox="0 0 62 62" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <g filter="url(#filter0_d)">
                                    <rect x="10" y="7" width="42" height="42" rx="21" fill="white" />
                                </g>
                                <path d="M33 34L27 28L33 22" stroke="#AAAABE" stroke-width="2"
                                      stroke-linecap="round" stroke-linejoin="round" />
                                <defs>
                                    <filter id="filter0_d" x="0" y="0" width="62" height="62"
                                            filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                                        <feFlood flood-opacity="0" result="BackgroundImageFix" />
                                        <feColorMatrix in="SourceAlpha" type="matrix"
                                                       values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
                                        <feOffset dy="3" />
                                        <feGaussianBlur stdDeviation="5" />
                                        <feColorMatrix type="matrix"
                                                       values="0 0 0 0 0 0 0 0 0 0.1 0 0 0 0 1 0 0 0 0.03 0" />
                                        <feBlend mode="normal" in2="BackgroundImageFix"
                                                 result="effect1_dropShadow" />
                                        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow"
                                                 result="shape" />
                                    </filter>
                                </defs>
                            </svg>
                        </div>
                        <div class="arrow-right">
                            <svg width="62" height="62" viewBox="0 0 62 62" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <g filter="url(#filter0_d)">
                                    <rect x="52" y="49" width="42" height="42" rx="21"
                                          transform="rotate(-180 52 49)" fill="white" />
                                </g>
                                <path d="M29 22L35 28L29 34" stroke="#AAAABE" stroke-width="2"
                                      stroke-linecap="round" stroke-linejoin="round" />
                                <defs>
                                    <filter id="filter0_d" x="0" y="0" width="62" height="62"
                                            filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                                        <feFlood flood-opacity="0" result="BackgroundImageFix" />
                                        <feColorMatrix in="SourceAlpha" type="matrix"
                                                       values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
                                        <feOffset dy="3" />
                                        <feGaussianBlur stdDeviation="5" />
                                        <feColorMatrix type="matrix"
                                                       values="0 0 0 0 0 0 0 0 0 0.1 0 0 0 0 1 0 0 0 0.03 0" />
                                        <feBlend mode="normal" in2="BackgroundImageFix"
                                                 result="effect1_dropShadow" />
                                        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow"
                                                 result="shape" />
                                    </filter>
                                </defs>
                            </svg>

                        </div>
                    </div>
                </div>

                <div class="slider_popular_tov">
                    <? foreach ($popular as $v):?>
                    <? if($v->quantity > 0):?>
                    <div class="tov_item">
                        <a href="/product?id=<?=$v->id;?>" class="tov_item_img">
                            <img src="<?=$v->getImage();?>" alt="">
                        </a>
                        <a href="/product?id=<?=$v->id;?>" class="tov-button"><?=Yii::t('app', 'More details');?></a>
                        <div class="tov_item_info">
                            <? if($v->isNew):?>
                                <div class="tov_new"><?=Yii::t('app', 'NEW!');?></div>
                            <? endif;?>
                            <? if($v->isPopular):?>
                                <div class="tov_new tov_pov"><?=Yii::t('app', 'Popular!');?></div>
                            <? endif;?>
                            <? if($v->isExclusive):?>
                                <div class="tov_new tov_exs"><?=Yii::t('app', 'Exclusive!');?></div>
                            <? endif;?>
                            <p><?=$v->getName();?></p>
                            <? if($v->newPrice != null):?>
                                <div class="old_price"><?=$v->price;?><span>тг</span></div>
                                <div class="price"><?=$v->newPrice;?><span>тг</span></div>
                            <? endif;?>
                            <? if($v->newPrice == null):?>
                                <div class="price"><?=$v->price;?><span>тг</span></div>
                            <? endif;?>
                        </div>
                    </div>
                    <? endif;?>
                    <? endforeach;?>
                </div>

                <div class="title_tov">
                    <div class="title">
                        <p><?=Yii::t('app', 'Catalog of women\'s clothing!');?></p>
                        <a href="/catalog?id=1"><span><?=Yii::t('app', 'Go to catalog');?></span></a>
                    </div>
                </div>

                <div class="cont_tov">
                    <? foreach ($clothes as $v):?>
                    <? if($v->quantity > 0):?>
                        <div class="tov_item">
                            <a href="/product?id=<?=$v->id;?>" class="tov_item_img">
                                <img src="<?=$v->getImage();?>" alt="">
                            </a>
                            <a href="/product?id=<?=$v->id;?>" class="tov-button"><?=Yii::t('app', 'More details');?></a>
                            <div class="tov_item_info">
                                <? if($v->isNew):?>
                                    <div class="tov_new"><?=Yii::t('app', 'NEW!');?></div>
                                <? endif;?>
                                <? if($v->isPopular):?>
                                    <div class="tov_new tov_pov"><?=Yii::t('app', 'Popular!');?></div>
                                <? endif;?>
                                <? if($v->isExclusive):?>
                                    <div class="tov_new tov_exs"><?=Yii::t('app', 'Exclusive!');?></div>
                                <? endif;?>
                                <p><?=$v->getName();?></p>
                                <? if($v->newPrice != null):?>
                                    <div class="old_price"><?=$v->price;?><span>тг</span></div>
                                    <div class="price"><?=$v->newPrice;?><span>тг</span></div>
                                <? endif;?>
                                <? if($v->newPrice == null):?>
                                    <div class="price"><?=$v->price;?><span>тг</span></div>
                                <? endif;?>
                            </div>
                        </div>
                    <? endif;?>
                    <? endforeach;?>
                </div>

                <a href="/catalog/block?id=<?=Yii::$app->view->params['block'][4]->id;?>" class="offer"
                   style="background-image: url(<?=Yii::$app->view->params['block'][4]->getImage();?>);">
                    <?=Yii::$app->view->params['block'][4]->getContent();?>
                </a>
            </div>
        </div>
    </section>
</div>