
<div class="checkout mb-5">
    <div class="container mt-4">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <p class="main-page-link"><a href="/"><?=Yii::$app->view->params['main']->getText();?></a> <img src="/public/img/_.png"> <?=Yii::t('app', 'Checkout');?></p>
            </div>
        </div>
    </div>
    <div class="container mt-5">
        <div class="row align-items-start">
            <div class="col-sm-12 col-md-8 basket-products__contain">
                <h1 class="basket-header"><?=Yii::t('app', 'Items in the basket');?></h1>
                <? if(count($_SESSION['basket']) != 0):?>
                    <div class="container mt-4">
                    <? $m = 0;?>
                    <? foreach ($_SESSION['basket'] as $v):?>
                        <? $m++;?>
                        <div class="row">
                            <div class="col-md-2 col-4 pl-0">
                                <img width="100%" src="<?=$v->getImage();?>">
                            </div>
                            <div class="col-md-10 col-8">
                                <div class="container basket-products">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <h3><?=$v->getName();?></h3>
                                            <div class="container pl-0">
                                                <? if($v->size != null):?>
                                                    <div class="row align-items-end mt-2">
                                                        <div class="col-sm-12 col-md-2"><p><?=Yii::t('app', 'Size');?></p></div>
                                                        <div class="col-sm-12 col-md-6 line"></div>
                                                        <div class="col-sm-12 col-md-4"><p><?=$v->getSize($v->size);?></p></div>
                                                    </div>
                                                <? endif;?>
                                                <? if($v->color != null):?>
                                                    <div class="row align-items-end mt-2">
                                                        <div class="col-sm-12 col-md-2"><p><?=Yii::t('app', 'Color');?></p></div>
                                                        <div class="col-sm-12 col-md-6 line"></div>
                                                        <div class="col-sm-12 col-md-4"><p><?=$v->getColor($v->color);?></p></div>
                                                    </div>
                                                <? endif;?>
                                                <div class="row mt-4">
                                                    <div class="col-sm-12 col-md-3 inc-dec">
                                                        <input type="button" class="decrease" onclick="decreaseProduct(<?=$v->id;?>, <?=$v->color != null ? $v->color:0;?>, <?=$v->size != null ? $v->size:0;?>)">
                                                        <p class="output" id="countProduct<?=$v->id.''.$v->color.''.$v->size;?>"><?=$v->count;?></p>
                                                        <input type="button" class="increase" onclick="increaseProduct(<?=$v->id;?>, <?=$v->color != null ? $v->color:0;?>, <?=$v->size != null ? $v->size:0;?>)">
                                                    </div>
                                                    <div class="col-sm-12 col-md-9"></div>
                                                </div>

                                            </div>

                                        </div>
                                        <div class="col-md-3">
                                            <div class="price-look">
                                                <? if($v->newPrice != null):?>
                                                    <h3 class="previous-price"><?=$v->price;?> тг.</h3>
                                                    <h2 class="sale-price"><?=$v->newPrice;?> тг.</h2>
                                                <? endif;?>
                                                <? if($v->newPrice == null):?>
                                                    <h2 class="sale-price"><?=$v->price;?> тг.</h2>
                                                <? endif;?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10"></div>
                                        <div class="col-md-2">
                                            <div class="add">
                                                <div class="to-trash" onclick="deleteProductFromBasket(<?=$v->id;?>, <?=$v->color != null ? $v->color:0;?>, <?=$v->size != null ? $v->size:0;?>)"
                                                     data-title-msg = "<?=Yii::t('app', 'Are you sure?');?>" data-text-msg = "<?=Yii::t('app', 'you want to delete this item!');?>"
                                                     data-cancel-msg="<?=Yii::t('app', 'Cancel');?>">
                                                    <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M2.75 5.5H4.58333H19.25" stroke="#AAAABE" stroke-width="1.8" stroke-linecap="round" stroke-linejoin="round"/>
                                                        <path d="M7.3335 5.50004V3.66671C7.3335 3.18048 7.52665 2.71416 7.87047 2.37034C8.21428 2.02653 8.6806 1.83337 9.16683 1.83337H12.8335C13.3197 1.83337 13.786 2.02653 14.1299 2.37034C14.4737 2.71416 14.6668 3.18048 14.6668 3.66671V5.50004M17.4168 5.50004V18.3334C17.4168 18.8196 17.2237 19.2859 16.8799 19.6297C16.536 19.9736 16.0697 20.1667 15.5835 20.1667H6.41683C5.9306 20.1667 5.46428 19.9736 5.12047 19.6297C4.77665 19.2859 4.5835 18.8196 4.5835 18.3334V5.50004H17.4168Z" stroke="#AAAABE" stroke-width="1.8" stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>
                                                </div>
                                                <? if(!Yii::$app->user->isGuest):?>
                                                    <div class="to-favorite <?=$v->getFavoriteStatus() ? 'activeFavorite':'';?>" id="heart_for_favorite<?=$v->id;?>" onclick="addProductToFavoriteFromBasket(<?=$v->id;?>)">
                                                        <svg width="22" height="20" viewBox="0 0 22 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M10.8882 1.86775L11.0656 2.00475L11.2472 1.87353C12.4544 1.00163 13.9579 0.527488 15.462 0.527488C17.0612 0.527488 18.6246 1.17739 19.7511 2.33146C22.3501 5.05929 22.3484 9.34744 19.7534 12.0358L19.7194 12.0695L12.3149 19.1577L12.3149 19.1577L12.3109 19.1615C11.588 19.8795 10.4024 19.8795 9.67951 19.1615L9.67954 19.1615L9.67614 19.1582L2.27358 12.0341C-0.359859 9.3058 -0.356632 5.02018 2.2759 2.33317L2.27598 2.33309C3.55608 1.02549 5.2733 0.3 7.10108 0.3C8.27009 0.3 9.54292 0.828637 10.8882 1.86775ZM17.998 10.3403L18.0041 10.3345L18.0098 10.3283C19.6452 8.58246 19.6455 5.82337 17.9698 4.07729C17.2971 3.36787 16.3378 2.99858 15.3857 2.99858H15.3475V2.99844L15.3385 2.99871C14.0032 3.0389 12.7854 3.56244 11.8528 4.48856C11.359 4.97901 10.5551 4.97901 10.0613 4.48856L10.0613 4.48852L10.0575 4.48488C9.49219 3.94281 8.94799 3.51856 8.44388 3.22819C7.94361 2.94003 7.46096 2.77109 7.02473 2.77109C5.87474 2.77109 4.80663 3.22076 3.98817 4.03358L3.98804 4.03345L3.98079 4.04117C2.30846 5.82349 2.30515 8.58325 3.98316 10.331L3.98295 10.3312L3.99248 10.3403L10.7881 16.8237L10.9952 17.0213L11.2023 16.8237L17.998 10.3403Z" fill="#AAAABE" stroke="white" stroke-width="0.6"/>
                                                        </svg>
                                                    </div>
                                                <? endif;?>

                                                <? if(Yii::$app->user->isGuest):?>
                                                    <a href="#" data-toggle="modal" data-target="#login">
                                                        <div class="to-favorite"  >
                                                            <svg width="22" height="20" viewBox="0 0 22 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                <path d="M10.8882 1.86775L11.0656 2.00475L11.2472 1.87353C12.4544 1.00163 13.9579 0.527488 15.462 0.527488C17.0612 0.527488 18.6246 1.17739 19.7511 2.33146C22.3501 5.05929 22.3484 9.34744 19.7534 12.0358L19.7194 12.0695L12.3149 19.1577L12.3149 19.1577L12.3109 19.1615C11.588 19.8795 10.4024 19.8795 9.67951 19.1615L9.67954 19.1615L9.67614 19.1582L2.27358 12.0341C-0.359859 9.3058 -0.356632 5.02018 2.2759 2.33317L2.27598 2.33309C3.55608 1.02549 5.2733 0.3 7.10108 0.3C8.27009 0.3 9.54292 0.828637 10.8882 1.86775ZM17.998 10.3403L18.0041 10.3345L18.0098 10.3283C19.6452 8.58246 19.6455 5.82337 17.9698 4.07729C17.2971 3.36787 16.3378 2.99858 15.3857 2.99858H15.3475V2.99844L15.3385 2.99871C14.0032 3.0389 12.7854 3.56244 11.8528 4.48856C11.359 4.97901 10.5551 4.97901 10.0613 4.48856L10.0613 4.48852L10.0575 4.48488C9.49219 3.94281 8.94799 3.51856 8.44388 3.22819C7.94361 2.94003 7.46096 2.77109 7.02473 2.77109C5.87474 2.77109 4.80663 3.22076 3.98817 4.03358L3.98804 4.03345L3.98079 4.04117C2.30846 5.82349 2.30515 8.58325 3.98316 10.331L3.98295 10.3312L3.99248 10.3403L10.7881 16.8237L10.9952 17.0213L11.2023 16.8237L17.998 10.3403Z" fill="#AAAABE" stroke="white" stroke-width="0.6"/>
                                                            </svg>
                                                        </div>
                                                    </a>
                                                <? endif;?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <? if(count($_SESSION['basket']) != $m):?>
                            <div class="separator mt-4 mb-4"></div>
                        <? endif;?>
                    <? endforeach;?>
                </div>
                <? endif;?>
            </div>
            <div class="col-sm-12 col-md-4 basket-block checked-products">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#personal-date"><?=Yii::t('app', 'Personal data');?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#personal-payment"><?=Yii::t('app', 'Payment and delivery');?></a>
                    </li>
                </ul>
                <div class="tab-content container">
                    <div class="tab-pane container active tab-pane-1" id="personal-date">
                        <form>
                            <span> <?=Yii::t('app', 'Required field*');?></span>
                            <input type="text"  placeholder="<?=Yii::t('app', 'Enter your full name (full name)');?>" id="fio"
                                   value="<?=Yii::$app->user->isGuest ? '' : $user->name.' '.$user->surname.' '.$user->father;?>" data-fio-msg="<?=Yii::t('app', 'It is necessary to fill in the "name".');?>">
                            <span> <?=Yii::t('app', 'Required field*');?></span>
                            <input type="text"  placeholder="<?=Yii::t('app', 'Enter phone number');?>" name="UserProfile[telephone]" id="telephone"
                                   value="<?=Yii::$app->user->isGuest ? '' : $user->telephone?>" data-telephone-msg="<?=Yii::t('app', 'You must fill in the "Phone".');?>">
                            <input type="text" class="email-input"  placeholder="<?=Yii::t('app', 'Enter e-mail address');?>" id="email"
                                   value="<?=Yii::$app->user->isGuest ? '' : Yii::$app->user->identity->email;?>" data-email-msg="<?=Yii::t('app', 'E-mail is not a valid email address.');?>">
                        </form>

                        <div class="strip"></div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <div class="basket_wrapper">
                                    <p><?=Yii::t('app', 'Cost');?></p>
                                    <p><span class="sumProduct"><?=$sumProduct;?></span> тг.</p>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-12">
                                <div class="basket_wrapper">
                                    <p><?=Yii::t('app', 'Cost of delivery');?></p>
                                    <p><span class="deliveryPrice"><?=$deliveryPrice;?></span> тг.</p>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12">
                                <div class="basket_wrapper">
                                    <p><?=Yii::t('app', 'Money transfer -4%');?></p>
                                    <p><span class="percent"><?=$percent;?></span> тг.</p>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-12">
                                <div class="basket_wrapper">
                                    <p><?=Yii::t('app', 'Total');?></p>
                                    <p><span class="sum"><?=$sum;?></span> тг.</p>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-sm-12 col-md-12 ">
                                <a class="nextBtn" href="#"><?=Yii::t('app', 'Next step');?> <img src="/public/img/arrow-left.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane container fade" id="personal-payment">
                        <form>
                            <p><?=Yii::t('app', 'Locality');?><span> <?=Yii::t('app', 'Required field*');?></span></p>
                            <select id = 'addressA' onchange="updateDeliveryPrice($(this).val())" data-locality-msg="<?=Yii::t('app', 'It is necessary to fill in the “locality”.');?>">
                                <? foreach (Yii::$app->view->params['delivery-address'] as $v):?>
                                    <option value="<?=$v->id;?>" <?=$user->locality_id == $v->id ? 'selected':'';?>><?=$v->city;?></option>
                                <? endforeach;?>
                            </select>
                            <p><?=Yii::t('app', 'Delivery address');?><span> <?=Yii::t('app', 'Required field*');?></span></p>
                            <input type="text"  placeholder="<?=Yii::t('app', 'Street/ house/ flat');?>" id="addressB"
                                    value="<?=Yii::$app->user->isGuest ? '' : $user->address?>" data-address-msg="<?=Yii::t('app', 'You must fill in the "Delivery Address".');?>">
                            <p><?=Yii::t('app', 'Comment on the order');?></p>
                            <textarea  cols="30" rows="5" id="other_information"
                                      placeholder="<?=Yii::t('app', 'Additional Information');?>"></textarea>
                        </form>
                        <p><?=Yii::t('app', 'Payment Methods');?></p>
                        <div class="checked-payment" data-method-msg="<?=Yii::t('app', 'Select "Payment Methods."');?>">
                            <a style="cursor: pointer"  class="payment-block" id="cash_to_courier" data-status = "0" title="<?=Yii::t('app', 'Cash to courier');?>"
                            ><?=Yii::t('app', 'Cash to courier');?> 
                            <img src="/public/img/Vector.png">
                            </a>
<!--                            <a style="cursor: pointer"  class="payment-block" id="banking_card" data-status = "0" title="--><?//=Yii::t('app', 'By credit card');?><!--">--><?//=Yii::t('app', 'By credit card');?><!-- </a>-->
                        </div>
                        <div class="strip"></div>

                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <div class="basket_wrapper">
                                    <p><?=Yii::t('app', 'Cost');?></p>
                                    <p><span class="sumProduct"><?=$sumProduct;?></span> тг.</p>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-12">
                                <div class="basket_wrapper">
                                    <p><?=Yii::t('app', 'Cost of delivery');?></p>
                                    <p><span class="deliveryPrice"><?=$deliveryPrice;?></span> тг.</p>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12">
                                <div class="basket_wrapper">
                                    <p><?=Yii::t('app', 'Money transfer -4%');?></p>
                                    <p><span class="percent"><?=$percent;?></span> тг.</p>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-12">
                                <div class="basket_wrapper">
                                    <p><?=Yii::t('app', 'Total');?></p>
                                    <p><span class="sum"><?=$sum;?></span> тг.</p>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-sm-12 col-md-12">
                                <input type="button" class="checkout" value="<?=Yii::t('app', 'Go to the payment');?>" onclick="checkout()">
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-sm-12 col-md-12">
                                <a href="#">
                                    <input type="button" class="previousBtn back" value="<?=Yii::t('app', 'Previous step');?>">
                                </a>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

</div>


