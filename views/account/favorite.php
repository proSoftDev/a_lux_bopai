<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11.10.2019
 * Time: 15:24
 */

 ?>

<div class="history">
    <div class="container mt-5 mb-5">
        <div class="row">
            <div class="col-sm-12 col-md-3">
                <nav class="switch-nav">
                    <ul>
                        <li><a href="/account/"><?=Yii::t('app', 'Personal data');?></a></li>
                        <li><a href="/account/favorite"><?=Yii::t('app', 'Favorites');?></a></li>
                        <li class="switch-nav_active"><a><?=Yii::t('app', 'Order History');?></a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-sm-12 col-md-1"></div>
            <div class="col-sm-12 col-md-8">
                <div class="main-page favorites">
                        <div class="cont_tov">
                            <div class="tov_item">
                                <button class="delete-products">
                                    <span>&times;</span>
                                </button>
                                <a href="/product?id=13" class="tov_item_img">
                                    <img src="/uploads/images/product/1594364125_WhatsApp Image 2020-07-08 at 18.26.25 (1).jpeg"
                                        alt="">
                                </a>
                                <a href="/product?id=13" class="tov-button">Подробнее</a>
                                <div class="tov_item_info">
                                    <div class="tov_new">Новинка!</div>
                                    <p>АААААА</p>
                                    <div class="price">5700<span>тг</span></div>
                                </div>
                            </div>
                            <div class="tov_item">
                                <button class="delete-products">
                                    <span>&times;</span>
                                </button>
                                <a href="/product?id=12" class="tov_item_img">
                                    <img src="/uploads/images/product/1594212471_WhatsApp Image 2020-07-08 at 18.26.25 (1).jpeg"
                                        alt="">
                                </a>
                                <a href="/product?id=12" class="tov-button">Подробнее</a>
                                <div class="tov_item_info">
                                    <div class="tov_new">Новинка!</div>
                                    <div class="tov_new tov_pov">Популярное!</div>
                                    <div class="tov_new tov_exs">Эксклюзив!</div>
                                    <p>Велосипкдка</p>
                                    <div class="price">5000<span>тг</span></div>
                                </div>
                            </div>
                            <div class="tov_item">
                                <button class="delete-products">
                                    <span>&times;</span>
                                </button>
                                <a href="/product?id=11" class="tov_item_img">
                                    <img src="/uploads/images/product/1594211421_WhatsApp Image 2020-07-08 at 18.19.59 (1).jpeg"
                                        alt="">
                                </a>
                                <a href="/product?id=11" class="tov-button">Подробнее</a>
                                <div class="tov_item_info">
                                    <div class="tov_new">Новинка!</div>
                                    <div class="tov_new tov_pov">Популярное!</div>
                                    <p>Летняя платье</p>
                                    <div class="old_price">5700<span>тг</span></div>
                                    <div class="price">3000<span>тг</span></div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>