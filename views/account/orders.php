<div class="history">
    <div class="container mt-5 mb-5">
        <div class="row">
            <div class="col-sm-12 col-md-3">
                <nav class="switch-nav">
                    <ul>
                        <li><a href="/account/"><?=Yii::t('app', 'Personal data');?></a></li>
                        <li><a href="/account/favorite"><?=Yii::t('app', 'Favorites');?></a></li>
                        <li class="switch-nav_active"><a ><?=Yii::t('app', 'Order History');?></a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-sm-12 col-md-1"></div>
            <div class="col-sm-12 col-md-8">

                <table class="order-table"  cellspacing="5">
                    <thead>
                    <tr>
                        <th class="date-header" colspan="1"><?=Yii::t('app', 'date');?></th>
                        <th class="count-header" colspan="1"><?=Yii::t('app', 'Order');?></th>
                        <th class="cost-header" colspan="1"><?=Yii::t('app', 'Cost');?></th>
                        <th class="status-header" colspan="1"><?=Yii::t('app', 'Status');?></th>
                        <th class="repeat-header" colspan="1" style="opacity: 0;"><?=Yii::t('app', 'Repeat order');?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <? if(count($orders) != null):?>
                    <? foreach ($orders as $v):?>
                        <tr>
                            <td>
                                <p class="date"><?=$v->getDate();?></p>
                            </td>
                            <td>
                                <p class="count" data-placement="bottom" data-toggle="popover<?=$v->id;?>"><span><?=$v->countProduct;?></span> <?=Yii::t('app', 'product');?></p>
                            </td>
                            <td>
                                <p class="cost"><?=$v->sum;?> тг.</p>
                            </td>
                            <td>
                                <?= \app\modules\admin\status\LabelDelivery::statusLabel($v->status_delivery);?>
                            </td>
                            <td>
                                <a href="/account/add-basket-from-orders?order_id=<?=$v->id;?>"><button type="button" class="repeat" ><?=Yii::t('app', 'Repeat order');?></button></a>
                            </td>
                        </tr>
                    <? endforeach;?>
                    <? endif;?>
                    </tbody>
                </table>

                <div class="hidden__mobile__products-card">

                    <? foreach ($orders as $v):?>
                        <div class="mob__product-card" data-placement="bottom" data-toggle="popover">
                            <div class="info">
                                <div class="text">
                                    <div class="qty"><?=$v->countProduct;?> <?=Yii::t('app', 'product');?></div>
                                    <div class="sum"><?=$v->sum;?> тг</div>
                                    <div class="date"><?=$v->getDate();?></div>
                                </div>
                                <div class="repeat-cont">
                                    <a href="/account/add-basket-from-orders?order_id=<?=$v->id;?>">
                                        <button type="button" class="repeat"><?=Yii::t('app', 'Repeat order');?></button>
                                    </a>
                                </div>
                            </div>
                            <div class="strip"></div>
                            <div class="status">
                                <div class="new"><?=Yii::t('app', 'New');?></div>
                            </div>
                        </div>
                    <? endforeach;?>

                </div>
            </div>
        </div>
    </div>
</div>

<script>
    <? foreach ($orders as $v):?>
    <? $products = $v->products;?>
    $(document).ready(function() {
        let content = [`<div class="container">
                          <div class="row">
                            <? foreach ($products as $val):?>
                            <div class="col-sm-12 col-md-12 pl-0 pr-0">
                             <div>
                              <img width=50  src="<?=$val->product->getImage();?>">
                              <p class="product-name"><?=$val->product->name;?></p>
                             </div>
                              <p class="product-count"><?=$val->count;?> <?=Yii::t('app', 'pс.');?></p>
                              <p class="product-cost"><?=$val->count*$val->product->getPrice();?> тг</p>
                            </div>
                            <? endforeach;?>

                          </div>
                        </div>`].join();
        $('[data-toggle="popover<?=$v->id;?>"]').popover({
            trigger: 'hover',
            content: content,
            placement: "bottom",

            html: true
        });
    });
    <? endforeach;?>
</script>