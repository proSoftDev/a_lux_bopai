<div class="personal">
    <div class="container mt-5 mb-5">
        <div class="row">
            <div class="col-sm-12 col-md-3">
                <nav class="switch-nav">
                    <ul>
                        <li class="switch-nav_active"><a><?=Yii::t('app', 'Personal data');?></a></li>
                        <li><a href="/account/orders"><?=Yii::t('app', 'Favorites');?></a></li>
                        <li><a href="/account/orders"><?=Yii::t('app', 'Order History');?></a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-sm-12 col-md-1"></div>
            <div class="col-sm-12 col-md-5 personal-form">
                <div class="private-data">
                    <form>
                        <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
                        <input type="text" placeholder="<?=Yii::t('app', 'Name');?>" value="<?=$userProfile->name;?>" name="UserProfile[name]">
                        <input type="text" placeholder="<?=Yii::t('app', 'Surname');?>" value="<?=$userProfile->surname;?>" name="UserProfile[surname]">
                        <input type="text" placeholder="<?=Yii::t('app', 'Father');?>" value="<?=$userProfile->father;?>" name="UserProfile[father]">
                        <div class="separator mt-3 mb-3"></div>
                        <input type="tel" pattern="[8]{1}[0-9]{3}[0-9]{3}[0-9]{2}[0-9]{2}" value="<?=$userProfile->telephone;?>" placeholder="<?=Yii::t('app', 'Phone number');?>" name="UserProfile[telephone]" class="profile-tel"
                               data-error="<?=Yii::t('app', 'You must fill in the "Phone".');?>">
                        <input type="email" placeholder="<?=Yii::t('app', 'Enter e-mail address');?>" name="User[email]" value="<?=$user->email;?>" disabled>
                        <div class="separator mt-3 mb-3"></div>
                        <select name="UserProfile[locality_id]">
                            <? foreach (Yii::$app->view->params['delivery-address'] as $v):?>
                                <option value="<?=$v->id;?>" <?=$userProfile->locality_id == $v->id ? 'selected':'';?>><?=$v->city;?></option>
                            <? endforeach;?>
                        </select>
                        <input type="text" placeholder="<?=Yii::t('app', 'Address');?>" name="UserProfile[address]" value="<?=$userProfile->address;?>">
                        <div class="separator mt-3 mb-3"></div>
                        <input type="password" placeholder="<?=Yii::t('app', 'New Password');?>" name="User[password]" class="profile-password">
                        <input type="password" placeholder="<?=Yii::t('app', 'Password confirmation');?>" name="User[password_verify]" class="profile-password-verify">
                        <div class="save">
                            <input type="button" value="<?=Yii::t('app', 'Save');?>" id="save-profile"
                                   data-success-msg="<?=Yii::t('app', 'Your changes have been saved successfully.');?>">
                            <p><?=Yii::t('app', 'By clicking the “Save” button you accept the terms');?></p>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-12 col-md-3 void"></div>
        </div>
    </div>
</div>