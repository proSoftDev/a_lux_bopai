<nav id="catalog-nav">
    <ul class="first-nav">
        <? foreach (Yii::$app->view->params['catalog'] as $v):?>
            <? $category = $v->category;?>
            <li>
                <span><?=$v->getName();?></span>
                <? if($category != null):?>
                <ul>
                    <? foreach ($category as $v2):?>
                        <? $subCategory = $v2->category;?>
                        <li>
                            <a href="/catalog?id=<?=$v2->id;?>"><?=$v2->getName();?></a>
                            <? if($subCategory != null):?>
                            <ul>
                                <? foreach ($subCategory as $v3):?>
                                    <li><a href="/catalog?id=<?=$v3->id;?>"><?=$v3->getName();?></a></li>
                                <? endforeach;?>
                            </ul>
                            <? endif;?>
                        </li>
                    <? endforeach;?>
                </ul>
                <? endif;?>
            </li>
        <? endforeach;?>


        <li class="inner-nav">
            <span>Навигация</span>
            <ul>
                <? foreach ( Yii::$app->view->params['headerMenu'] as $v):?>
                    <li>
                        <a href="<?=$v->url;?>"><?=$v->getText();?></a>
                    </li>
                <? endforeach;?>
            </ul>
        </li>

    </ul>
</nav>
<a class="toggle">
    <span></span>
</a>