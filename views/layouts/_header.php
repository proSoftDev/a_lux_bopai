

<div class="header" id="header">
    <div class="top-menu">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-2 w-50">
                    <select class="country js-states" name="lang" onchange="location = this.options[this.selectedIndex].value;">
                        <option value="/lang/?url=kz" <?=(Yii::$app->session["lang"]=="_kz")?'selected':''?>>Қазақша</option>
                        <option value="/lang/?url=ru"  <?=(Yii::$app->session["lang"]=="")?'selected':''?>>Русский</option>
                    </select>
                </div>
                <div class="col-sm-12 col-md-7 top-navv">
                    <nav>
                        <ul class="top-nav">
                            <? foreach ( Yii::$app->view->params['headerMenu']  as $v):?>
                                <li>
                                    <a href="<?=$v->url;?>"><?=$v->getText();?></a>
                                </li>
                            <? endforeach;?>
                        </ul>
                    </nav>
                </div>
                <div class="col-sm-6 col-md-3 text-right signInOrRegister w-50">
                    <? if(Yii::$app->user->isGuest):?>
                        <p>
                            <a href="#" data-toggle="modal" data-target="#login" id="log-in"><?=Yii::t('app', 'sign in');?></a> <span>/</span>
                            <a href="#" data-toggle="modal" data-target="#registration" id="registerr"><?=Yii::t('app', 'sign up');?></a>
                        </p>
                    <? endif;?>
                    <? if(!Yii::$app->user->isGuest):?>
                        <p>
                            <a href="/account/" ><?=Yii::$app->view->params['footerMenu'][8]->getText();?></a> <span>/</span>
                            <a href="/site/logoff" ><?=Yii::t('app', 'log out');?></a>
                        </p>
                    <? endif;?>
                </div>

            </div>
        </div>
    </div>
    <div class="bottom-menu pt-3 pb-4">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-2 stStatic">
                    <a href="/">
                        <img src="/public/img/logo.png" alt="Логотип">
                    </a>
                    <? if($_SERVER['REQUEST_URI'] != '/site/index' && $_SERVER['REQUEST_URI'] != '/' || preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"])):?>
                        <?=$this->render("_catalog");?>
                    <? endif;?>
                </div>
                <div class="col-sm-12 col-md-6">

                    <form action="/search">
                        <input class="awesomplete search p-3" list="mylist" data-list="#mylist" placeholder="<?=Yii::t('app', 'Search on the site');?>"  name="keyword" />
                        <datalist id="mylist">
                            <? foreach (Yii::$app->view->params['product']  as $v):?>
                                <option><?=$v->getName();?></option>
                            <? endforeach;?>
                        </datalist>

                        <input class="search-btn" type="submit" value="">
                    </form>
                </div>
                <div class="col-sm-6 col-md-2 d-flex align-items-center pr-0 w-50">
                    <? if(Yii::$app->user->isGuest):?>
                        <a href="#" data-toggle="modal" data-target="#login" class="favorite-link">
                            <svg width="22" height="20" viewBox="0 0 22 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M10.8882 1.86775L11.0656 2.00475L11.2472 1.87353C12.4544 1.00163 13.9579 0.527488 15.462 0.527488C17.0612 0.527488 18.6246 1.17739 19.7511 2.33146C22.3501 5.05929 22.3484 9.34744 19.7534 12.0358L19.7194 12.0695L12.3149 19.1577L12.3149 19.1577L12.3109 19.1615C11.588 19.8795 10.4024 19.8795 9.67951 19.1615L9.67954 19.1615L9.67614 19.1582L2.27358 12.0341C-0.359859 9.3058 -0.356632 5.02018 2.2759 2.33317L2.27598 2.33309C3.55608 1.02549 5.2733 0.3 7.10108 0.3C8.27009 0.3 9.54292 0.828637 10.8882 1.86775ZM17.998 10.3403L18.0041 10.3345L18.0098 10.3283C19.6452 8.58246 19.6455 5.82337 17.9698 4.07729C17.2971 3.36787 16.3378 2.99858 15.3857 2.99858H15.3475V2.99844L15.3385 2.99871C14.0032 3.0389 12.7854 3.56244 11.8528 4.48856C11.359 4.97901 10.5551 4.97901 10.0613 4.48856L10.0613 4.48852L10.0575 4.48488C9.49219 3.94281 8.94799 3.51856 8.44388 3.22819C7.94361 2.94003 7.46096 2.77109 7.02473 2.77109C5.87474 2.77109 4.80663 3.22076 3.98817 4.03358L3.98804 4.03345L3.98079 4.04117C2.30846 5.82349 2.30515 8.58325 3.98316 10.331L3.98295 10.3312L3.99248 10.3403L10.7881 16.8237L10.9952 17.0213L11.2023 16.8237L17.998 10.3403Z" fill="#757482" stroke="white" stroke-width="0.6"/>
                            </svg>
                        </a>
                    <? endif;?>
                    <? if(!Yii::$app->user->isGuest):?>
                    <a class="favorite-link" href="/account/favorite">
                        <svg width="22" height="20" viewBox="0 0 22 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill="red" d="M10.8882 1.86775L11.0656 2.00475L11.2472 1.87353C12.4544 1.00163 13.9579 0.527488 15.462 0.527488C17.0612 0.527488 18.6246 1.17739 19.7511 2.33146C22.3501 5.05929 22.3484 9.34744 19.7534 12.0358L19.7194 12.0695L12.3149 19.1577L12.3149 19.1577L12.3109 19.1615C11.588 19.8795 10.4024 19.8795 9.67951 19.1615L9.67954 19.1615L9.67614 19.1582L2.27358 12.0341C-0.359859 9.3058 -0.356632 5.02018 2.2759 2.33317L2.27598 2.33309C3.55608 1.02549 5.2733 0.3 7.10108 0.3C8.27009 0.3 9.54292 0.828637 10.8882 1.86775ZM17.998 10.3403L18.0041 10.3345L18.0098 10.3283C19.6452 8.58246 19.6455 5.82337 17.9698 4.07729C17.2971 3.36787 16.3378 2.99858 15.3857 2.99858H15.3475V2.99844L15.3385 2.99871C14.0032 3.0389 12.7854 3.56244 11.8528 4.48856C11.359 4.97901 10.5551 4.97901 10.0613 4.48856L10.0613 4.48852L10.0575 4.48488C9.49219 3.94281 8.94799 3.51856 8.44388 3.22819C7.94361 2.94003 7.46096 2.77109 7.02473 2.77109C5.87474 2.77109 4.80663 3.22076 3.98817 4.03358L3.98804 4.03345L3.98079 4.04117C2.30846 5.82349 2.30515 8.58325 3.98316 10.331L3.98295 10.3312L3.99248 10.3403L10.7881 16.8237L10.9952 17.0213L11.2023 16.8237L17.998 10.3403Z" stroke="white" stroke-width="0.6"></path>
                        </svg>
                    </a>
                    <? endif;?>
                    <a href="/account/favorite" class="favorite"><?=Yii::t('app', 'Favorites');?>
                        <span><?=count(Yii::$app->view->params['favorites']) == null ? Yii::t('app', 'No favorites'):count(Yii::$app->view->params['favorites']).' '.Yii::t('app', 'product');;?></span>
                    </a>
                </div>

                <div class="col-sm-6 col-md-2 d-flex align-items-center pr-0 w-50">
                    <a class="trash-link" href="/card/">
                        <svg width="22" height="24" viewBox="0 0 22 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M21.0571 5.85366H16.9714V5.56098C16.9714 4.08611 16.3423 2.67166 15.2224 1.62877C14.1026 0.585887 12.5837 0 11 0C9.41628 0 7.89742 0.585887 6.77756 1.62877C5.6577 2.67166 5.02857 4.08611 5.02857 5.56098V5.85366H0.942857C0.692796 5.85366 0.452977 5.94617 0.276157 6.11083C0.099337 6.2755 0 6.49884 0 6.73171V20.7805C0 21.6344 0.364233 22.4533 1.01257 23.057C1.66091 23.6608 2.54025 24 3.45714 24H18.5429C19.4597 24 20.3391 23.6608 20.9874 23.057C21.6358 22.4533 22 21.6344 22 20.7805V6.73171C22 6.49884 21.9007 6.2755 21.7238 6.11083C21.547 5.94617 21.3072 5.85366 21.0571 5.85366ZM6.91429 5.56098C6.91429 4.55186 7.34474 3.58407 8.11096 2.87052C8.87718 2.15697 9.9164 1.7561 11 1.7561C12.0836 1.7561 13.1228 2.15697 13.889 2.87052C14.6553 3.58407 15.0857 4.55186 15.0857 5.56098V5.85366H6.91429V5.56098ZM20.1143 20.7805C20.1143 21.1686 19.9487 21.5408 19.654 21.8153C19.3593 22.0897 18.9596 22.2439 18.5429 22.2439H3.45714C3.04037 22.2439 2.64068 22.0897 2.34598 21.8153C2.05128 21.5408 1.88571 21.1686 1.88571 20.7805V7.60976H5.02857V10.2439C5.02857 10.4768 5.12791 10.7001 5.30473 10.8648C5.48155 11.0294 5.72137 11.122 5.97143 11.122C6.22149 11.122 6.46131 11.0294 6.63813 10.8648C6.81495 10.7001 6.91429 10.4768 6.91429 10.2439V7.60976H15.0857V10.2439C15.0857 10.4768 15.1851 10.7001 15.3619 10.8648C15.5387 11.0294 15.7785 11.122 16.0286 11.122C16.2786 11.122 16.5185 11.0294 16.6953 10.8648C16.8721 10.7001 16.9714 10.4768 16.9714 10.2439V7.60976H20.1143V20.7805Z" fill="#DD2B1C"/>
                        </svg>

                    </a>
                    <a href="/card/" class="trash"><?=Yii::$app->view->params['footerMenu'][9]->getText();?>
                        <span id="header_basket"><?=count($_SESSION['basket']) == 0 ? Yii::t('app', 'your basket is empty'):count($_SESSION['basket']).' '.Yii::t('app', 'product');?></span>
                    </a>
                </div>

            </div>
        </div>
    </div>
</div>