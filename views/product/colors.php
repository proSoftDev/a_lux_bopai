
<? if($colors):?>
    <? foreach ($colors as $k=>$v):?>
        <option data-id="<?=$v->color_id;?>" <?=($product->getQuantityByColor($v->color_id)) == 0 ? 'disabled = "disabled"':'';?>
            <?=$v->color_id == $selected_color_id ? "selected":"";?>><?=$product->getColor($v->color_id);?></option>
    <? endforeach;?>
<? endif;?>