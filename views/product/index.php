<div class="product-item">
    <div class="container mt-4">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <p class="main-page-link"><a href="/"><?=Yii::$app->view->params['main']->getText();?></a>
                    <? for($i=count($breadcrumbs)-1;$i>=0;$i--):?>
                        <img src="/public/img/_.png">
                        <a href="/catalog?id=<?=$breadcrumbs[$i]["id"];?>"><?=$breadcrumbs[$i]["name"];?></a>
                    <? endfor;?>
                    <img src="/public/img/_.png"> <?=$product->getName();?>
                </p>
            </div>
        </div>
    </div>
    <div class="container mt-5 mb-5 pb-5 separator">
        <div class="row">
            <div class="col-sm-12 col-md-5 product-photo">
                <div class="container">
                    <div class="row" id="colorResult">
                        <div class="col-md-4 col-sm-12">
                            <div class="slider slider-nav">
                                <? foreach ($product->images as $v):?>
                                    <div>
                                        <img style="width: 100px;height: 130px;" src="<?=$v->getImage()?>">
                                    </div>
                                <? endforeach;?>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-12 mt-auto mb-auto">
                            <div class="slider slider-for">
                                <? foreach ($product->images as $v):?>
                                    <div>
                                        <img src="<?=$v->getImage()?>">
                                    </div>
                                <? endforeach;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-7">
                <div class="container">
                    <div class="row align-items-start">
                        <div class="col-sm-12 col-md-7 general-info">
                            <h1><?=$product->getName();?></h1>
                            <div class="container mt-3 pl-0 pr-0">

                                <div class="row" id="ratingResult">
                                    <div class="col-sm-12 col-md-5 star-rating pr-0">
                                        <div class="star-rating__wrap" >
                                            <? if(Yii::$app->user->isGuest):?><a href="#" data-toggle="modal" data-target="#login"><? endif;?>
                                            <input class="star-rating__input" id="star-rating-5" type="radio" name="rating" value="5" <?=Yii::$app->user->isGuest ? 'disabled':'';?> <?=($product->rating > 4 && $product->rating <= 5) ? 'checked':''; ?> <? if(!Yii::$app->user->isGuest):?> onclick="addUserRating(5, <?=$product->id;?>)" <? endif;?>>
                                            <label class="star-rating__ico star-rating__hover fa fa-star-o fa-lg" for="star-rating-5" title="<?=Yii::t('app', 'Product Rating {0} of 5',$product->rating);?>"></label>
                                            <input class="star-rating__input" id="star-rating-4" type="radio" name="rating" value="4" <?=Yii::$app->user->isGuest ? 'disabled':'';?> <?=($product->rating > 3 && $product->rating <= 4) ? 'checked':''; ?> <? if(!Yii::$app->user->isGuest):?> onclick="addUserRating(4, <?=$product->id;?>)" <? endif;?>>
                                            <label class="star-rating__ico star-rating__hover fa fa-star-o fa-lg" for="star-rating-4" title="<?=Yii::t('app', 'Product Rating {0} of 5',$product->rating);?>"></label>
                                            <input class="star-rating__input" id="star-rating-3" type="radio" name="rating" value="3" <?=Yii::$app->user->isGuest ? 'disabled':'';?> <?=($product->rating > 2 && $product->rating <= 3) ? 'checked':''; ?> <? if(!Yii::$app->user->isGuest):?> onclick="addUserRating(3, <?=$product->id;?>)" <? endif;?>>
                                            <label class="star-rating__ico star-rating__hover fa fa-star-o fa-lg" for="star-rating-3" title="<?=Yii::t('app', 'Product Rating {0} of 5',$product->rating);?>"></label>
                                            <input class="star-rating__input" id="star-rating-2" type="radio" name="rating" value="2" <?=Yii::$app->user->isGuest ? 'disabled':'';?> <?=($product->rating > 1 && $product->rating <= 2) ? 'checked':''; ?> <? if(!Yii::$app->user->isGuest):?> onclick="addUserRating(2, <?=$product->id;?>)" <? endif;?>>
                                            <label class="star-rating__ico star-rating__hover fa fa-star-o fa-lg" for="star-rating-2" title="<?=Yii::t('app', 'Product Rating {0} of 5',$product->rating);?>"></label>
                                            <input class="star-rating__input" id="star-rating-1" type="radio" name="rating" value="1" <?=Yii::$app->user->isGuest ? 'disabled':'';?>  <?=($product->rating > 0 && $product->rating <= 1) ? 'checked':''; ?> <? if(!Yii::$app->user->isGuest):?> onclick="addUserRating(1, <?=$product->id;?>)" <? endif;?>>
                                            <label class="star-rating__ico star-rating__hover fa fa-star-o fa-lg" for="star-rating-1" title="<?=Yii::t('app', 'Product Rating {0} of 5',$product->rating);?>"></label>
                                            <? if(Yii::$app->user->isGuest):?></a><? endif;?>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-3 pl-0 pr-0">
                                        <p class="reviews"><?=$product->ratingCount;?> <?=Yii::t('app', 'reviews');?></p>
                                    </div>
                                    <div class="col-sm-12 col-md-4 pl-0 pr-0">
                                        <p class="bought"><?=Yii::t('app', 'Have bought');?> <?=$product->sales_quantity;?>
                                            <?=Yii::t('app', 'time');?> </p>
                                    </div>
                                </div>

                                <div class="row characteristics">
                                    <div class="col-sm-12 col-md-12 general-info mt-5">
                                        <div class="container pl-0">
                                            <div class="row align-items-end">
                                                <div class="col-sm-12 col-md-3"><p><?=Yii::t('app', 'Availability');?></p></div>
                                                <div class="col-sm-12 col-md-6 line"></div>

                                                <? if($product->getQuantity() > 0):?>
                                                    <div class="col-sm-12 col-md-3" ><h5 id="productQuantity"><?=$product->getQuantity();?></h5></div>
                                                <? endif;?>
                                                <? if($product->getQuantity() == 0):?>
                                                    <div class="col-sm-12 col-md-3"><h5><?=Yii::t('app', 'No');?></h5></div>
                                                <? endif;?>
                                            </div>
                                            <div class="row align-items-end">
                                                <div class="col-sm-12 col-md-4"><p><?=Yii::t('app', 'Manufacturer');?></p></div>
                                                <div class="col-sm-12 col-md-5 line"></div>
                                                <div class="col-sm-12 col-md-3"><h5><?=$product->getManufacturer();?></h5></div>
                                            </div>
                                            <?=$product->getContent();?>
                                        </div>
                                    </div>
                                </div>

                                <div class="row description mt-4">
                                    <div class="col-sm-12 col-md-12">
                                        <h3><?=Yii::t('app', 'Description');?></h3>
                                        <p>
                                            <?=$product->getDescription();?>
                                        </p>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="col-sm-12 col-md-5 trash-block">
                            <div class="container">
                                <div class="row">
                                    <? if($product->newPrice != null):?>
                                        <div class="col-sm-12 col-md-12 pl-0 pr-0">
                                            <h3 class="previous-price"><?=$product->price;?> тг.</h3>
                                        </div>
                                        <div class="col-sm-12 col-md-12 pl-0 pr-0">
                                            <h2 class="sale-price"><?=$product->newPrice;?> тг.</h2>
                                        </div>
                                    <? endif;?>
                                    <? if($product->newPrice == null):?>
                                        <div class="col-sm-12 col-md-12 pl-0 pr-0">
                                            <h2 class="sale-price"><?=$product->price;?> тг.</h2>
                                        </div>
                                    <? endif;?>

                                    <div class="col-sm-12 col-md-12 pl-0 pr-0">
                                            <? if($product->catalog->lastParentAttribute == 3):?>
                                                <select onchange="getColorData($('#product-colors option:selected').attr('data-id'), <?=$product->id;?>, $('#product-sizes option:selected').attr('data-id'))" class="color js-states form-control select2-hidden-accessible"
                                                        id="product-colors" tabindex="-1" aria-hidden="true">
                                                    <? if($colors):?>
                                                        <? foreach ($colors as $v):?>
                                                            <option data-select2-id="3"></option>
                                                            <option data-id="<?=$v->id;?>" <?=($product->getQuantityByColor($v->id)) == 0 ? 'disabled = "disabled"':'';?>><?=$v->getName();?></option>
                                                        <? endforeach;?>
                                                    <? endif;?>
                                                </select>

                                                <select onchange="getSizeData($('#product-sizes option:selected').attr('data-id'), <?=$product->id;?>,$('#product-colors option:selected').attr('data-id'))" class="sizes js-states form-control select2-hidden-accessible"
                                                        id="product-sizes" tabindex="-1" aria-hidden="true">
                                                    <? if($sizes):?>
                                                        <? foreach ($sizes as $v):?>
                                                            <option data-select2-id="3"></option>
                                                            <option data-id="<?=$v->id;?>" <?=($product->getQuantityBySize($v->id)) == 0 ? 'disabled = "disabled"':'';?>><?=$v->name;?></option>
                                                        <? endforeach;?>
                                                    <? endif;?>
                                                </select>
                                            <? endif;?>

                                            <? if($product->catalog->lastParentAttribute == 2):?>
                                                <select class="color js-states form-control select2-hidden-accessible"
                                                        id="product-colors" tabindex="-1" aria-hidden="true">
                                                    <? foreach ($colors as $v):?>
                                                        <option data-select2-id="3"></option>
                                                        <option data-id="<?=$v->id;?>" <?=($product->getQuantityByColor($v->id)) == 0 ? 'disabled = "disabled"':'';?>><?=$v->getName();?></option>
                                                    <? endforeach;?>
                                                </select>
                                            <? endif;?>


                                            <div class="trash-block-btns">
                                                <? if(!$product->getQuantity() != 0):?>
                                                    <input class="addToTrash"  type="submit" value="<?=Yii::t('app', 'Add to Shopping Cart');?>" disabled>
                                                <? endif;?>
                                                <? if($product->getQuantity() != 0):?>
                                                    <? if($product->catalog->lastParentAttribute == 3):?>
                                                        <input onclick="addProductBasketThree(<?=$product->id;?>,$('#product-colors option:selected').attr('data-id'),$('#product-sizes option:selected').attr('data-id'))" class="addToTrash" type="button" value="<?=Yii::t('app', 'Add to Shopping Cart');?>"
                                                        data-color-msg="<?=Yii::t('app', 'Choose a color.');?>" data-size-msg="<?=Yii::t('app', 'Choose a size.');?>" data-title-msg="<?=Yii::t('app', 'Product added to cart!');?>"
                                                        data-cancel-msg="<?=Yii::t('app', 'Continue shopping');?>"  data-text-msg="<?=Yii::t('app', 'Checkoutt');?>">
                                                    <? endif;?>
                                                    <? if($product->catalog->lastParentAttribute == 2):?>
                                                        <input onclick="addProductBasketTwo(<?=$product->id;?>,$('#product-colors option:selected').attr('data-id'))" class="addToTrash" type="button" value="<?=Yii::t('app', 'Add to Shopping Cart');?>"
                                                               data-color-msg="<?=Yii::t('app', 'Choose a color.');?>" data-size-msg="<?=Yii::t('app', 'Choose a size.');?>" data-title-msg="<?=Yii::t('app', 'Product added to cart!');?>"
                                                               data-cancel-msg="<?=Yii::t('app', 'Continue shopping');?>"  data-text-msg="<?=Yii::t('app', 'Checkoutt');?>">
                                                    <? endif;?>
                                                    <? if($product->catalog->lastParentAttribute == 1):?>
                                                        <input onclick="addProductBasketOne(<?=$product->id;?>)" class="addToTrash" type="button" value="<?=Yii::t('app', 'Add to Shopping Cart');?>"
                                                               data-color-msg="<?=Yii::t('app', 'Choose a color.');?>" data-size-msg="<?=Yii::t('app', 'Choose a size.');?>" data-title-msg="<?=Yii::t('app', 'Product added to cart!');?>"
                                                               data-cancel-msg="<?=Yii::t('app', 'Continue shopping');?>"  data-text-msg="<?=Yii::t('app', 'Checkoutt');?>">
                                                    <? endif;?>
                                                <? endif;?>
                                                <label for="favorite" class="heart_for_favorite <?=$isFavorite == 1 ? 'activeFavorite':'';?>">
                                                    <svg width="22" height="20" viewBox="0 0 22 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M10.8882 1.86775L11.0656 2.00475L11.2472 1.87353C12.4544 1.00163 13.9579 0.527488 15.462 0.527488C17.0612 0.527488 18.6246 1.17739 19.7511 2.33146C22.3501 5.05929 22.3484 9.34744 19.7534 12.0358L19.7194 12.0695L12.3149 19.1577L12.3149 19.1577L12.3109 19.1615C11.588 19.8795 10.4024 19.8795 9.67951 19.1615L9.67954 19.1615L9.67614 19.1582L2.27358 12.0341C-0.359859 9.3058 -0.356632 5.02018 2.2759 2.33317L2.27598 2.33309C3.55608 1.02549 5.2733 0.3 7.10108 0.3C8.27009 0.3 9.54292 0.828637 10.8882 1.86775ZM17.998 10.3403L18.0041 10.3345L18.0098 10.3283C19.6452 8.58246 19.6455 5.82337 17.9698 4.07729C17.2971 3.36787 16.3378 2.99858 15.3857 2.99858H15.3475V2.99844L15.3385 2.99871C14.0032 3.0389 12.7854 3.56244 11.8528 4.48856C11.359 4.97901 10.5551 4.97901 10.0613 4.48856L10.0613 4.48852L10.0575 4.48488C9.49219 3.94281 8.94799 3.51856 8.44388 3.22819C7.94361 2.94003 7.46096 2.77109 7.02473 2.77109C5.87474 2.77109 4.80663 3.22076 3.98817 4.03358L3.98804 4.03345L3.98079 4.04117C2.30846 5.82349 2.30515 8.58325 3.98316 10.331L3.98295 10.3312L3.99248 10.3403L10.7881 16.8237L10.9952 17.0213L11.2023 16.8237L17.998 10.3403Z" fill="#757482" stroke="white" stroke-width="0.6"/>
                                                    </svg>
                                                </label>

                                                <? if(Yii::$app->user->isGuest):?>
                                                    <a href="#" data-toggle="modal" data-target="#login"><input id="favorite" type="submit" style="display: none;"></a>
                                                <? endif;?>
                                                <? if(!Yii::$app->user->isGuest):?>
                                                    <input id="favorite" type="submit" style="display: none;" onclick="addProductToFavoriteFromProduct(<?=$product->id;?>)">
                                                <? endif;?>

                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <? if($product->recommendedProducts != null):?>
    <div class="container recommended">
        <div class="row">
            <div class="col-sm-12 col-md-12 mb-5">
                <h2><?=Yii::t('app', 'We recommend it!');?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-10 mb-5">
                <div class="slider autoplay">
                    <? foreach ($product->recommendedProducts as $v):?>
                        <div>
                            <div class="product-container">
                                <div>
                                    <img src="<?=$v->product->getImage();?>">
                                    <a href="/product?id=<?=$v->product->id;?>" class="product-btn"><?=Yii::t('app', 'More details');?></a>
                                    <div class="separator"></div>
                                    <? if($v->product->isNew):?>
                                        <p class="novelty"><?=Yii::t('app', 'NEW!');?></p>
                                    <? endif;?>
                                    <? if($v->product->isExclusive):?>
                                        <p class="exclusive"><?=Yii::t('app', 'Exclusive!');?></p>
                                    <? endif;?>
                                    <p class="product-description"><?=$v->product->getName();?></p>
                                    <p class="price"><?=$v->product->newPrice != null ? $v->product->newPrice:$v->product->price;?> <span>тг.</span></p>
                                </div>
                            </div>
                        </div>
                    <? endforeach;?>
                </div>
            </div>
            <div class="col-sm-12 col-md-2"></div>
        </div>
    </div>
    <? endif;?>

    <? if($product->alsoBuysProducts != null):?>
    <div class="container recommended">
        <div class="row">
            <div class="col-sm-12 col-md-12 mb-5">
                <h2><?=Yii::t('app', 'Also buy with this');?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-10 mb-5">
                <div class="slider autoplay">
                    <? foreach ($product->alsoBuysProducts as $v):?>
                    <div>
                        <div class="product-container">
                            <div>
                                <img src="<?=$v->product->getImage();?>">
                                <a href="/product?id=<?=$v->product->id;?>" class="product-btn"><?=Yii::t('app', 'More details');?></a>
                                <div class="separator"></div>
                                <? if($v->product->isNew):?>
                                    <p class="novelty"><?=Yii::t('app', 'NEW!');?></p>
                                <? endif;?>
                                <? if($v->product->isExclusive):?>
                                    <p class="exclusive"><?=Yii::t('app', 'Exclusive!');?></p>
                                <? endif;?>
                                <p class="product-description"><?=$v->product->getName();?></p>
                                <p class="price"><?=$v->product->newPrice != null ? $v->product->newPrice:$v->product->price;?> <span>тг.</span></p>
                            </div>
                        </div>
                    </div>
                    <? endforeach;?>
                </div>
            </div>
            <div class="col-sm-12 col-md-2"></div>
        </div>
    </div>
    <? endif;?>
</div>