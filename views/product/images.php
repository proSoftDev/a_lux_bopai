<div class="col-md-4 col-sm-12">
    <div class="slider slider-nav">
        <? foreach ($images as $v):?>
            <div>
                <img style="width: 100px;height: 130px;" src="<?=$v->getImage()?>">
            </div>
        <? endforeach;?>
    </div>
</div>
<div class="col-md-8 col-sm-12 mt-auto mb-auto">
    <div class="slider slider-for">
        <? foreach ($images as $v):?>
            <div>
                <img src="<?=$v->getImage()?>">
            </div>
        <? endforeach;?>
    </div>
</div>