<div class="about">

    <div class="container mt-4">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <p class="main-page-link"><a href="/"><?=Yii::$app->view->params['main']->getText();?></a> <img src="/public/img/_.png"> <?=$model->getText();?></p>
            </div>
        </div>
    </div>
    <? $m = 0;?>
    <? foreach ($about as $v):?>
        <? $m++;?>
        <div class="container mt-5 <?=$m ==count($about)?"mb-5":"";?>">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <h1><?=$v->getTitle();?></h1>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-sm-12 col-md-12">
                    <p>
                        <?=$v->getContent();?>
                    </p>
                </div>
            </div>
            <? $n = 0;?>
            <? if($v->id == 2):?>
                <div class="row mt-3 mb-5">
                    <? foreach ($payment as $v):?>
                    <? $n++;?>
                    <div class="col-sm-12 col-md-4">
                        <div class="payment-method-0<?=$n;?> payment-method">
                            <div>
                                <p><?=$v->getContent();?></p>
                            </div>
                        </div>
                    </div>
                    <? endforeach;?>
                </div>
            <? endif;?>
        </div>
    <? endforeach;?>
</div>