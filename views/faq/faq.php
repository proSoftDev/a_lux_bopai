<div class="question">
    <div class="container mt-5 mb-5">
        <div class="row">
            <div class="col-sm-12 col-md-3">
                <nav class="switch-nav">
                    <ul>
                        <li><a href="<?=Yii::$app->view->params['footerMenu'][10]->url;?>"><?=Yii::$app->view->params['footerMenu'][10]->getText();?></a></li>
                        <li><a href="<?=Yii::$app->view->params['footerMenu'][2]->url;?>"><?=Yii::$app->view->params['footerMenu'][2]->getText()?></a></li>
                        <li class="switch-nav_active"><a href="#"><?=Yii::$app->view->params['footerMenu'][3]->getText();?></a></li>
                        <li><a href="<?=Yii::$app->view->params['footerMenu'][4]->url;?>"><?=Yii::$app->view->params['footerMenu'][4]->getText()?></a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-sm-12 col-md-1"></div>
            <div class="col-sm-12 col-md-8">
                <div class="question__container container">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <h1><?=Yii::t('app', 'FAQ');?></h1>
                        </div>
                    </div>

                    <? foreach ($faq as $v):?>
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <div class="dropdown">
                                    <input type="checkbox">
                                    <a href="#"><?=$v->getQuestion();?></a>
                                    <div class="dropdown-content">
                                        <p><?=$v->getContent();?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <? endforeach;?>
                </div>
            </div>
        </div>
    </div>
</div>