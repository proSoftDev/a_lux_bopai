<div class="catalog">
    <div class="container pt-4">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <p class="main-page-link"><a href="/"><?=Yii::$app->view->params['main']->getText()?></a>
                    <? for($i=count($breadcrumbs)-1;$i>=0;$i--):?>
                        <img src="/public/img/_.png">
                        <? if($i != 0):?>
                            <a href="/catalog?id=<?=$breadcrumbs[$i]["id"];?>"><?=$breadcrumbs[$i]["name"];?></a>
                        <? endif;?>
                        <? if($i == 0):?>
                            <?=$breadcrumbs[$i]["name"];?>
                        <? endif;?>
                    <? endfor;?>
                </p>
            </div>
        </div>
    </div>
    <div class="container pt-5 pb-5">
        <div class="row">
            <div class="col-sm-12 col-md-3">
                <ul class="accordion">
                    <? if($catalog->lastParentAttribute == 3):?>
                    <li class="accordion-item">
                        <input id="s1" class="hide" type="checkbox">
                        <label for="s1" class="accordion-label"><?=Yii::t('app', 'Size');?>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9 18L15 12L9 6" stroke="#AAAABE" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </label>
                        <ul class="accordion-child">
                            <? foreach ($sizes as $v):?>
                                <li>
                                    <input id="size<?=$v->id;?>" type="checkbox" value="<?=$v->id;?>" name="size" data-id="<?=$catalog->id;?>">
                                    <label for="size<?=$v->id;?>">
                                        <?=$v->name;?>
                                    </label>
                                </li>
                            <? endforeach;?>

                        </ul>
                    </li>
                    <? endif;?>
                    <? if($catalog->lastParentAttribute == 2 || $catalog->lastParentAttribute == 3):?>
                    <li class="accordion-item">
                        <input id="s2" class="hide" type="checkbox">
                        <label for="s2" class="accordion-label"><?=Yii::t('app', 'Color');?>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9 18L15 12L9 6" stroke="#AAAABE" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </label>
                        <ul class="accordion-child">

                            <? foreach ($color as $v):?>
                                <li>
                                    <input id="color<?=$v->id;?>" type="checkbox" value="<?=$v->id;?>" name="color" data-id="<?=$catalog->id;?>">
                                    <label for="color<?=$v->id;?>">
                                        <?=$v->getName();?>
                                    </label>
                                </li>
                            <? endforeach;?>
                        </ul>
                    </li>
                    <? endif;?>
                    <li class="accordion-item">
                        <input id="s3" class="hide" type="checkbox" >
                        <label for="s3" class="accordion-label"><?=Yii::t('app', 'Price');?>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9 18L15 12L9 6" stroke="#AAAABE" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>

                        </label>
                        <ul class="accordion-child">
                            <p>
                                <?=Yii::t('app', 'From');?> <input autocomplete="off" placeholder="5990" maxlength="8" id="from" data-id="<?=$catalog->id;?>"> тг
                                <?=Yii::t('app', 'To');?> <input autocomplete="off" placeholder="201000" maxlength="8" id="to" data-id="<?=$catalog->id;?>" тг
                            </p>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="col-sm-12 col-md-9">
                <div class="container products-catalog">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 mt-3 ml-3 mb-3" >
                            <h2><?=$catalog->getName();?></h2>
                            <div class="multifilters mt-3">
								<span class="products-catalog-sort">
									<?=Yii::t('app', 'Sort by');?>:
								</span>
                                <div class="multifilter">
                                    <input id="f1" type="checkbox" class="hide" data-id="<?=$catalog->id;?>" >
                                    <label for="f1" class="multifilter-title" >
                                        <?=Yii::t('app', 'By popularity');?>
                                    </label>
                                </div>
                                <div class="multifilter">
                                    <input id="f2" type="checkbox" class="hide" data-id="<?=$catalog->id;?>">
                                    <label for="f2" class="multifilter-title">
                                        <?=Yii::t('app', 'NEWS!');?>
                                    </label>
                                </div>
                                <div class="multifilter">
                                    <input id="f3" type="checkbox" class="hide" data-id="<?=$catalog->id;?>">
                                    <label for="f3" class="multifilter-title">
                                        <?=Yii::t('app', 'Decreasing price');?>
                                    </label>
                                </div>
                                <div class="multifilter">
                                    <input id="f4" type="checkbox" class="hide" data-id="<?=$catalog->id;?>">
                                    <label for="f4" class="multifilter-title">
                                        <?=Yii::t('app', 'Rising price');?>
                                    </label>
                                </div>
                                <div class="multifilter">
                                    <input id="f5" type="checkbox" class="hide" data-id="<?=$catalog->id;?>">
                                    <label for="f5" class="multifilter-title">
                                        <?=Yii::t('app', 'Discounts');?>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-sm-12 col-md-8">
                            <ul class="products-catalog-nav">
                                <? $m = 0;?>
                                <? foreach ($catalog->category as $v):?>
                                    <? $m++;?>
                                    <? if($m == 5) break;?>
                                    <li><a href="catalog?id=<?=$v->id;?>"><?=$v->getName();?></a></li>
                                <? endforeach;?>
                            </ul>
                        </div>
                        <? if(count($catalog->category) >= 5):?>
                        <div class="col-sm-12 col-md-4">
                            <ul class="accordion">
                                <li class="accordion-item">
                                    <input id="f6" class="hide" type="checkbox">
                                    <label for="f6" class="accordion-label"><?=Yii::t('app', 'show all');?>
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9 18L15 12L9 6" stroke="#AAAABE" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                    </label>
                                    <ul class="accordion-child">
                                        <? $m = 0;?>
                                        <? foreach ($catalog->category as $v):?>
                                            <? $m++;?>
                                            <? if($m >= 5):?>
                                                <li>
                                                    <label>
                                                        <input type="checkbox">
                                                        <a href="/catalog?id=<?=$v->id;?>"><?=$v->getName();?></a>
                                                    </label>
                                                </li>
                                            <? endif;?>
                                        <? endforeach;?>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <? endif;?>
                    </div>
                </div>
                <div class="container products-show">
                    <div class="cont_tov" id="filterResult">
                        <? if($products != null):?>
                        <? foreach ($products as $v):?>
                        <? if($v->quantity > 0):?>
                            <? $v->images;$m++;?>
                            <div class="tov_item">
                                <a href="/product?id=<?=$v->id;?>" class="tov_item_img">
                                    <img src="<?=$v->getImage();?>" alt="">
                                </a>
                                <a href="/product?id=<?=$v->id;?>" class="tov-button"><?=Yii::t('app', 'More details');?></a>
                                <div class="tov_item_info">
                                    <? if($v->isNew):?>
                                        <div class="tov_new"><?=Yii::t('app', 'NEW!');?></div>
                                    <? endif;?>
                                    <? if($v->isPopular):?>
                                        <div class="tov_new tov_pov"><?=Yii::t('app', 'Popular!');?></div>
                                    <? endif;?>
                                    <? if($v->isExclusive):?>
                                        <div class="tov_new tov_exs"><?=Yii::t('app', 'Exclusive!');?></div>
                                    <? endif;?>
                                    <p><?=$v->getName();?></p>
                                    <? if($v->newPrice != null):?>
                                        <div class="old_price"><?=$v->price;?><span>тг</span></div>
                                        <div class="price"><?=$v->newPrice;?><span>тг</span></div>
                                    <? endif;?>
                                    <? if($v->newPrice == null):?>
                                        <div class="price"><?=$v->price;?><span>тг</span></div>
                                    <? endif;?>
                                </div>
                            </div>
                        <? endif;?>
                        <? endforeach;?>
                        <? endif;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
