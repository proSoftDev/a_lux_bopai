function showSuccessAlert(text){
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 1500,
        timerProgressBar: true
    });
    Toast.fire({
        icon: 'success',
        title: text
    });
}

$('input[name="UserProfile[telephone]"]').inputmask("8(999) 999-9999");

$('input[name="UserProfile[name]"]').keyup(function(e) {
    var regex = /^[a-zA-Zа-яА-Яа-яА-Я]+$/;
    if (regex.test(this.value) !== true) {
        this.value = this.value.replace(/[^a-zA-Zа-яА-Яа-яА-Я]+/, '');
    }
});

$('input[name="UserProfile[surname]"]').keyup(function(e) {
    var regex = /^[a-zA-Zа-яА-Яа-яА-Я]+$/;
    if (regex.test(this.value) !== true) {
        this.value = this.value.replace(/[^a-zA-Zа-яА-Яа-яА-Я]+/, '');
    }
});


$('input[name="UserProfile[father]"]').keyup(function(e) {
    var regex = /^[a-zA-Zа-яА-Яа-яА-Я]+$/;
    if (regex.test(this.value) !== true) {
        this.value = this.value.replace(/[^a-zA-Zа-яА-Яа-яА-Я]+/, '');
    }
});



$('body').on('click','#haveAccount', function(e){

    $('#login').modal('show');
    $('#registration').modal('hide');

});

$('body').on('click','#go-to-register', function(e){

    $('#login').modal('hide');
    $('#registration').modal('show');
    $('#registration').css('overflow-y', 'auto');

});


$('body').on('click', '#login-button', function (e) {
    Swal.showLoading();
    $.ajax({
        type: 'POST',
        url: '/site/login',
        data: $(this).closest('form').serialize(),
        success: function (response) {
            if(response == 1){
                Swal.close();
                window.location.href = '/account';
            }else{
                Swal.close();
                Swal.fire({
                    icon: 'error',
                    text: response,
                });
            }
        },
        error: function () {
            Swal.close();
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});


$('body').on('click', '#register',function(e){

    $(this).attr('disabled', true);
    phone = $('input[name="UserProfile[telephone]"]').val();
    phoneStatus = true;
    for (var j = 0; j < phone.length; j++) {
        if (phone.charAt(j) == '_') {
            phoneStatus = false;
        }
    }


    if (!phoneStatus) {
        $('#register').attr('disabled', false);
        swal('',$('input[name="UserProfile[telephone]"]').attr('data-error'),'error');

    }else {
        Swal.showLoading();
        $.ajax({
            type: 'POST',
            url: '/site/register',
            data: $(this).closest('form').serialize(),
            success: function (response) {
                if (response == 1) {
                    Swal.close();
                    window.location.href = '/account';
                } else {
                    $('#register').attr('disabled', false);
                    Swal.close();
                    Swal.fire({
                        icon: 'error',
                        text: response,
                    });

                }
            },
            error: function () {
                $('#register').attr('disabled', false);
                Swal.close();
                swal('Упс!', 'Что-то пошло не так.', 'error');

            }
        });
    }
});




$('body').on('click', '#save-profile', function (e) {

    var phone = $('.profile-tel').val();
    var phoneStatus = true;
    for (var j = 0; j < phone.length; j++) {
        if (phone.charAt(j) == '_') {
            phoneStatus = false;
        }
    }

    if (!phoneStatus) {
        swal('',$('.profile-tel').attr('data-error'),'error');
    }else {
        $.ajax({
            type: 'POST',
            url: '/account/update-account-data',
            data: $(this).closest('form').serialize(),
            success: function (response) {
                if (response == 1) {
                    $('.profile-password').val('');
                    $('.profile-password-verify').val('');
                    showSuccessAlert($('#save-profile').attr('data-success-msg'));
                } else {
                    Swal.fire({
                        icon: 'error',
                        text: response,
                    });
                }
            },
            error: function () {
                swal('Упс!', 'Что-то пошло не так.', 'error');
            }
        });
    }
});







