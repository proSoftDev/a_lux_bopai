

function showSuccessAlert(text){
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true
    });
    Toast.fire({
        icon: 'success',
        title: text
    });
}

$('input[name="Feedback[telephone]"]').inputmask("8(999) 999-9999");

$('input[name="Feedback[fio]"]').keyup(function(e) {
    var regex = /^[a-zA-Zа-яА-Яа-яА-Я]+$/;
    if (regex.test(this.value) !== true) {
        this.value = this.value.replace(/[^a-zA-Zа-яА-Яа-яА-Я]+/, '');
    }
});



$('body').on('click','#save-subscription', function(){


    $.ajax({
        type: 'POST',
        url: '/site/new-subscribe',
        data: $(this).closest('form').serialize(),
        success: function (response) {
            if (response == 1) {
                $('input[name = "Subscription[email]"]').val('');
                showSuccessAlert($('#save-subscription').attr('data-success-msg'));
            } else {
                Swal.fire({
                    icon: 'error',
                    text: response,
                });
            }
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});


function loadingShow() {
    document.getElementById('loaderBackground').style.zIndex = '1000';
    document.getElementById('loaderBackground').style.display = 'flex';
}

function loadingHide() {
    document.getElementById('loaderBackground').style.zIndex = '-1000';
    document.getElementById('loaderBackground').style.display = 'none';
}


$('body').on('click','#sendFeedback', function(){

    var name = $('input[name="Feedback[fio]"]').val();
    var phone = $('input[name="Feedback[telephone]"]').val();
    var date = $('input[name="Feedback[date]"]').val();

    phoneStatus = true;
    for (var j = 0; j < phone.length; j++) {
        if (phone.charAt(j) == '_') {
            phoneStatus = false;
        }
    }

    if(!name) {
        Swal.fire({
            icon: 'error',
            text: $('input[name="Feedback[fio]"]').attr('data-error-msg'),
        });
    }else if(!phone || !phoneStatus){
        Swal.fire({
            icon: 'error',
            text: $('input[name="Feedback[telephone]"]').attr('data-error-msg'),
        });
    }else if(!date){
        Swal.fire({
            icon: 'error',
            text: $('input[name="Feedback[date]"]').attr('data-error-msg'),
        });
    }else{
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: '/feedback/request',
            data: {name:name,phone:phone,date:date},
            success: function(data){
                if(data.status){
                    $('input[name="Feedback[telephone]"]').val('');
                    $('input[name="Feedback[fio]"]').val('');
                    $('input[name="Feedback[date]"]').val('');
                    swal($('#sendFeedback').attr('data-success-msg-title'), $('#sendFeedback').attr('data-success-msg-text'), 'success');
                }else{
                    $('input[name="Feedback[telephone]"]').val('');
                    $('input[name="Feedback[fio]"]').val('');
                    $('input[name="Feedback[date]"]').val('');
                    swal('Упс!', 'Что-то пошло не так.', 'error');
                }
            },
            error: function () {
                swal('Упс!', 'Что-то пошло не так.', 'error');
            }
        });

        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: '/feedback/request-email',
            data: {name:name,phone:phone,date:date},
            success: function(data){

            },
            error: function () {

            }
        });
    }
});