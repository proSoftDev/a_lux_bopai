

function getStatus(id){
    if($(id).prop('checked') == true){
        return 1;
    }else if($(id).prop('checked') == false){
        return 0;
    }
}



function getStatusSort(priceDecrease, priceIncrease){
    if(priceDecrease == 1 || priceIncrease == 1){
        return 1;
    }else{
        return 0;
    }
}


function getSizes(){
    var sizes = '';
    $.each($('input[name="size"]:checked'), function(){
        sizes += $(this).val() + ',';
    });
    return sizes.substring(0, sizes.length - 1);
}


function getColors(){
    var colors = '';
    $.each($('input[name="color"]:checked'), function(){
        colors += $(this).val() + ',';
    });
    return colors.substring(0, colors.length - 1);

}


function getType(type, block_id){
    if(type == null && block_id == null){
        url = '/catalog/get-product-by-filter-status';
    }else{
        url = '/catalog/get-product-by-type';
    }
    return url;
}


$('body').on('click','#f1', function () {

    var statusPopular = getStatus('#f1');
    var statusNew = getStatus('#f2');
    var priceDecrease = getStatus('#f3');
    var priceIncrease = getStatus('#f4');
    var statusSort = getStatusSort(priceDecrease, priceIncrease);
    var statusDiscount = getStatus('#f5');
    var catalog_id = $(this).attr('data-id');
    var sizes = getSizes();
    var colors = getColors();
    var fromPrice = $('#from').val();
    var toPrice = $('#to').val();
    var type = $('#type').val();
    var block_id = $('#block_id').val();
    var url = getType(type,block_id);


    $.ajax({
        url: url,
        type: 'GET',
        data: {catalog_id:catalog_id, statusPopular:statusPopular,statusNew:statusNew, statusDiscount:statusDiscount,
            statusSort:statusSort, priceDecrease:priceDecrease, priceIncrease:priceIncrease,fromPrice:fromPrice,toPrice:toPrice,
            sizes:sizes,colors:colors,type:type,block_id:block_id},
        success: function(data){
            $('#filterResult').html(data);
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });

});





$('body').on('click','#f2', function () {

    var statusPopular = getStatus('#f1');
    var statusNew = getStatus('#f2');
    var priceDecrease = getStatus('#f3');
    var priceIncrease = getStatus('#f4');
    var statusSort = getStatusSort(priceDecrease, priceIncrease);
    var statusDiscount = getStatus('#f5');
    var catalog_id = $(this).attr('data-id');
    var sizes = getSizes();
    var colors = getColors();
    var fromPrice = $('#from').val();
    var toPrice = $('#to').val();
    var type = $('#type').val();
    var block_id = $('#block_id').val();
    var url = getType(type,block_id);


    $.ajax({
        url: url,
        type: 'GET',
        data: {catalog_id:catalog_id, statusPopular:statusPopular,statusNew:statusNew, statusDiscount:statusDiscount,
            statusSort:statusSort, priceDecrease:priceDecrease, priceIncrease:priceIncrease,fromPrice:fromPrice,toPrice:toPrice,
            sizes:sizes,colors:colors,type:type,block_id:block_id},
        success: function(data){
            $('#filterResult').html(data);
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });

});



$('body').on('click','#f3', function () {

    var statusPopular = getStatus('#f1');
    var statusNew = getStatus('#f2');
    var priceDecrease = getStatus('#f3');
    var priceIncrease = getStatus('#f4');
    var statusSort = getStatusSort(priceDecrease, priceIncrease);
    var statusDiscount = getStatus('#f5');
    var catalog_id = $(this).attr('data-id');
    var sizes = getSizes();
    var colors = getColors();
    var fromPrice = $('#from').val();
    var toPrice = $('#to').val();
    var type = $('#type').val();
    var block_id = $('#block_id').val();
    var url = getType(type,block_id);

    if(priceDecrease == 1){
        $('#f4').prop('checked', false);
        priceIncrease = 0;
    }



    $.ajax({
        url: url,
        type: 'GET',
        data: {catalog_id:catalog_id, statusPopular:statusPopular,statusNew:statusNew, statusDiscount:statusDiscount,
            statusSort:statusSort, priceDecrease:priceDecrease, priceIncrease:priceIncrease,fromPrice:fromPrice,toPrice:toPrice,
            sizes:sizes,colors:colors,type:type,block_id:block_id},
        success: function(data){
            $('#filterResult').html(data);
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });

});




$('body').on('click','#f4', function () {

    var statusPopular = getStatus('#f1');
    var statusNew = getStatus('#f2');
    var priceDecrease = getStatus('#f3');
    var priceIncrease = getStatus('#f4');
    var statusSort = getStatusSort(priceDecrease, priceIncrease);
    var statusDiscount = getStatus('#f5');
    var catalog_id = $(this).attr('data-id');
    var sizes = getSizes();
    var colors = getColors();
    var fromPrice = $('#from').val();
    var toPrice = $('#to').val();
    var type = $('#type').val();
    var block_id = $('#block_id').val();
    var url = getType(type,block_id);

    if(priceIncrease == 1){
        $('#f3').prop('checked', false);
        priceDecrease = 0;
    }


    $.ajax({
        url: url,
        type: 'GET',
        data: {catalog_id:catalog_id, statusPopular:statusPopular,statusNew:statusNew, statusDiscount:statusDiscount,
            statusSort:statusSort, priceDecrease:priceDecrease, priceIncrease:priceIncrease,fromPrice:fromPrice,toPrice:toPrice,
            sizes:sizes,colors:colors,type:type,block_id:block_id},
        success: function(data){
            $('#filterResult').html(data);
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });

});




$('body').on('click','#f5', function () {

    var statusPopular = getStatus('#f1');
    var statusNew = getStatus('#f2');
    var priceDecrease = getStatus('#f3');
    var priceIncrease = getStatus('#f4');
    var statusSort = getStatusSort(priceDecrease, priceIncrease);
    var statusDiscount = getStatus('#f5');
    var catalog_id = $(this).attr('data-id');
    var sizes = getSizes();
    var colors = getColors();
    var fromPrice = $('#from').val();
    var toPrice = $('#to').val();
    var type = $('#type').val();
    var block_id = $('#block_id').val();
    var url = getType(type,block_id);


    $.ajax({
        url: url,
        type: 'GET',
        data: {catalog_id:catalog_id, statusPopular:statusPopular,statusNew:statusNew, statusDiscount:statusDiscount,
            statusSort:statusSort, priceDecrease:priceDecrease, priceIncrease:priceIncrease,fromPrice:fromPrice,toPrice:toPrice,
            sizes:sizes,colors:colors,type:type,block_id:block_id},
        success: function(data){
            $('#filterResult').html(data);
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });

});




$('body').on('change paste keyup','#from', function () {

    var statusPopular = getStatus('#f1');
    var statusNew = getStatus('#f2');
    var priceDecrease = getStatus('#f3');
    var priceIncrease = getStatus('#f4');
    var statusSort = getStatusSort(priceDecrease, priceIncrease);
    var statusDiscount = getStatus('#f5');
    var catalog_id = $(this).attr('data-id');
    var sizes = getSizes();
    var colors = getColors();
    var fromPrice = $('#from').val();
    var toPrice = $('#to').val();
    var type = $('#type').val();
    var block_id = $('#block_id').val();
    var url = getType(type,block_id);

    $.ajax({
        url: url,
        type: 'GET',
        data: {catalog_id:catalog_id, statusPopular:statusPopular,statusNew:statusNew, statusDiscount:statusDiscount,
            statusSort:statusSort, priceDecrease:priceDecrease, priceIncrease:priceIncrease,fromPrice:fromPrice,toPrice:toPrice,
            sizes:sizes,colors:colors,type:type,block_id:block_id},
        success: function(data){
            $('#filterResult').html(data);
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});



$('body').on('change paste keyup','#to', function () {

    var statusPopular = getStatus('#f1');
    var statusNew = getStatus('#f2');
    var priceDecrease = getStatus('#f3');
    var priceIncrease = getStatus('#f4');
    var statusSort = getStatusSort(priceDecrease, priceIncrease);
    var statusDiscount = getStatus('#f5');
    var catalog_id = $(this).attr('data-id');
    var sizes = getSizes();
    var colors = getColors();
    var fromPrice = $('#from').val();
    var toPrice = $('#to').val();
    var type = $('#type').val();
    var block_id = $('#block_id').val();
    var url = getType(type,block_id);


    $.ajax({
        url: url,
        type: 'GET',
        data: {catalog_id:catalog_id, statusPopular:statusPopular,statusNew:statusNew, statusDiscount:statusDiscount,
            statusSort:statusSort, priceDecrease:priceDecrease, priceIncrease:priceIncrease,fromPrice:fromPrice,toPrice:toPrice,
            sizes:sizes,colors:colors,type:type,block_id:block_id},
        success: function(data){
            $('#filterResult').html(data);
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});



$('body').on('click','input[name="size"]', function () {

    var statusPopular = getStatus('#f1');
    var statusNew = getStatus('#f2');
    var priceDecrease = getStatus('#f3');
    var priceIncrease = getStatus('#f4');
    var statusSort = getStatusSort(priceDecrease, priceIncrease);
    var statusDiscount = getStatus('#f5');
    var catalog_id = $(this).attr('data-id');
    var sizes = getSizes();
    var colors = getColors();
    var fromPrice = $('#from').val();
    var toPrice = $('#to').val();
    var type = $('#type').val();
    var block_id = $('#block_id').val();
    var url = getType(type,block_id);


    $.ajax({
        url: url,
        type: 'GET',
        data: {catalog_id:catalog_id, statusPopular:statusPopular,statusNew:statusNew, statusDiscount:statusDiscount,
            statusSort:statusSort, priceDecrease:priceDecrease, priceIncrease:priceIncrease,fromPrice:fromPrice,toPrice:toPrice,
            sizes:sizes,colors:colors,type:type,block_id:block_id},
        success: function(data){
            $('#filterResult').html(data);
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});



$('body').on('click','input[name="color"]', function () {

    var statusPopular = getStatus('#f1');
    var statusNew = getStatus('#f2');
    var priceDecrease = getStatus('#f3');
    var priceIncrease = getStatus('#f4');
    var statusSort = getStatusSort(priceDecrease, priceIncrease);
    var statusDiscount = getStatus('#f5');
    var catalog_id = $(this).attr('data-id');
    var sizes = getSizes();
    var colors = getColors();
    var fromPrice = $('#from').val();
    var toPrice = $('#to').val();
    var type = $('#type').val();
    var block_id = $('#block_id').val();
    var url = getType(type,block_id);


    $.ajax({
        url: url,
        type: 'GET',
        data: {catalog_id:catalog_id, statusPopular:statusPopular,statusNew:statusNew, statusDiscount:statusDiscount,
            statusSort:statusSort, priceDecrease:priceDecrease, priceIncrease:priceIncrease,fromPrice:fromPrice,toPrice:toPrice,
            sizes:sizes,colors:colors,type:type,block_id:block_id},
        success: function(data){
            $('#filterResult').html(data);
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
});