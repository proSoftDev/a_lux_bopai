/* Аккордеон в боковом меню
—---------------------------------------------------— */

$('.accordion-trigger').click(function() {
    $(this).toggleClass('accordion-trigger--open')
    $(this).parent().find('.sub-menu').slideToggle();
});

/* Аккордеон каталога
—---------------------------------------------------— */
$('.catalog-trigger').click(function() {
    $(this).toggleClass('catalog-trigger--open')
    $(this).parent().find('.mob-menu').slideToggle();
});


/* Аккордеон оплаты
—---------------------------------------------------— */
$('.payment-trigger').click(function() {
    $(this).toggleClass('payment-trigger--open')
    $(this).parent().find('.payment-menu').slideToggle();
});

/* Аккордеон в окне регистрации
—---------------------------------------------------— */
$('.reg-trigger').click(function() {
    $(this).toggleClass('reg-trigger-open')
    $(this).parent().find('.hiddem-form').slideToggle();
});


/* Слайдер популярных товаров */

$('.slider_popular_tov').owlCarousel({
    loop:true,
    autoplay:true,
    autoplayTimeout:5000,
    autoplayHoverPause:true,
    responsive:{
        0:{
            items:2
        },
        400:{
            items:2
        },
        500:{
            items:2
        },
        600:{
            items:2
        },
        700:{
            items:3
        },
        800:{
            items:3
        },
        900:{
            items:4
        },
        1320:{
            items:4
        },
        1920:{
            items:4
        }
    }
});

var nav = $('.slider_popular_tov');

$('.arrow-right').click(function() {
    nav.trigger('next.owl.carousel', [600]);
})

$('.arrow-left').click(function() {
    nav.trigger('prev.owl.carousel', [600]);
});
/* —---------------------------------------------------— */

/* Навигация по странице
—---------------------------------------------------— */
$('.mobile-menu').click(function () {
    $('.mobile-window').addClass('nav-open');
    $('body').addClass('modal-open');
    return false;
});


$('.mobile-menu-close').click(function () {
    $('.mobile-window').removeClass('nav-open');
    $('body').removeClass('modal-open');
    return false;
});


$(function () {
    $(document).click(function (event) {
        if ($('.mobile-window').hasClass('nav-open')) {
            if ($(event.target).closest('.mobile-window').length) {
                return;
            }
            $('.mobile-window').removeClass('nav-open');
            $('body').removeClass('modal-open');
            event.stopPropagation();
        }
    });
});

/* Всплывающее окно регистрации--— */
$('.reg-modal-trigger').click(function () {
    $('.modal-window').addClass('nav-open');
    $('body').addClass('modal-open');
    return false;
});


$('.modal-menu-close').click(function () {
    $('.modal-window').removeClass('nav-open');
    $('body').removeClass('modal-open');
    return false;
});


$(function () {
    $(document).click(function (event) {
        if ($('.modal-window').hasClass('nav-open')) {
            if ($(event.target).closest('.modal-window').length) {
                return;
            }
            $('.modal-window').removeClass('nav-open');
            $('body').removeClass('modal-open');
            event.stopPropagation();
        }
    });
});




// Кастомизация select header
$(document).ready(function() {
    var $disabledResults = $(".country");
    $disabledResults.select2();
    $('.country').select2({
        minimumResultsForSearch: Infinity
    });

});
// Навигация
$(document).ready(function($) {
    let Nav = $('#main-nav').hcOffcanvasNav({
        navTitle: 'Навигация',
        maxWidth: 992,
        disableBody: true,
        insertClose: /*5*/ false,
        /*labelClose: 'Закрыть'*/
    });
});
// Каталог
$(document).ready(function($) {
    let $Catalog = $('#catalog-nav');
    let $Toggle = $('.toggle');

    let defaultData = {
        maxWidth: false,
        customToggle: $Toggle,
        navTitle: 'Каталог',
        levelTitles: true,
        pushContent: '#header',
        insertClose: /*11*/ false,
        labelBack: 'Назад',
        /* labelClose: 'Закрыть',*/
        closeLevels: false
    };
    let Navigation = $Catalog.hcOffcanvasNav(defaultData);
    const update = (settings) => {
        if (Navigation.isOpen()) {
            Navigation.on('close.once', function() {
                Navigation.update(settings);
                Navigation.open();
            });

            Navigation.close();
        }
        else {
            Navigation.update(settings);
        }
    };
});
// slick
$(document).ready(function(){
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav',

    });
    $('.slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: false,
        autoplay: true,
        autoplaySpeed: 3000,
        centerMode: true,
        focusOnSelect: true,
        vertical: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    vertical: false,
                    arrows: false,
                }
            }
        ]
    });
});

$('.autoplay').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    responsive: [
        {
            breakpoint: 601,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false
            }
        },
        {
            breakpoint: 1001,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                arrows: false
            }
        },
        {
            breakpoint: 1151,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        }
    ]
});
// Кастомизация select product item
$(document).ready(function() {
    var $disabledResults = $(".sizes");
    $disabledResults.select2();
    $('.sizes').select2({
        placeholder: "Выберите размер",
        allowClear: true,
        minimumResultsForSearch: Infinity
    });

});

$(document).ready(function() {
    var $disabledResults = $(".color");
    $disabledResults.select2();
    $('.color').select2({
        placeholder: "Выберите цвет",
        allowClear: true,
        minimumResultsForSearch: Infinity
    });

});
/*$(document).ready(function() {
  $('[data-toggle="popover"]').popover({
    trigger: 'hover',
  });
})*/

// Increase
$(document).ready(function() {
    $('.increase').click(function() {
        $('.output').val(+$('.output').val() + 1);
    });

});

$(document).ready(function() {
    if (window.location.pathname == "/address") {
        $(".popover").addClass("astana-almaty");
    }
});
$(document).ready(function() {
    $('.almatyCity').click(function() {
        $("#almaty").popover('show');
        $("#astana").popover('hide');
        $("#almaty").css({'fill' : '#FF3A52', 'stroke':'#FF3A52'});
        $("#astana").css({'fill' : '#AAAABE', 'stroke': 'none'});

    });
    $('.astanaCity').click(function() {
        $("#almaty").popover('hide');
        $("#astana").popover('show');
        $("#astana").css({'fill' : '#FF3A52', 'stroke':'#FF3A52'});
        $("#almaty").css({'fill' : '#AAAABE', 'stroke': 'none'});

    });
    $('#almaty').click(function() {
        $("#almaty").popover('show');
        $("#astana").popover('hide');
        $("#almaty").css({'fill' : '#FF3A52', 'stroke':'#FF3A52'});
        $("#astana").css({'fill' : '#AAAABE', 'stroke': 'none'});
        $(".almatyCity").addClass("active");
        $(".astanaCity").removeClass("active");
    });
    $('#astana').click(function() {
        $("#almaty").popover('hide');
        $("#astana").popover('show');
        $("#astana").css({'fill' : '#FF3A52', 'stroke':'#FF3A52'});
        $("#almaty").css({'fill' : '#AAAABE', 'stroke': 'none'});
        $(".astanaCity").addClass("active");
        $(".almatyCity").removeClass("active");
    });

});

$(document).ready(function() {
    $('#maxAlmaty').click(function() {
        $("#maxAlmaty").popover('show');
        $("#maxAstana").popover('hide');
        $("#maxAlmaty").css({'fill' : '#FF3A52', 'stroke':'#FF3A52'});
        $("#maxAstana").css({'fill' : '#AAAABE', 'stroke': 'none'});
        $('.popover').not($('.astana-almaty')).addClass('zindex');
    });
    $('#maxAstana').click(function() {
        $("#maxAlmaty").popover('hide');
        $("#maxAstana").popover('show');
        $("#maxAstana").css({'fill' : '#FF3A52', 'stroke':'#FF3A52'});
        $("#maxAlmaty").css({'fill' : '#AAAABE', 'stroke': 'none'});
        $('.popover').not($('.astana-almaty')).addClass('zindex');
    });
});
//detailed page GLIDE
$(document).ready(function() {
    new Glide(".glide_header", {
        autoplay: 3000,
        type: 'carousel',
        hoverpause: false,
    }).mount()
    new Glide('.glide', {
        autoplay: 3000,
        hoverpause: false,
        type: 'carousel',
        startAt: 0,
        perView: 5,
        breakpoints: {
            600: {
                perView: 3
            }
        }
    }).mount()
});
$('.delete-products').click(function(){
    $(this).parent('.tov_item').css('display', 'none');
    console.log(1)
})
//Star Rating
if($('.star-rating__input').prop('disabled')) {
    console.log('why');
    $('.star-rating__ico').removeClass('star-rating__hover');
}

