
function getColorData(color_id,product_id, selected_size_id){

    $.ajax({
        type: 'GET',
        url: '/product/get-color-sizes',
        data: {color_id:color_id, product_id:product_id, selected_size_id:selected_size_id},
        success: function (response) {
            $('#product-sizes').html(response);
            $('#product-sizes').select2({minimumResultsForSearch: Infinity});
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
            $(this).removeAttr('disabled');
        }
    });


    $.ajax({
        url: '/product/get-color-images',
        data: {color_id:color_id, product_id:product_id},
        success: function(data){
            $('#colorResult').html(data);
            $('.slider-for').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                asNavFor: '.slider-nav',

            });
            $('.slider-nav').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                asNavFor: '.slider-for',
                dots: false,
                autoplay: true,
                autoplaySpeed: 3000,
                centerMode: true,
                focusOnSelect: true,
                vertical: true,
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            vertical: false,
                            arrows: false,
                        }
                    }
                ]
            });
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });



    $.ajax({
        type: 'GET',
        url: '/product/get-color-quantity',
        data: {color_id:color_id, product_id:product_id, selected_size_id:selected_size_id},
        success: function (response) {
            $('#productQuantity').html(response);
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });



}




function getSizeData(size_id,product_id,selected_color_id ){

    $.ajax({
        type: 'GET',
        url: '/product/get-size-colors',
        data: {size_id:size_id, product_id:product_id,selected_color_id:selected_color_id},
        success: function (response) {
            $('#product-colors').html(response);
            $('#product-colors').select2({minimumResultsForSearch: Infinity});
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });


    $.ajax({
        type: 'GET',
        url: '/product/get-size-quantity',
        data: {size_id:size_id, product_id:product_id,selected_color_id:selected_color_id},
        success: function (response) {
            $('#productQuantity').html(response);
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });



}




function addProductBasketThree(product_id, selected_color_id, selected_size_id){

    if(selected_color_id == null){
        swal($('.addToTrash').attr('data-color-msg'));
    }else if(selected_size_id == null){
        swal($('.addToTrash').attr('data-size-msg'));
    }else{

        $.ajax({
            url: '/product/add-basket',
            type: 'GET',
            dataType: 'json',
            data: {product_id:product_id,selected_color_id:selected_color_id,selected_size_id:selected_size_id},
            success: function(data){
                if(data.status){
                    $('#header_basket').html(data.count+' товар</span>');
                    swal($('.addToTrash').attr('data-title-msg'), {
                        buttons: {
                            cancel: $('.addToTrash').attr('data-cancel-msg'),
                            catch: {
                                text: $('.addToTrash').attr('data-text-msg'),
                                value: 'catch',
                            },
                        },
                    })
                        .then((value) => {
                            switch (value) {

                                case 'catch':
                                    window.location.href = '/card/';
                                    break;

                            }
                        });
                }else{
                    swal('Упс!', 'Что-то пошло не так.', 'error');
                }
            },
            error: function () {
                swal('Упс!', 'Что-то пошло не так.', 'error');
            }
        });
    }

}




function addProductBasketTwo(product_id, selected_color_id){

    if(selected_color_id == null){
        swal($('.addToTrash').attr('data-color-msg'));
    }else{

        $.ajax({
            url: '/product/add-basket',
            type: 'GET',
            dataType: 'json',
            data: {product_id:product_id,selected_color_id:selected_color_id},
            success: function(data){
                if(data.status){
                    $('#header_basket').html(data.count+' товар</span>');
                    swal($('.addToTrash').attr('data-title-msg'), {
                        buttons: {
                            cancel: $('.addToTrash').attr('data-cancel-msg'),
                            catch: {
                                text: $('.addToTrash').attr('data-text-msg'),
                                value: 'catch',
                            },
                        },
                    })
                    .then((value) => {
                        switch (value) {

                            case 'catch':
                                window.location.href = '/card/';
                                break;
                        }
                    });
                }else{
                    swal('Упс!', 'Что-то пошло не так.', 'error');
                }
            },
            error: function () {
                swal('Упс!', 'Что-то пошло не так.', 'error');
            }
        });
    }
}




function addProductBasketOne(product_id){

    $.ajax({
        url: '/product/add-basket',
        type: 'GET',
        dataType: 'json',
        data: {product_id:product_id},
        success: function(data){
            if(data.status){
                $('#header_basket').html(data.count+' товар</span>');
                swal($('.addToTrash').attr('data-title-msg'), {
                    buttons: {
                        cancel: $('.addToTrash').attr('data-cancel-msg'),
                        catch: {
                            text: $('.addToTrash').attr('data-text-msg'),
                            value: 'catch',
                        },
                    },
                })
                    .then((value) => {
                        switch (value) {

                            case 'catch':
                                window.location.href = '/card/';
                                break;
                        }
                    });
            }else{
                swal('Упс!', 'Что-то пошло не так.', 'error');
            }
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });

}


function addProductToFavoriteFromProduct(product_id){
    $.ajax({
        type: 'GET',
        url: '/product/add-product-to-favorite',
        data: {product_id:product_id},
        success: function (response) {
            if(response == 1){
                $('.heart_for_favorite').addClass('activeFavorite');
            }else if(response == 0){
                $('.heart_for_favorite').removeClass('activeFavorite');
            }else{
                swal('Упс!', 'Что-то пошло не так.', 'error');
            }

        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
}



function addProductToFavoriteFromBasket(product_id){
    $.ajax({
        type: 'GET',
        url: '/product/add-product-to-favorite',
        data: {product_id:product_id},
        success: function (response) {
            if(response == 1){
                $('#heart_for_favorite'+product_id).addClass('activeFavorite');
            }else if(response == 0){
                $('#heart_for_favorite'+product_id).removeClass('activeFavorite');
            }else{
                swal('Упс!', 'Что-то пошло не так.', 'error');
            }

        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
}



function addUserRating(rating, product_id){
    $.ajax({
        type: 'GET',
        url: '/product/add-new-rating',
        data: {rating:rating,product_id:product_id},
        success: function (data) {
            $('#ratingResult').html(data);
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
}


