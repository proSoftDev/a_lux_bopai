<?php
	require_once('header2.php');
?>
<div class="finished">

	<div class="container">
		<div class="finished-container">
			<div class="container">
			    <div class="title">Заказ завершен <img src="img/Vector.png" alt=""></div>
		     	<div class="subtitle">Спасибо, что выбрали интернет-магазин Bopai!
					  Отследить состояние своего заказа Вы можете в личном кабинете</div>
				<div class="time-order">Ожидайте курьера 12 декабря 2019, в 10:00</div>
				<input type="button" class="button-finished" value="Перейти в личный кабинет">
			</div>
			<div class="container mt-4">
				<div class="row">
					<div class="col-12 title-products">Товары в заказе</div>
				</div>
				<div class="row order">
					<div class="img">
						<img src="img/basket-product-01.png" width="100%" alt="">
					</div>
					<div class="info">
						 <div class="name">Белая рубашка</div>
						 <div class="quantity">
							<div class="qty">1шт.</div>
							<div class="sum">13 000 тг.</div>
						 </div>
					</div>
				</div>
				<div class="row order">
					<div class="img">
						<img src="img/basket-product-01.png" width="100%" alt="">
					</div>
					<div class="info">
						 <div class="name">Белая рубашка</div>
						 <div class="quantity">
							<div class="qty">1шт.</div>
							<div class="sum">13 000 тг.</div>
						 </div>
					</div>
				</div>
				<div class="row order">
					<div class="img">
						<img src="img/basket-product-01.png" width="100%" alt="">
					</div>
					<div class="info">
						 <div class="name">Белая рубашка</div>
						 <div class="quantity">
							<div class="qty">1шт.</div>
							<div class="sum">13 000 тг.</div>
						 </div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="strip"></div>
			</div>
			<div class="container">
				<div class="amount">
					<p>ИТОГО:</p>
					<span>39 000 тг.</span>
				</div>
			</div>
		</div>
	</div>

</div>
<?php
	require_once('footer.php');
?>