<?php
	require_once('header2.php');
?>
<div class="product-item">
	<div class="container mt-4">
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<p class="main-page-link"><a href="#">Главная</a> <img src="img/_.png"> Одежда</p>
			</div>
		</div>
	</div>
	<div class="container mt-5 mb-5 pb-5 separator">
		<div class="row">
			<div class="col-sm-12 col-md-5 product-photo">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-12">
							<div class="slider slider-nav">
								<div>
									<img src="img/1.png">
								</div>
								<div>
									<img src="img/2.png">
								</div>
								<div>
									<img src="img/4.png">
								</div>
								<div>
									<img src="img/4.png">
								</div>
							</div>
						</div>
						<div class="col-md-8 col-sm-12 mt-auto mb-auto">
							<div class="slider slider-for">
								<div>
									<img src="img/5.png">
								</div>
								<div>
									<img src="img/5.png">
								</div>
								<div>
									<img src="img/5.png">
								</div>
								<div>
									<img src="img/5.png">
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-7">
				<div class="container">
					<div class="row align-items-start">
						<div class="col-sm-12 col-md-7 general-info">
							<h1>Рубашка белая</h1>
							<div class="container mt-3 pl-0 pr-0">

								<div class="row">
									<div class="col-sm-12 col-md-5 star-rating pr-0">
										    <div class="star-rating__wrap">
										        <input class="star-rating__input" id="star-rating-5" type="radio" name="rating" value="5" disabled>
										        <label class="star-rating__ico star-rating__hover fa fa-star-o fa-lg" for="star-rating-5" title="5 out of 5 stars"></label>
										        <input class="star-rating__input" id="star-rating-4" type="radio" name="rating" value="4" disabled>
										        <label class="star-rating__ico star-rating__hover fa fa-star-o fa-lg" for="star-rating-4" title="4 out of 5 stars"></label>
										        <input class="star-rating__input" id="star-rating-3" type="radio" name="rating" value="3" disabled>
										        <label class="star-rating__ico star-rating__hover fa fa-star-o fa-lg" for="star-rating-3" title="3 out of 5 stars"></label>
										        <input class="star-rating__input" id="star-rating-2" type="radio" name="rating" value="2" disabled>
										        <label class="star-rating__ico star-rating__hover fa fa-star-o fa-lg" for="star-rating-2" title="2 out of 5 stars"></label>
										        <input class="star-rating__input" id="star-rating-1" type="radio" name="rating" value="1" disabled>
										        <label class="star-rating__ico star-rating__hover fa fa-star-o fa-lg" for="star-rating-1" title="1 out of 5 stars"></label>
										     </div>
									</div>
									<div class="col-sm-12 col-md-3 pl-0 pr-0">
										<p class="reviews">10 отзывов</p>
									</div>
									<div class="col-sm-12 col-md-4 pl-0 pr-0">
										<p class="bought">Купили 76 000 раз</p>
									</div>
								</div>

								<div class="row characteristics">
									<div class="col-sm-12 col-md-12 general-info mt-5">
										<div class="container pl-0">
											<div class="row align-items-end">
												<div class="col-sm-12 col-md-3"><p>Наличие</p></div>
												<div class="col-sm-12 col-md-6 line"></div>
												<div class="col-sm-12 col-md-3"><h5>Много</h5></div>
											</div>
											<div class="row align-items-end">
												<div class="col-sm-12 col-md-4"><p>Производитель</p></div>
												<div class="col-sm-12 col-md-5 line"></div>
												<div class="col-sm-12 col-md-3"><h5>Китай</h5></div>
											</div>
											<div class="row align-items-end">
												<div class="col-sm-12 col-md-5"><p>Фактура материала</p></div>
												<div class="col-sm-12 col-md-4 line"></div>
												<div class="col-sm-12 col-md-3"><h5>Трикотажный</h5></div>
											</div>
											<div class="row align-items-end">
												<div class="col-sm-12 col-md-3"><p>Тип рукава</p></div>
												<div class="col-sm-12 col-md-6 line"></div>
												<div class="col-sm-12 col-md-3"><h5>Длинный</h5></div>
											</div>
											<div class="row align-items-end">
												<div class="col-sm-12 col-md-4"><p>Тип застежки</p></div>
												<div class="col-sm-12 col-md-5 line"></div>
												<div class="col-sm-12 col-md-3"><h5>Пуговицы</h5></div>
											</div>
										</div>
									</div>
								</div>

								<div class="row description mt-4">
									<div class="col-sm-12 col-md-12">
										<h3>Описание</h3>
										<p>
											Легкая рубашка из эластичного штапеля. Полуприлегающий силуэт. Цветочный принт в натуральных тонах на белом фоне. Материал декатирован - обработан паром и водой. Застежка на пуговицы. Воротник рубашечный. Рукав цельновыкроенный короткий с драпированной манжетой и патой по низу. Фигурная линия низа. Длина по спинке от плечевого шва ок 70 см. На фото с брюками 330-220.
										</p>
									</div>
								</div>
							</div>
							
								
							</div>
							<div class="col-sm-12 col-md-5 trash-block">
								<div class="container">
									<div class="row">
										<div class="col-sm-12 col-md-12 pl-0 pr-0">
											<h3 class="previous-price">56 000 тг.</h3>	
										</div>
										<div class="col-sm-12 col-md-12 pl-0 pr-0">
											<h2 class="sale-price">13 000 тг.</h2>
										</div>
										<div class="col-sm-12 col-md-12 pl-0 pr-0">
										  <form action="">
											<select class="sizes js-states form-control select2-hidden-accessible" data-select2-id="1" tabindex="-1" aria-hidden="true">
											  <option data-select2-id="3"></option>
											  <option value="40">40</option>
											  <option value="42">42</option>
											  <option value="44">44</option>
											  <option value="46" disabled="disabled">46</option>
											  <option value="48" disabled="disabled">48</option>
											  <option value="50">50</option>
											</select>
										
										
											<select class="color js-states form-control select2-hidden-accessible" data-select2-id="1" tabindex="-1" aria-hidden="true">
											  <option data-select2-id="3"></option>
											  <option value="white">Белый</option>
											  <option value="red">Красный</option>
											  <option value="yellow">Желтый</option>
											  <option value="blue">Синий</option>
											  <option value="green" disabled="disabled">Зеленый</option>
											</select>
									 		<div class="trash-block-btns">
											  <input class="addToTrash" type="button" value="Добавить в корзину">
											  <label for="favorite">
												  <svg width="22" height="20" viewBox="0 0 22 20" fill="none" xmlns="http://www.w3.org/2000/svg">
												  <path d="M10.8882 1.86775L11.0656 2.00475L11.2472 1.87353C12.4544 1.00163 13.9579 0.527488 15.462 0.527488C17.0612 0.527488 18.6246 1.17739 19.7511 2.33146C22.3501 5.05929 22.3484 9.34744 19.7534 12.0358L19.7194 12.0695L12.3149 19.1577L12.3149 19.1577L12.3109 19.1615C11.588 19.8795 10.4024 19.8795 9.67951 19.1615L9.67954 19.1615L9.67614 19.1582L2.27358 12.0341C-0.359859 9.3058 -0.356632 5.02018 2.2759 2.33317L2.27598 2.33309C3.55608 1.02549 5.2733 0.3 7.10108 0.3C8.27009 0.3 9.54292 0.828637 10.8882 1.86775ZM17.998 10.3403L18.0041 10.3345L18.0098 10.3283C19.6452 8.58246 19.6455 5.82337 17.9698 4.07729C17.2971 3.36787 16.3378 2.99858 15.3857 2.99858H15.3475V2.99844L15.3385 2.99871C14.0032 3.0389 12.7854 3.56244 11.8528 4.48856C11.359 4.97901 10.5551 4.97901 10.0613 4.48856L10.0613 4.48852L10.0575 4.48488C9.49219 3.94281 8.94799 3.51856 8.44388 3.22819C7.94361 2.94003 7.46096 2.77109 7.02473 2.77109C5.87474 2.77109 4.80663 3.22076 3.98817 4.03358L3.98804 4.03345L3.98079 4.04117C2.30846 5.82349 2.30515 8.58325 3.98316 10.331L3.98295 10.3312L3.99248 10.3403L10.7881 16.8237L10.9952 17.0213L11.2023 16.8237L17.998 10.3403Z" fill="#757482" stroke="white" stroke-width="0.6"/>
												  </svg>

											  </label>
											  <input id="favorite" type="button" style="display: none;">
											</div>
										    </form>
										</div>
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
		<div class="container recommended">
			<div class="row">
				<div class="col-sm-12 col-md-12 mb-5">
					<h2>Мы рекомендуем!</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-md-10 mb-5">

					<div class="slider autoplay">

						<div>
						  <div class="product-container">
						   <div>
							<img src="img/img1.png">
							<a href="#" class="product-btn">Подробнее</a>
							<div class="separator"></div>
							<p class="novelty">Новинка!</p>
							<p class="product-description">Женское платье Klimini, бежевое</p>
							<p class="price">13 000 <span>тг.</span></p>
						   </div>
						  </div>
						</div>
						<div>
						 <div class="product-container">
						   <div>
							<img src="img/img2.png">
							<a href="#" class="product-btn">Подробнее</a>
							<div class="separator"></div>
							<p class="product-description">Платье Versace, фиолетовое</p>
							<p class="price">9 340 <span>тг.</span></p>
						   </div>
						  </div>
						</div>
						<div>
						  <div class="product-container">
						   <div>
							<img src="img/img3.png">
							<a href="#" class="product-btn">Подробнее</a>
							<div class="separator"></div>
							<p class="product-description">Платье, оранжевое</p>
							<p class="previous-price">56 000 <span>тг.</span></p>
							<p class="price">36 000 <span>тг.</span></p>
						   </div>						 
						  </div>
						</div>
						<div>
						  <div class="product-container">
						   <div>
							<img src="img/img4.png">
							<a href="#" class="product-btn">Подробнее</a>
							<div class="separator"></div>
							<p class="novelty">Новинка!</p>
							<p class="product-description">Темно синий женский жилет</p>
							<p class="price">7 300 <span>тг.</span></p>
						   </div>
						  </div>
						</div>
						<div>
						  <div class="product-container">
						   <div>
							<img src="img/tov1.png">
							<a href="#" class="product-btn">Подробнее</a>
							<div class="separator"></div>
							<p class="novelty">Новинка!</p>
							<p class="product-description">Платье - сарафан, розовое</p>
							<p class="price">5 630 <span>тг.</span></p>
						   </div>
						  </div>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-2"></div>
			</div>
		</div>
		<div class="container recommended">
			<div class="row">
				<div class="col-sm-12 col-md-12 mb-5">
					<h2>С этим также покупают</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-md-10 mb-5">

					<div class="slider autoplay">

						<div>
						  <div class="product-container">
						   <div>
							<img src="img/tov5.png">
							<a href="#" class="product-btn">Подробнее</a>
							<div class="separator"></div>
							
							<p class="product-description">Блузка голубая</p>
							<p class="price">3 000 <span>тг.</span></p>
						   </div>
						  </div>
						</div>
						<div>
						 <div class="product-container">
						   <div>
							<img src="img/tov6.png">
							<a href="#" class="product-btn">Подробнее</a>
							<div class="separator"></div>
							<p class="product-description">Туника в клетку, фиолетовая</p>
							<p class="previous-price">9 000 <span>тг.</span></p>
							<p class="price">4 990 <span>тг.</span></p>
						   </div>
						  </div>
						</div>
						<div>
						  <div class="product-container">
						   <div>
							<img src="img/tov7.png">
							<a href="#" class="product-btn">Подробнее</a>
							<div class="separator"></div>
							<p class="product-description">Длинный трикотажный кардиган, серый</p>
							<p class="price">8 060 <span>тг.</span></p>
						   </div>
						  </div>
						</div>
						<div>
						  <div class="product-container">
						   <div>
							<img src="img/tov8.png">
							<a href="#" class="product-btn">Подробнее</a>
							<div class="separator"></div>
							<p class="exclusive">Эксклюзив!</p>
							<p class="product-description">Блузка оранжевая</p>
							<p class="price">3 120 <span>тг.</span></p>
						   </div>
						  </div>
						</div>
						<div>
						  <div class="product-container">
						   <div>
							<img src="img/tov1.png">
							<a href="#" class="product-btn">Подробнее</a>
							<div class="separator"></div>
							<p class="novelty">Новинка!</p>
							<p class="product-description">Платье - сарафан, розовое</p>
							<p class="price">5 630 <span>тг.</span></p>
						   </div>
						  </div>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-2"></div>
			</div>
		</div>
	</div>

<?php
	require_once('footer.php');
?>