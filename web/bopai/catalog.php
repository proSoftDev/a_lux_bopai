<?php
	require_once('header2.php');
?>
	
<div class="catalog">
	<div class="container pt-4">
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<p class="main-page-link"><a href="#">Главная</a> <img src="img/_.png"> Одежда</p>
			</div>
		</div>
	</div>
	<div class="container pt-5 pb-5">
		<div class="row">
			<div class="col-sm-12 col-md-3">
				<ul class="accordion">
				  <li class="accordion-item">
				    <input id="s1" class="hide" type="checkbox">
				    <label for="s1"class="accordion-label">Размер одежды
						<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M9 18L15 12L9 6" stroke="#AAAABE" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round"/>
						</svg>	
				    </label>


				    <ul class="accordion-child">
				      <li>
				        
				          <input id="xs" type="checkbox">
				          <label for="xs">
				          XS (34-36 RUS)
				        </label>
				      </li>
				      <li>
				        
				        <input id="s" type="checkbox">
				        <label for="s">
				        S (38 RUS)
				        </label>
				      </li>
				      <li>
				        
				        <input id="m" type="checkbox">
				        <label for="m">
				        M (40-42 RUS)
				        </label>
				      </li>
				      <li>
				        
				        <input id="l" type="checkbox">
				        <label for="l">
				        L (44 RUS) 
				        </label>
				      </li>
				      <li>
				        
				        <input id="xl" type="checkbox">
				        <label for="xl">
				        XL (46-48 RUS)
				        </label>
				      </li>
				      <li>
				        
				        <input id="xxl" type="checkbox">
				        <label for="xxl">
				        XXL (50 RUS)
				        </label>
				      </li>
				      <li>
				        
				        <input type="checkbox">
				        <label for="xxxl">
				        XXXL (52 RUS)
				        </label>
				      </li>
				   </ul>
				  </li>
				  <li class="accordion-item">
				    <input id="s2" class="hide" type="checkbox">
				    <label for="s2" class="accordion-label">Цвет
						<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M9 18L15 12L9 6" stroke="#AAAABE" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round"/>
						</svg>

				    </label>
				    <ul class="accordion-child">
				      <li>
				        
				         <input id="color1" type="checkbox">
				         <label for="color1">
				         	Бежевый
				        </label>
				      </li>
				      <li>
				        
				        <input id="color2" type="checkbox">
				        <label for="color2">
				        	Белый
				        </label>
				      </li>
				      <li>
				        
				        <input id="color3" type="checkbox">
				        <label for="color3">
				        Бирюзовый
				        </label>
				      </li>
				      <li>
				        
				        <input id="color4" type="checkbox">
				        <label for="color4">
				        Бордовый
				        </label>
				      </li>
				      <li>
				        
				        <input id="color5" type="checkbox">
				        <label for="color5">
				        Голубой
				        </label>
				      </li>
				      <li>
				        
				        <input id="color6" type="checkbox">
				        <label for="color6">
				        Желтый
				        </label>
				      </li>
				      <li>
				        
				        <input id="color7" type="checkbox">
				        <label for="color7">
				        Зеленый
				        </label>
				      </li>
				      <li>
				        
				        <input id="color8" type="checkbox">
				        <label for="color8">
				        Коричневый
				        </label>
				      </li>
				      <li>
				        <input id="color9" type="checkbox">
				        <label for="color9">
				        Красный  
				        </label>
				      </li>
				      <li>
				        
				      	<input id="color10" type="checkbox">
				      	<label for="color10">
				        Мультиколор
				        </label>
				      </li>
				      <li>
				        
				        <input id="color11" type="checkbox">
				        <label for="color11">
				        Хаки
				        </label>
				      </li>
				      <li>
				        
				        <input id="color12" type="checkbox">
				        <label for="color12">
				        Черный  
				        </label>
				      </li>
				   </ul>
				  </li>
				  <li class="accordion-item">
				    <input id="s3" class="hide" type="checkbox" >
				    <label for="s3" class="accordion-label">Цена
						<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M9 18L15 12L9 6" stroke="#AAAABE" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round"/>
						</svg>

				    </label>
				    <ul class="accordion-child">
				      <p>От <input autocomplete="off" placeholder="5990" maxlength="8"> тг До <input autocomplete="off" placeholder="201000" maxlength="8"> тг</p>
				   </ul>
				  </li>
				  <li class="accordion-item">
				    <input id="s4" class="hide" type="checkbox" >
				    <label for="s4" class="accordion-label">Сезон
						<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M9 18L15 12L9 6" stroke="#AAAABE" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round"/>
						</svg>

				    </label>
				    <ul class="accordion-child">
				      <li>
				        
				        <input id="summer" type="checkbox">
				        <label for="summer">
				        Летний
				        </label>
				      </li>
				      <li>
				        
				        <input id="winter" type="checkbox">
				        <label for="winter">
				        Зимний
				        </label>
				      </li>
				      <li>
				        
				        <input id="demi-s" type="checkbox">
				        <label for="demi-s">
						Демисезонный				        	
				        </label>
				      </li>
				      <li>
				        
				        <input id="all-s" type="checkbox">
				        <label for="all-s">
				       	Всесезонный
				        </label>
				      </li>
				   </ul>
				  </li>
				  <li class="accordion-item">
				    <input id="s5" class="hide" type="checkbox" >
				    <label for="s5" class="accordion-label">Материал
						<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M9 18L15 12L9 6" stroke="#AAAABE" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round"/>
						</svg>

				    </label>
				    <ul class="accordion-child">
				      <li>
				        
				        <input id="acrylic" type="checkbox">
				        <label for="acrylic">
				        Акрил
				        </label>
				      </li>
				      <li>
				        
				        <input id="viscose" type="checkbox">
				        <label for="viscose">
				        Вискоза
				        </label>
				      </li>
				      <li>
				        
				        <input id="cashemire" type="checkbox">
				        <label for="cashemire">
						Кашемир				        	
				        </label>
				      </li>
				      <li>
				        
				        <input id="lycra" type="checkbox">
				        <label for="lycra">
				        Лайкра   
				        </label>
				      </li>
				      <li>
				        
				        <input id="polyamide" type="checkbox">
				        <label for="polyamide">
				        Полиамид   
				        </label>
				      </li>
				      <li>
				        
				        <input id="polyester" type="checkbox"> 
				        <label for="polyester">
				        Полиэстер
				        </label>
				      </li>
				      <li>
				        
				        <input id="wool" type="checkbox">
				        <label for="wool">
				        Шерсть
				        </label>
				      </li>
				      <li>
				        <input id="cotton" type="checkbox"> 
				        <label for="cotton">
				        Хлопок
				        </label>
				      </li>
				   </ul>
				  </li>
				</ul>
			</div>
			<div class="col-sm-12 col-md-9">
				<div class="container products-catalog">
					<div class="row">
						<div class="col-sm-12 col-md-12 mt-3 ml-3 mb-3" >
							<h2>Женская одежда</h2>	
							<div class="multifilters mt-3">
								<span class="products-catalog-sort">
									Сортировать по:
								</span>
								<div class="multifilter">
									<input id="f1" type="checkbox" class="hide" >
									<label for="f1" class="multifilter-title">
										По популярности
									</label>
								</div>
								<div class="multifilter">
									<input id="f2" type="checkbox" class="hide">
									<label for="f2" class="multifilter-title">
										Новинки
									</label>
								</div>
								<div class="multifilter">
									<input id="f3" type="checkbox" class="hide">
									<label for="f3" class="multifilter-title">
										Убывающей цене
									</label>
								</div>
								<div class="multifilter">									
									<input id="f4" type="checkbox" class="hide">
									<label for="f4" class="multifilter-title">
										Возрастающей цене
									</label>
								</div>
								<div class="multifilter">
									<input id="f5" type="checkbox" class="hide">
									<label for="f5" class="multifilter-title">
										Скидки
									</label>
								</div>
							</div>
						</div>
					</div>
					<div class="row align-items-center">
						<div class="col-sm-12 col-md-8">
							<ul class="products-catalog-nav">
								<li><a href="#">Платья</a></li>
								<li><a href="#">Футболки</a></li>
								<li><a href="#">Джинсы и брюки</a></li>
								<li><a href="#">Пиджаки</a></li>
								<li><a href="#">Пальто</a></li>
							</ul>
						</div>
						<div class="col-sm-12 col-md-4">
							<ul class="accordion">
							  <li class="accordion-item">
							    <input id="f6" class="hide" type="checkbox">
							    <label for="f6"class="accordion-label">Показать все
									<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M9 18L15 12L9 6" stroke="#AAAABE" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round"/>
									</svg>	
							    </label>
							    <ul class="accordion-child">
							      <li>
							        <label>
							          <input type="checkbox">
							          <a href="#">Еще</a>
							        </label>
							      </li>
							      <li>
							        <label>
							          <input type="checkbox">
							          <a href="#">Еще</a>
							        </label>
							      </li>
							      <li>
							        <label>
							          <input type="checkbox">
							          <a href="#">Еще</a>
							        </label>
							      </li>
							      <li>
							        <label>
							          <input type="checkbox">
							          <a href="#">Еще</a>
							        </label>
							      </li>
							      <li>
							        <label>
							          <input type="checkbox">
							          <a href="#">Еще</a>
							        </label>
							      </li>
							      <li>
							        <label>
							          <input type="checkbox">
							          <a href="#">Еще</a>
							        </label>
							      </li>
							      <li>
							        <label>
							          <input type="checkbox">
							          <a href="#">Еще</a>
							        </label>
							      </li>
							   </ul>
							  </li>
							</ul>

						</div>
					</div>
				</div>
				<div class="container products-show">
				<div class="cont_tov">
                        <div class="tov_item">
                            <a href="" class="tov_item_img">
                                <img src="./img/tov1.png" alt="">
                            </a>
                            <a href="" class="tov-button">Подробнее</a>
                            <div class="tov_item_info">
                                <div class="tov_new">Новинка!</div>
                                <p>Платье - сарафон, розовое</p>
                                <div class="price">5 630 <span>тг</span></div>
                            </div>
                        </div>

                        <div class="tov_item">
                            <a href="" class="tov_item_img">
                                <img src="./img/tov2.png" alt="">
                            </a>
                            <a href="" class="tov-button">Подробнее</a>
                            <div class="tov_item_info">
                                <p>Футболка, красная</p>
                                <div class="price">7 000 <span>тг</span></div>
                            </div>
                        </div>

                        <div class="tov_item">
                            <a href="" class="tov_item_img">
                                <img src="./img/tov3.png" alt="">
                            </a>
                            <a href="" class="tov-button">Подробнее</a>
                            <div class="tov_item_info">
                                <div class="tov_new tov_pov">Популярное!</div>
                                <p>Укороченные джинсовые шорты</p>
                                <div class="price">1060 <span>тг</span></div>
                            </div>
                        </div>

                        <div class="tov_item">
                            <a href="" class="tov_item_img">
                                <img src="./img/tov4.png" alt="">
                            </a>
                            <a href="" class="tov-button">Подробнее</a>
                            <div class="tov_item_info">
                                <p>Свадебное платье</p>
                                <div class="price">21 060 <span>тг</span></div>
                            </div>
                        </div>

                        <div class="tov_item">
                            <a href="" class="tov_item_img">
                                <img src="./img/tov5.png" alt="">
                            </a>
                            <a href="" class="tov-button">Подробнее</a>
                            <div class="tov_item_info">
                                <p>Блузка голубая</p>
                                <div class="price">3 000 <span>тг</span></div>
                            </div>
                        </div>

                        <div class="tov_item">
                            <a href="" class="tov_item_img">
                                <img src="./img/tov6.png" alt="">
                            </a>
                            <a href="" class="tov-button">Подробнее</a>
                            <div class="tov_item_info">
                                <p>Туника в клетку, фиолетовая</p>
                                <div class="old_price">9 000 <span>тг</span></div>
                                <div class="price">4 990 <span>тг</span></div>
                            </div>
                        </div>

                        <div class="tov_item">
                            <a href="" class="tov_item_img">
                                <img src="./img/tov7.png" alt="">
                            </a>
                            <a href="" class="tov-button">Подробнее</a>
                            <div class="tov_item_info">
                                <p>Длинный трикотажный кардиган, серый</p>
                                <div class="price">8 000 <span>тг</span></div>
                            </div>
                        </div>

                        <div class="tov_item">
                            <a href="" class="tov_item_img">
                                <img src="./img/tov8.png" alt="">
                            </a>
                            <a href="" class="tov-button">Подробнее</a>
                            <div class="tov_item_info">
                                <div class="tov_new tov_exs">Эксклюзив!</div>
                                <p>Блузка оранжевая</p>
                                <div class="price">3 120 <span>тг</span></div>
                            </div>
                        </div>

                        <div class="tov_item">
                            <a href="" class="tov_item_img">
                                <img src="./img/img1.png" alt="">
                            </a>
                            <a href="" class="tov-button">Подробнее</a>
                            <div class="tov_item_info">
                                <div class="tov_new">Новинка!</div>
                                <p>Женское пальто Klimini, бежевое</p>
                                <div class="price">13 000 <span>тг</span></div>
                            </div>
                        </div>

                        <div class="tov_item">
                            <a href="" class="tov_item_img">
                                <img src="./img/img2.png" alt="">
                            </a>
                            <a href="" class="tov-button">Подробнее</a>
                            <div class="tov_item_info">
                                <p>Платье Versace, фиолетовое</p>
                                <div class="price">9 340 <span>тг</span></div>
                            </div>
                        </div>

                        <div class="tov_item">
                            <a href="" class="tov_item_img">
                                <img src="./img/img3.png" alt="">
                            </a>
                            <a href="" class="tov-button">Подробнее</a>
                            <div class="tov_item_info">
                                <p>Платье оранжевое</p>
                                <div class="old_price">56 000 <span>тг</span></div>
                                <div class="price">13 000 <span>тг</span></div>
                            </div>
                        </div>

                        <div class="tov_item">
                            <a href="" class="tov_item_img">
                                <img src="./img/img4.png" alt="">
                            </a>
                            <a href="" class="tov-button">Подробнее</a>
                            <div class="tov_item_info">
                                <div class="tov_new">Новинка!</div>
                                <p>Темно-синий женский жилет</p>
                                <div class="price">7 390 <span>тг</span></div>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
	


<?php
	require_once('footer.php');
?>