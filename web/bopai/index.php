<?php
	require_once('header.php');
?>



    <div class="main-page">
        <section class="grid">
            <div class="wrap">
                <div class="sidebar">
                    <div class="menu">
                        <ul>
                            <li>
                                <a href="">Одежда</a>
                                <img src="./img/arrow-right.png" alt="" class="accordion-trigger">
                                <ul class="sub-menu">
                                    <li>
                                        <a href="">Женская</a>
                                    </li>
                                    <li><a href="">Мужская</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="">Обувь</a>
                                <img src="./img/arrow-right.png" alt="" class="accordion-trigger">
                                <ul class="sub-menu">
                                    <li><a href="">Женская</a></li>
                                    <li><a href="">Мужская</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="">Белье</a>
                                <img src="./img/arrow-right.png" alt="" class="accordion-trigger">
                                <ul class="sub-menu">
                                    <li><a href="">Мужское нижнее</a></li>
                                    <li><a href="">Женское нижнее</a></li>
                                </ul>
                            </li>
                            <li><a href="">Большие размеры </a></li>
                            <li><a href="">Спецодежда</a></li>
                            <li><a href="">Одежда для офиса</a></li>
                            <li><a href="">Одежда для дома</a></li>
                            <li><a href="">Свадебные наряды</a></li>
                            <li><a href="">Будущие мамы</a></li>
                            <li><a href="">Все для пляжа</a></li>
                        </ul>
                    </div>
                    <div class="news">
                        <div class="title">Полезные статьи</div>
                        <div class="news_cont">
                            <div class="news_item">
                                <div class="img">
                                    <img src="./img/foto1.jpg" alt="">
                                </div>
                                <a href="">10 антитрендов 2019 года, или вещи от которых стоит избавиться</a>
                            </div>

                            <div class="news_item">
                                <div class="img">
                                    <img src="./img/foto2.jpg" alt="">
                                </div>
                                <a href="">Как взять в отпуск только необходимо и выглядеть стильно</a>
                            </div>

                            <div class="news_item">
                                <div class="img">
                                    <img src="./img/foto3.jpg" alt="">
                                </div>
                                <a href="">Вне моды и времени: 9 вещей, которые будут актуальны и после вашей смерти</a>
                            </div>
                        </div>
                    </div>
                    <div class="payment">
                        <div class="title">Способы оплаты</div>
                        <div class="subtitle">Мы принимаем патежи от следующих платежных систем</div>
                        <div class="pay_cont">
                            <a href="">
                                <img src="./img/master card.png" alt="">
                            </a>
                            <a href="">
                                <img src="./img/yandex.png" alt="">
                            </a>
                            <a href="">
                                <img src="./img/webmoney.png" alt="">
                            </a>
                            <a href="">
                                <img src="./img/verified visa.png" alt="">
                            </a>
                            <a href="">
                                <img src="./img/visa.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>


                <div class="content">
                    <div class="main_ban_cont">
                        <a href="" class="ban_75">
                            <span>Создай свой образ</span>
                            <p>ОФИЦИАЛЬНАЯ КОЛЛЕКЦИЯ</p>
                        </a>
                        <div class="ban_cont_35">
                            <a href="" class="main_ban_item">
                                <p>Повседневная одежда</p>
                            </a>
                            <a href="" class="main_ban_item">
                                <p>Кроссовки</p>
                                <span>Для спорта</span>
                            </a>
                        </div>
                    </div>
                    <div class="ban_cont">
                        <a href="" class="ban_item">
                            Новинки!
                        </a>
                        <a href="" class="ban_item">
                            Специальные предложения
                        </a>
                        <a href="" class="ban_item">
                            Снова в наличии
                        </a>
                        <a href="" class="ban_item">
                            Хит продаж
                        </a>
                    </div>

                    <div class="slider_title_nav">
                        <div class="title">
                            <p>Выбор покупателей!</p> <a href=""><span>Все популярные товары</span></a>
                        </div>
                        <div class="nav">
                            <div class="arrow-left">
                                <svg width="62" height="62" viewBox="0 0 62 62" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <g filter="url(#filter0_d)">
                                        <rect x="10" y="7" width="42" height="42" rx="21" fill="white" />
                                    </g>
                                    <path d="M33 34L27 28L33 22" stroke="#AAAABE" stroke-width="2"
                                        stroke-linecap="round" stroke-linejoin="round" />
                                    <defs>
                                        <filter id="filter0_d" x="0" y="0" width="62" height="62"
                                            filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                                            <feFlood flood-opacity="0" result="BackgroundImageFix" />
                                            <feColorMatrix in="SourceAlpha" type="matrix"
                                                values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
                                            <feOffset dy="3" />
                                            <feGaussianBlur stdDeviation="5" />
                                            <feColorMatrix type="matrix"
                                                values="0 0 0 0 0 0 0 0 0 0.1 0 0 0 0 1 0 0 0 0.03 0" />
                                            <feBlend mode="normal" in2="BackgroundImageFix"
                                                result="effect1_dropShadow" />
                                            <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow"
                                                result="shape" />
                                        </filter>
                                    </defs>
                                </svg>
                            </div>
                            <div class="arrow-right">
                                <svg width="62" height="62" viewBox="0 0 62 62" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <g filter="url(#filter0_d)">
                                        <rect x="52" y="49" width="42" height="42" rx="21"
                                            transform="rotate(-180 52 49)" fill="white" />
                                    </g>
                                    <path d="M29 22L35 28L29 34" stroke="#AAAABE" stroke-width="2"
                                        stroke-linecap="round" stroke-linejoin="round" />
                                    <defs>
                                        <filter id="filter0_d" x="0" y="0" width="62" height="62"
                                            filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                                            <feFlood flood-opacity="0" result="BackgroundImageFix" />
                                            <feColorMatrix in="SourceAlpha" type="matrix"
                                                values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
                                            <feOffset dy="3" />
                                            <feGaussianBlur stdDeviation="5" />
                                            <feColorMatrix type="matrix"
                                                values="0 0 0 0 0 0 0 0 0 0.1 0 0 0 0 1 0 0 0 0.03 0" />
                                            <feBlend mode="normal" in2="BackgroundImageFix"
                                                result="effect1_dropShadow" />
                                            <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow"
                                                result="shape" />
                                        </filter>
                                    </defs>
                                </svg>

                            </div>
                        </div>
                    </div>

                    <div class="slider_popular_tov">
                        <div class="tov_item">
                            <a href="" class="tov_item_img">
                                <img src="./img/img1.png" alt="">
                            </a>
                            <a href="" class="tov-button">Подробнее</a>
                            <div class="tov_item_info">
                                <div class="tov_new">Новинка!</div>
                                <p>Женское пальто Klimini, бежевое</p>
                                <div class="price">13 000 <span>тг</span></div>
                            </div>
                        </div>

                        <div class="tov_item">
                            <a href="" class="tov_item_img">
                                <img src="./img/img2.png" alt="">
                            </a>
                            <a href="" class="tov-button">Подробнее</a>
                            <div class="tov_item_info">
                                <p>Платье Versace, фиолетовое</p>
                                <div class="price">9 340 <span>тг</span></div>
                            </div>
                        </div>

                        <div class="tov_item">
                            <a href="" class="tov_item_img">
                                <img src="./img/img3.png" alt="">
                            </a>
                            <a href="" class="tov-button">Подробнее</a>
                            <div class="tov_item_info">
                                <p>Платье оранжевое</p>
                                <div class="old_price">56 000 <span>тг</span></div>
                                <div class="price">13 000 <span>тг</span></div>
                            </div>
                        </div>

                        <div class="tov_item">
                            <a href="" class="tov_item_img">
                                <img src="./img/img4.png" alt="">
                            </a>
                            <a href="" class="tov-button">Подробнее</a>
                            <div class="tov_item_info">
                                <div class="tov_new">Новинка!</div>
                                <p>Темно-синий женский жилет</p>
                                <div class="price">7 390 <span>тг</span></div>
                            </div>
                        </div>
                    </div>

                    <div class="title_tov">
                        <div class="title">
                            <p>Выбор покупателей!</p> <a href=""><span>Все популярные товары</span></a>
                        </div>
                    </div>

                    <div class="cont_tov">
                        <div class="tov_item">
                            <a href="" class="tov_item_img">
                                <img src="./img/tov1.png" alt="">
                            </a>
                            <a href="" class="tov-button">Подробнее</a>
                            <div class="tov_item_info">
                                <div class="tov_new">Новинка!</div>
                                <p>Платье - сарафон, розовое</p>
                                <div class="price">5 630 <span>тг</span></div>
                            </div>
                        </div>

                        <div class="tov_item">
                            <a href="" class="tov_item_img">
                                <img src="./img/tov2.png" alt="">
                            </a>
                            <a href="" class="tov-button">Подробнее</a>
                            <div class="tov_item_info">
                                <p>Футболка, красная</p>
                                <div class="price">7 000 <span>тг</span></div>
                            </div>
                        </div>

                        <div class="tov_item">
                            <a href="" class="tov_item_img">
                                <img src="./img/tov3.png" alt="">
                            </a>
                            <a href="" class="tov-button">Подробнее</a>
                            <div class="tov_item_info">
                                <div class="tov_new tov_pov">Популярное!</div>
                                <p>Укороченные джинсовые шорты</p>
                                <div class="price">1060 <span>тг</span></div>
                            </div>
                        </div>

                        <div class="tov_item">
                            <a href="" class="tov_item_img">
                                <img src="./img/tov4.png" alt="">
                            </a>
                            <a href="" class="tov-button">Подробнее</a>
                            <div class="tov_item_info">
                                <p>Свадебное платье</p>
                                <div class="price">21 060 <span>тг</span></div>
                            </div>
                        </div>

                        <div class="tov_item">
                            <a href="" class="tov_item_img">
                                <img src="./img/tov5.png" alt="">
                            </a>
                            <a href="" class="tov-button">Подробнее</a>
                            <div class="tov_item_info">
                                <p>Блузка голубая</p>
                                <div class="price">3 000 <span>тг</span></div>
                            </div>
                        </div>

                        <div class="tov_item">
                            <a href="" class="tov_item_img">
                                <img src="./img/tov6.png" alt="">
                            </a>
                            <a href="" class="tov-button">Подробнее</a>
                            <div class="tov_item_info">
                                <p>Туника в клетку, фиолетовая</p>
                                <div class="old_price">9 000 <span>тг</span></div>
                                <div class="price">4 990 <span>тг</span></div>
                            </div>
                        </div>

                        <div class="tov_item">
                            <a href="" class="tov_item_img">
                                <img src="./img/tov7.png" alt="">
                            </a>
                            <a href="" class="tov-button">Подробнее</a>
                            <div class="tov_item_info">
                                <p>Длинный трикотажный кардиган, серый</p>
                                <div class="price">8 000 <span>тг</span></div>
                            </div>
                        </div>

                        <div class="tov_item">
                            <a href="" class="tov_item_img">
                                <img src="./img/tov8.png" alt="">
                            </a>
                            <a href="" class="tov-button">Подробнее</a>
                            <div class="tov_item_info">
                                <div class="tov_new tov_exs">Эксклюзив!</div>
                                <p>Блузка оранжевая</p>
                                <div class="price">3 120 <span>тг</span></div>
                            </div>
                        </div>

                        <div class="tov_item">
                            <a href="" class="tov_item_img">
                                <img src="./img/img1.png" alt="">
                            </a>
                            <a href="" class="tov-button">Подробнее</a>
                            <div class="tov_item_info">
                                <div class="tov_new">Новинка!</div>
                                <p>Женское пальто Klimini, бежевое</p>
                                <div class="price">13 000 <span>тг</span></div>
                            </div>
                        </div>

                        <div class="tov_item">
                            <a href="" class="tov_item_img">
                                <img src="./img/img2.png" alt="">
                            </a>
                            <a href="" class="tov-button">Подробнее</a>
                            <div class="tov_item_info">
                                <p>Платье Versace, фиолетовое</p>
                                <div class="price">9 340 <span>тг</span></div>
                            </div>
                        </div>

                        <div class="tov_item">
                            <a href="" class="tov_item_img">
                                <img src="./img/img3.png" alt="">
                            </a>
                            <a href="" class="tov-button">Подробнее</a>
                            <div class="tov_item_info">
                                <p>Платье оранжевое</p>
                                <div class="old_price">56 000 <span>тг</span></div>
                                <div class="price">13 000 <span>тг</span></div>
                            </div>
                        </div>

                        <div class="tov_item">
                            <a href="" class="tov_item_img">
                                <img src="./img/img4.png" alt="">
                            </a>
                            <a href="" class="tov-button">Подробнее</a>
                            <div class="tov_item_info">
                                <div class="tov_new">Новинка!</div>
                                <p>Темно-синий женский жилет</p>
                                <div class="price">7 390 <span>тг</span></div>
                            </div>
                        </div>
                    </div>

                    <a href="" class="offer">
                        <div class="subtitle">ТУФЛИ И БАЛЕТКИ</div>
                        <div class="title">ОСЕННЯЯ КОЛЛЕКЦИЯ!</div>
                    </a>
                </div>
            </div>
        </section>
    </div>


    <?php
	require_once('footer.php');
?>	