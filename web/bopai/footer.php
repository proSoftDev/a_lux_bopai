<div class="footer">
		<div class="top-footer">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-5">
						<p class="subscription-article">
							Подписка на новости
							<span>Будьте в курсе всех новостей, скидок и акций</span>
						</p>
					</div>
					<div class="col-sm-12 col-md-7">
						<form action="" class="subscription-form">
							<input type="email" placeholder="Введите свой email">
							<input type="button" value="Подписаться">	
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="bottom-footer pt-5 pb-5">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-9">
						<div class="container">
							<div class="row">
								<div class="col-sm-3 col-md-3 pl-0 pr-0">
									<img src="img/footer_logo.png" alt="Логотип">
									<p class="copyright">
										2004-2019 © bopai.kz — модный интернет-магазин женской одежды, обуви и аксессуаров. Все права защищены.
									</p>
								</div>
								<div class="col-sm-12 col-md-3 tc">
									<h4>Клиентам</h4>
									<ul class="mt-3">
										<li><a href="/about.php">О компании</a></li>
										<li><a href="#">Доставка и оплата</a></li>
										<li><a href="#">Акции и скидки</a></li>
										<li><a href="#">Отзывы на товары</a></li>
									</ul>
								</div>
								<div class="col-sm-12 col-md-3 tc">
									<h4>Помощь</h4>
									<ul class="mt-3">
										<li><a href="#">Адреса магазинов</a></li>
										<li><a href="#">Вопросы и ответы</a></li>
										<li><a href="#">Связаться с нами</a></li>
										<li><a href="#">Возврат товара</a></li>
									</ul>
								</div>
								<div class="col-sm-12 col-md-3 tc">
									<h4>Кабинет</h4>
									<ul class="mt-3">
										<li><a href="/personal.php">Мой профиль</a></li>
										<li><a href="/basket.php">Корзина</a></li>
										<li><a href="#">Избранные товары</a></li>
										<li><a href="/history.php">Мои покупки</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12 col-md-3 social-btns">
						<a href="https://instagram.com">
							<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M5.66667 0.25C2.67558 0.25 0.25 2.67558 0.25 5.66667V14.3333C0.25 17.3244 2.67558 19.75 5.66667 19.75H14.3333C17.3244 19.75 19.75 17.3244 19.75 14.3333V5.66667C19.75 2.67558 17.3244 0.25 14.3333 0.25H5.66667ZM16.5 2.41667C17.098 2.41667 17.5833 2.902 17.5833 3.5C17.5833 4.098 17.098 4.58333 16.5 4.58333C15.902 4.58333 15.4167 4.098 15.4167 3.5C15.4167 2.902 15.902 2.41667 16.5 2.41667ZM10 4.58333C12.9911 4.58333 15.4167 7.00892 15.4167 10C15.4167 12.9911 12.9911 15.4167 10 15.4167C7.00892 15.4167 4.58333 12.9911 4.58333 10C4.58333 7.00892 7.00892 4.58333 10 4.58333ZM10 6.75C9.13805 6.75 8.3114 7.09241 7.7019 7.7019C7.09241 8.3114 6.75 9.13805 6.75 10C6.75 10.862 7.09241 11.6886 7.7019 12.2981C8.3114 12.9076 9.13805 13.25 10 13.25C10.862 13.25 11.6886 12.9076 12.2981 12.2981C12.9076 11.6886 13.25 10.862 13.25 10C13.25 9.13805 12.9076 8.3114 12.2981 7.7019C11.6886 7.09241 10.862 6.75 10 6.75Z" fill="white"/>
							</svg>

							instagram
						</a>
						<a href="https://telegram.com">
							<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
							<g clip-path="url(#clip0)">
							<path d="M19.925 3.15843L16.9167 17.3668C16.7083 18.3751 16.1 18.6168 15.25 18.1501L10.6667 14.7584L8.44999 16.9001C8.19999 17.1501 7.99165 17.3668 7.53332 17.3668C6.93332 17.3668 7.03332 17.1418 6.83332 16.5751L5.24999 11.4168L0.708321 10.0001C-0.275012 9.70843 -0.283345 9.03343 0.924988 8.54176L18.6417 1.70843C19.45 1.3501 20.225 1.90843 19.9167 3.1501L19.925 3.15843Z" fill="white"/>
							<line x1="2.79693" y1="12.5431" x2="11.7969" y2="8.54309" stroke="#2CA5E0"/>
							</g>
							<defs>
							<clipPath id="clip0">
							<rect width="20" height="20" fill="white"/>
							</clipPath>
							</defs>
							</svg>


							telegram
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script> 
	<script type="text/javascript" src="slick/slick.min.js"></script>  
	<script src="hc-offcanvas-nav-master/dist/hc-offcanvas-nav.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="./libs/owlcarousel/owl.carousel.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/@glidejs/glide"></script>
	<script src="./js/bootstrap-datetimepicker.min.js"></script>
    <script src="./js/main.js"></script>
	<script type="text/javascript">
		$(".form_datetime").datetimepicker({
			format: "dd MM yyyy hh:ii",
			autoclose: true,
			todayBtn: true,
			pickerPosition: "bottom-left"
		});
	</script>            
</body>
</html>