<?php
	require_once('header2.php');
?>
<div class="detailed">
    <div class="background-slider">
        <div class="glide_header">
            <div class="glide__track" data-glide-el="track">
                <ul class="glide__slides">
                    <li class="glide__slide" style="background-image: url('img/detailed-bg_01.png')">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <p>6 февраля 2019</p>
                                    <h1>Кофе, лед и еще 8 хитростей для стирки</h1>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="glide__slide" style="background-image: url('img/detailed-bg_02.png')">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <p>6 февраля 2019</p>
                                    <h1>Кофе, лед и еще 8 хитростей для стирки</h1>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="detailed__container">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ac enim eget nibh elementum pharetra. Donec eu dolor sem. Praesent nibh lectus, sollicitudin a hendrerit eget, porta sit amet libero. Maecenas sollicitudin mauris nec ipsum viverra, eu porta libero dictum. Nunc nec lobortis turpis, ac interdum elit. Vestibulum arcu odio, malesuada in dictum tristique, ultrices ac tortor. Mauris suscipit suscipit justo, ac tempus lacus pretium a. Nulla at suscipit lorem, eu pretium nunc.
                    </p>
                    <h2>H2 заголовок</h2>
                    <p>
                    In viverra malesuada malesuada. Phasellus aliquam nisi eros, sit amet venenatis nunc venenatis ac. Ut ultricies ligula ac sapien pretium, vitae sollicitudin turpis auctor. Quisque rutrum rhoncus tortor, a porta nisl tincidunt eu. Nam vel risus egestas, cursus mauris sit amet, lacinia nunc. Aliquam gravida feugiat risus. Cras aliquam dui non augue tempus, vitae venenatis quam dignissim. Aliquam lacus nibh, commodo ut nulla ac, volutpat egestas mauris. Aenean in tincidunt augue. Morbi fringilla neque neque. Sed blandit fringilla ullamcorper. Nulla suscipit rutrum dolor ac sollicitudin. Ut sed leo faucibus, varius lorem vel, fringilla libero. Morbi dolor nibh, lacinia eu ullamcorper vitae, vehicula ut nulla. Donec consequat luctus metus, sed convallis leo. Sed euismod consectetur vehicula.
                    </p>
                    <h3>H3 заголовок</h3>
                    <p>
                    Aenean posuere ac est a mollis. Nulla volutpat venenatis enim mollis pellentesque. Duis non pharetra lectus. In venenatis enim vel pellentesque aliquet. Nam vehicula lectus velit, eget posuere libero pulvinar vitae. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nulla ac sem felis:
                    </p>
                    <div class="figure">
                        <img src="img/detailed-img.png" alt="">
                        <span>Подсказка под любой фотографией в статье</span>
                    </div>
                    <p>
                    In viverra malesuada malesuada. Phasellus aliquam nisi eros, sit amet venenatis nunc venenatis ac. Ut ultricies ligula ac sapien pretium, vitae sollicitudin turpis auctor. Quisque rutrum rhoncus tortor, a porta nisl tincidunt eu. Nam vel risus egestas, cursus mauris sit amet, lacinia nunc. Aliquam gravida feugiat risus. Cras aliquam dui non augue tempus, vitae venenatis quam dignissim. Aliquam lacus nibh, commodo ut nulla ac, volutpat egestas mauris. Aenean in tincidunt augue. Morbi fringilla neque neque. Sed blandit fringilla ullamcorper. Nulla suscipit rutrum dolor ac sollicitudin. Ut sed leo faucibus, varius lorem vel, fringilla libero. Morbi dolor nibh, lacinia eu ullamcorper vitae, vehicula ut nulla. Donec consequat luctus metus, sed convallis leo. Sed euismod consectetur vehicula.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
	require_once('footer.php');
?>