<?php
	require_once('header2.php');
?>
	<div class="personal">
		<div class="container mt-5 mb-5">
			<div class="row">
				<div class="col-sm-12 col-md-3">
					<nav class="switch-nav">
						<ul>
							<li class="switch-nav_active"><a href="#">Личные данные</a></li>
							<li><a href="#">Избранное</a></li>
							<li><a href="/history.php">Истории заказов</a></li>
						</ul>
					</nav>
				</div>
				<div class="col-sm-12 col-md-1"></div>
				<div class="col-sm-12 col-md-5 personal-form">
				  <div class="private-data">
					<form action="" method="">
						<input type="text" placeholder="Имя">
						<input type="text" placeholder="Фамилия">
						<input type="text" placeholder="Отчество">
						<div class="separator mt-3 mb-3"></div>
						<input type="tel" pattern="[8]{1}[0-9]{3}[0-9]{3}[0-9]{2}[0-9]{2}" placeholder="Номер телефона">
						<input type="email" placeholder="E-mail адрес">
						<input type="text" placeholder="Ваш фактический адрес">
						<div class="save">
							<input type="button" value="Сохранить">
							<p>Нажимая кнопку «Сохранить» вы принимаете условия <a href="#">публичной оферты</a></p>
						</div>
					</form>
				  </div>
				</div>
				<div class="col-sm-12 col-md-3 void"></div>
			</div>
		</div>
	</div>
<?php
	require_once('footer.php');
?>	