<?php
	require_once('header2.php');
?>
<div class="question">
	<div class="container mt-5 mb-5">
        <div class="row">
            <div class="col-sm-12 col-md-3">
                <nav class="switch-nav">
                    <ul>
                        <li><a href="#">Контакты</a></li>
                        <li><a href="#">Адреса магазинов</a></li>
                        <li class="switch-nav_active"><a href="#">Частые вопросы</a></li>
                        <li><a href="#">Возврат товара</a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-sm-12 col-md-1"></div>
            <div class="col-sm-12 col-md-8">
                <div class="question__container container">
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<h1>Частозадаваемые вопросы</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<div class="dropdown">
								<input type="checkbox">
								<a href="#">Способы оплаты заказа</a>
								<div class="dropdown-content">
									<p>Способы оплаты заказа</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<div class="dropdown">
								<input type="checkbox">
								<a href="#">Правила возврата товара</a>
								<div class="dropdown-content">
									<p>Правила возврата товара</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<div class="dropdown">
								<input type="checkbox">
								<a href="#">Как зарегистрироваться</a>
								<div class="dropdown-content">
									<p>Как зарегистрироваться</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<div class="dropdown">
								<input type="checkbox">
								<a href="#">Как оформить заказ на сайте</a>
								<div class="dropdown-content">
									<p>Как оформить заказ на сайте</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<div class="dropdown">
								<input type="checkbox">
								<a href="#">Изменение номера телефона</a>
								<div class="dropdown-content">
									<p>Изменение номера телефона</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<div class="dropdown">
								<input type="checkbox">
								<a href="#">Скидка постоянного покупателя</a>
								<div class="dropdown-content">
									<p>Скидка постоянного покупателя</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<div class="dropdown">
								<input type="checkbox">
								<a href="#">Можно ли оплатить заказ "Бонусами спасибо"</a>
								<div class="dropdown-content">
									<p>К сожалению, в интернет-магазине Bopai невозможна оплата заказа бонусами «Спасибо» от Сбербанка.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	require_once('footer.php');
?>