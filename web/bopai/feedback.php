<?php
	require_once('header2.php');
?>
<div class="feedback">
    <div class="container mt-4">
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<p class="main-page-link"><a href="#">Главная</a> <img src="img/_.png"> Обратная связь</p>
			</div>
		</div>
	</div>
    <div class="container mt-5 pb-5">
        <div class="feedback__container ">
            <h1>Обратная связь</h1>
            <p>
                Вы можете заказать обратный звонок, наш оператор свяжется с вами и ответит на все интересующие Вас вопросы
            </p>
            <form action="" method="">
                <input type="text" placeholder="Имя">
                <input type="tel" pattern="[8]{1}[0-9]{3}[0-9]{3}[0-9]{2}[0-9]{2}" placeholder="Номер телефона">
                <div class="input-append date form_datetime">
                    <input size="16" type="text" placeholder="Дата">
                    <span class="add-on">
                    
                    <i class="icon-th"></i></span>
                </div>
                            
                <input type="button" value="Заказть звонок">
            </form>
        </div>
    </div>
</div>
<?php
	require_once('footer.php');
?>