<?php
	require_once('header2.php');
?>
    <div class="contact">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-3">
					<nav class="switch-nav">
						<ul>
							<li class="switch-nav_active"><a href="#">Контакты</a></li>
							<li><a href="#">Адреса магазинов</a></li>
							<li><a href="#">Частые вопросы</a></li>
                            <li><a href="#">Возврат товара</a></li>
						</ul>
					</nav>
                </div>
                <div class="col-sm-12 col-md-1"></div>
				<div class="col-sm-12 col-md-8">
                    <h1>Контактная информация</h1>
                    <p>По всем вопросам и проблемам в работе с магазином Bopai</p>
                    <div class="container p-0 mt-5">
                      <div class="row">
                        <div class="col-md-4">
                            <div class="contact__info-block">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" 
                                viewBox="0 0 35 35" style="enable-background:new 0 0 35 35;" xml:space="preserve">
                                <g>
	                            <path d="M25.302,0H9.698c-1.3,0-2.364,1.063-2.364,2.364v30.271C7.334,33.936,8.398,35,9.698,35h15.604
		                        c1.3,0,2.364-1.062,2.364-2.364V2.364C27.666,1.063,26.602,0,25.302,0z M15.004,1.704h4.992c0.158,0,0.286,0.128,0.286,0.287
		                        c0,0.158-0.128,0.286-0.286,0.286h-4.992c-0.158,0-0.286-0.128-0.286-0.286C14.718,1.832,14.846,1.704,15.004,1.704z M17.5,33.818
		                        c-0.653,0-1.182-0.529-1.182-1.183s0.529-1.182,1.182-1.182s1.182,0.528,1.182,1.182S18.153,33.818,17.5,33.818z M26.021,30.625
		                        H8.979V3.749h17.042V30.625z"/>
                                </g>
                                </svg>
                                <div>
                                    <h2>8 775 1333 038</h2>
                                    <p>Номер телефона (круглосуточно)</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="contact__info-block">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	                            viewBox="0 0 54.757 54.757" style="enable-background:new 0 0 54.757 54.757;" xml:space="preserve">
                                <g>
	                            <path d="M27.557,12c-3.859,0-7,3.141-7,7s3.141,7,7,7s7-3.141,7-7S31.416,12,27.557,12z M27.557,24c-2.757,0-5-2.243-5-5
		                        s2.243-5,5-5s5,2.243,5,5S30.314,24,27.557,24z"/>
	                            <path d="M40.94,5.617C37.318,1.995,32.502,0,27.38,0c-5.123,0-9.938,1.995-13.56,5.617c-6.703,6.702-7.536,19.312-1.804,26.952
		                        L27.38,54.757L42.721,32.6C48.476,24.929,47.643,12.319,40.94,5.617z M41.099,31.431L27.38,51.243L13.639,31.4
		                        C8.44,24.468,9.185,13.08,15.235,7.031C18.479,3.787,22.792,2,27.38,2s8.901,1.787,12.146,5.031
		                        C45.576,13.08,46.321,24.468,41.099,31.431z"/>
                                </g>
                                </svg>
                                <div>
                                    <h2>Адреса магазина</h2>
                                    <p>Нур-Султан: ТРЦ "Большой"</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="contact__info-block">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	                            viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                <g>
    	                            <g>
	    	                            <path d="M467,61H45C20.218,61,0,81.196,0,106v300c0,24.72,20.128,45,45,45h422c24.72,0,45-20.128,45-45V106
		    	                        C512,81.28,491.872,61,467,61z M460.786,91L256.954,294.833L51.359,91H460.786z M30,399.788V112.069l144.479,143.24L30,399.788z
			                            M51.213,421l144.57-144.57l50.657,50.222c5.864,5.814,15.327,5.795,21.167-0.046L317,277.213L460.787,421H51.213z M482,399.787
			                            L338.213,256L482,112.212V399.787z"/>
	                                </g>
                                </g>
                                </svg>
                                <div>
                                    <a href="mailto:sales@bopai.kz">sales@bopai.kz</a>
                                    <p>Электронная почта для обращений</p>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                    <div class="line mt-5 mb-5"></div>
                    <div class="container p-0">
                      <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <div class="contact__info-block">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	                            viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                <g>
                                	<g>
		                                <path d="M352,0H160C71.648,0,0,71.648,0,160v192c0,88.352,71.648,160,160,160h192c88.352,0,160-71.648,160-160V160
			                            C512,71.648,440.352,0,352,0z M464,352c0,61.76-50.24,112-112,112H160c-61.76,0-112-50.24-112-112V160C48,98.24,98.24,48,160,48
			                            h192c61.76,0,112,50.24,112,112V352z"/>
	                                </g>
                                </g>
                                <g>
	                                <g>
		                                <path d="M256,128c-70.688,0-128,57.312-128,128s57.312,128,128,128s128-57.312,128-128S326.688,128,256,128z M256,336
			                            c-44.096,0-80-35.904-80-80c0-44.128,35.904-80,80-80s80,35.872,80,80C336,300.096,300.096,336,256,336z"/>
	                                </g>
                                </g>
                                <g>
	                                <g>
		                                <circle cx="393.6" cy="118.4" r="17.056"/>
	                                </g>
                                </g>
                                </svg>
                                <div>
                                    <h3>Нур-Султан</h2>
                                    <p>Инстаграм <a href="instagram.com/bopai.astana">@bopai.astana</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="contact__info-block">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	                            viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                <g>
                                	<g>
		                                <path d="M352,0H160C71.648,0,0,71.648,0,160v192c0,88.352,71.648,160,160,160h192c88.352,0,160-71.648,160-160V160
			                            C512,71.648,440.352,0,352,0z M464,352c0,61.76-50.24,112-112,112H160c-61.76,0-112-50.24-112-112V160C48,98.24,98.24,48,160,48
			                            h192c61.76,0,112,50.24,112,112V352z"/>
	                                </g>
                                </g>
                                <g>
	                                <g>
		                                <path d="M256,128c-70.688,0-128,57.312-128,128s57.312,128,128,128s128-57.312,128-128S326.688,128,256,128z M256,336
			                            c-44.096,0-80-35.904-80-80c0-44.128,35.904-80,80-80s80,35.872,80,80C336,300.096,300.096,336,256,336z"/>
	                                </g>
                                </g>
                                <g>
	                                <g>
		                                <circle cx="393.6" cy="118.4" r="17.056"/>
	                                </g>
                                </g>
                                </svg>
                                <div>
                                    <h3>Алматы</h2>
                                    <p>Инстаграм <a href="https://instagram.com/bopai.kz">@bopai.kz</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="contact__info-block">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	                            viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                <g>
                                	<g>
		                                <path d="M352,0H160C71.648,0,0,71.648,0,160v192c0,88.352,71.648,160,160,160h192c88.352,0,160-71.648,160-160V160
			                            C512,71.648,440.352,0,352,0z M464,352c0,61.76-50.24,112-112,112H160c-61.76,0-112-50.24-112-112V160C48,98.24,98.24,48,160,48
			                            h192c61.76,0,112,50.24,112,112V352z"/>
	                                </g>
                                </g>
                                <g>
	                                <g>
		                                <path d="M256,128c-70.688,0-128,57.312-128,128s57.312,128,128,128s128-57.312,128-128S326.688,128,256,128z M256,336
			                            c-44.096,0-80-35.904-80-80c0-44.128,35.904-80,80-80s80,35.872,80,80C336,300.096,300.096,336,256,336z"/>
	                                </g>
                                </g>
                                <g>
	                                <g>
		                                <circle cx="393.6" cy="118.4" r="17.056"/>
	                                </g>
                                </g>
                                </svg>
                                <div>
                                    <h3>По жалобам</h2>
                                    <p>Инстаграм <a href="https://instagram.com/bakemarketer">@bakemarketer</a></p>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                    </div>
				</div>
			</div>
		</div>
	</div>

<?php
	require_once('footer.php');
?>	