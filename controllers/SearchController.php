<?php
/**
 * Created by PhpStorm.
 * User: Юрий
 * Date: 28.08.2018
 * Time: 9:34
 */


namespace app\controllers;


use app\models\About;
use app\models\Catalog;
use app\models\Faq;
use app\models\Menu;
use app\models\News;
use app\models\Product;
use Yii;
use yii\web\Controller;

class SearchController extends FrontendController
{


    public function actionIndex($keyword)
    {
        $this->setMeta('Поиск', "", "");
		if($keyword){
            $result = Product::getSearchResult($keyword);
			$count = count($result);
        }else{
		    $count = 0;
			return $this->render('result', compact('count', 'text','keyword'));
		}

        return $this->render('result', compact('result','count','keyword'));
    }
}