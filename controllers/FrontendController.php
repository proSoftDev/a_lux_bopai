<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 01.04.2019
 * Time: 11:45
 */

namespace app\controllers;



use app\models\BlockForProduct;
use app\models\Catalog;
use app\models\Copyright;
use app\models\DeliveryPrice;
use app\models\Emailforrequest;
use app\models\LoginForm;
use app\models\Menu;

use app\models\Product;
use app\models\SocialNetwork;
use app\models\UserFavorite;
use Yii;
use yii\web\Controller;

class FrontendController extends Controller
{

    public function init()
    {
        
        $this->setLang();
        $menu = Menu::getHeaderMenu();
        $catalog = Catalog::getCatalog();
        $socialNetwork = SocialNetwork::getAll();
        $copyright = Copyright::getCopyright();
        $product = Product::getAll();
        $block = BlockForProduct::getAll();
        $delivery = DeliveryPrice::getAll();

        Yii::$app->view->params['headerMenu'] = $menu;
        Yii::$app->view->params['catalog'] = $catalog;
        Yii::$app->view->params['social-network'] = $socialNetwork;
        Yii::$app->view->params['copyright'] = $copyright;
        Yii::$app->view->params['product'] = $product;
        Yii::$app->view->params['block'] = $block;
        Yii::$app->view->params['delivery-address'] = $delivery;

        $main = Menu::getModel('/');
        $about = Menu::getModel('/about');
        $delivery = Menu::getModel('/payment-and-delivery');
        $address = Menu::getModel('/address');
        $faq = Menu::getModel('/faq');
        $return = Menu::getModel('/product-return');
        $feedback = Menu::getModel("/feedback");
        $news = Menu::getModel("/news");
        $article = Menu::getModel("/article");
        $profile = Menu::getModel("/account");
        $basket = Menu::getModel("/card");
        $contact = Menu::getModel("/contact");

        Yii::$app->view->params['main'] = $main;
        Yii::$app->view->params['footerMenu'][0] = $about;
        Yii::$app->view->params['footerMenu'][1] = $delivery;
        Yii::$app->view->params['footerMenu'][2] = $address;
        Yii::$app->view->params['footerMenu'][3] = $faq;
        Yii::$app->view->params['footerMenu'][4] = $return;
        Yii::$app->view->params['footerMenu'][5] = $feedback;
        Yii::$app->view->params['footerMenu'][6] = $news;
        Yii::$app->view->params['footerMenu'][7] = $article;
        Yii::$app->view->params['footerMenu'][8] = $profile;
        Yii::$app->view->params['footerMenu'][9] = $basket;
        Yii::$app->view->params['footerMenu'][10] = $contact;


        if(!isset($_SESSION['basket'])){
            session_start();
            $_SESSION['basket'] = array();
        }

        if(!Yii::$app->user->isGuest){
            Yii::$app->view->params['favorites'] = UserFavorite::getAll();
        }

        parent::init();

    }



    protected function setMeta($title = null, $keywords = null, $description = null){
        $this->view->title = $title;
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => "$keywords"]);
        $this->view->registerMetaTag(['name' => 'description', 'content' => "$description"]);
    }

    private function setLang(){
        if(Yii::$app->session['lang'] == '_kz'){
            Yii::$app->language = 'kz';
        }
    }




}