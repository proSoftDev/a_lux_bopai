<?php

namespace app\controllers;



use app\models\About;
use app\models\AboutPayment;
use app\models\Catalog;
use app\models\Faq;
use app\models\LoginForm;
use app\models\Menu;


use app\models\News;
use app\models\Product;
use app\models\SignupForm;
use app\models\Subscription;
use app\models\User;
use app\models\UserProfile;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;


class SiteController extends FrontendController
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
                'foreColor' => 0xffffff,
                'offset' => 3,
                'backColor' =>  0x4a5e68,
                'fontFile'  => '@app/web/fonts/Century Gothic.ttf',
            ],
        ];
    }


    public function actionIndex()
    {
        $model = Menu::getModel("/");
        $this->setMeta($model->metaName, $model->metaKey, $model->metaDesc);

        $catalog = Catalog::getCatalog();
        $popular = Product::getPopular();
        $clothes = Catalog::getProducts($catalog[0]->id);
        $article = News::getMainArticles();
        $news = News::getMainNews();

        return $this->render('index',compact('catalog','popular','clothes','article','news'));
    }



    public function actionNewSubscribe(){
       $subscribe = new Subscription();
        if ($subscribe->load(Yii::$app->request->post()) && $subscribe->validate()) {
            $subscribe->save(false);
        }else{
            $error = "";
            $errors = $subscribe->getErrors();
            foreach($errors as $v){
                $error .= $v[0];
                break;
            }
            return $error;
        }
        return 1;
    }



    public function actionLogin(){

        $model = new LoginForm();
        $model->scenario = LoginForm::site;
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->login()) {
                return 1;
            }else{
                $error = "";
                $errors = $model->getErrors();

                foreach($errors as $v){
                    $error .= $v[0];
                    break;
                }
                return $error;
            }
        }
    }

    public function actionLogoff(){
        Yii::$app->user->logout();
        return $this->goHome();
    }




    public function actionRegister()
    {

        $model = new SignupForm();
        $profile = new UserProfile();

        if ($model->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post()) && $profile->validate()) {

            $password = $model->password;
            if ($user = $model->signUp()) {
                $profile->user_id = $user->id;
                if($profile->save()){
                    $this->sendInformationAboutRegistration($profile->name, $user->email, $password);
                    Yii::$app->getUser()->login($user);
                    return 1;
                }
            }else{
                $user_error = "";
                $user_errors = $model->getErrors();
                foreach($user_errors as $v){
                    $user_error .= $v[0];
                    break;
                }
                return $user_error;
            }
        }else{

            $profile_error = "";
            $profile_errors = $profile->getErrors();
            foreach($profile_errors as $v){
                $profile_error .= $v[0];
                break;
            }

            return $profile_error;

        }
    }






    private function sendInformationAboutRegistration($fio, $email, $password) {

        $host = Yii::$app->request->hostInfo;

        $emailSend = Yii::$app->mailer->compose()
            ->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->name . ' robot'])
            ->setTo($email)
            ->setSubject('Регистрация на сайте компании «Bopai»')

            ->setHtmlBody("<p>$fio, вы получили данное письмо, т.к. зарегистрировались на сайте компании «Bopai».</p> </br>
                                 </br>
                                 <p>Ваши данные для авторизации:</p> </br></br>
                                 <p>E-mail: $email</p>
                                 <p>Пароль: $password</p> </br></br>
                                 <p>---</p></br>
                                 <p>С уважением</p></br>
                                 <p>администрация <a href='$host'>Bopai.kz</a></p>");

        return $emailSend->send();

    }







}
