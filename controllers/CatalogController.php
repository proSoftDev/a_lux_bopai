<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 09.10.2019
 * Time: 17:48
 */

namespace app\controllers;


use app\models\BlockForProduct;
use app\models\BlockProduct;
use app\models\Catalog;
use app\models\Color;
use app\models\Product;
use app\models\Size;
use yii\web\NotFoundHttpException;

class CatalogController extends FrontendController
{
    public function actionIndex($id){

        $catalog = Catalog::findOne($id);
        if(!$catalog){
            throw new NotFoundHttpException();
        }
        $this->setMeta($catalog->name,"","");
        $breadcrumbs = Catalog::getBreadcrums($id);
        $products = Catalog::getProducts($id);
        $sizes = Size::getAllByCatalog($catalog->lastParent->id);
        $color = Color::getAll();


        return $this->render('index',compact('catalog','products','sizes','color','breadcrumbs'));
    }


    public function actionPopular(){

        $title = "Популярное товары";
        $type = 'popular';
        $this->setMeta($title,"","");
        $sizes = Size::getAll();
        $color = Color::getAll();
        $products = Product::getPopular();

        return $this->render('byStatus',compact('products','sizes','color','title','type'));

    }



    public function actionNews(){

        $title = "Новинки";
        $type = 'news';
        $this->setMeta($title,"","");
        $sizes = Size::getAll();
        $color = Color::getAll();
        $products = Product::getNew();

        return $this->render('byStatus',compact('products','sizes','color','title','type'));

    }



    public function actionBackInStock(){

        $title = "Снова в наличие";
        $type = 'backInStock';
        $this->setMeta($title,"","");
        $sizes = Size::getAll();
        $color = Color::getAll();
        $products = Product::getBackInStock();

        return $this->render('byStatus',compact('products','sizes','color','title','type'));

    }


    public function actionHit(){

        $title = "Хит продаж";
        $type = 'hit';
        $this->setMeta($title,"","");
        $sizes = Size::getAll();
        $color = Color::getAll();
        $products = Product::getHit();

        return $this->render('byStatus',compact('products','sizes','color','title','type'));

    }


    public function actionBlock($id){

        $block = BlockForProduct::findOne(['id' => $id]);
        $title = $block->metaName;
        if(!$block) throw new NotFoundHttpException();
        $this->setMeta($block->metaName,$block->metaKey, $block->metaDesc);
        $sizes = Size::getAll();
        $color = Color::getAll();
        $products =$block->products;

        return $this->render('byBlock',compact('products','sizes','color','title','type','block'));
    }



    public function actionGetProductByFilterStatus(){

        // GET BY STATUS
        $sql = "AND (";
        $check = 0;
        if($_GET['statusPopular']) {
            $check = 1;
            $sql .= "isPopular = 1";
        }

        if($_GET['statusNew']) {
            if ($check) $sql .= " OR isNew = 1";
            else $sql .= "isNew = 1";
            $check = 1;
        }

        if($_GET['statusDiscount']) {
            if ($check) $sql .= " OR newPrice > 0";
            else $sql .= "newPrice > 0";
            $check = 1;
        }
        $sql .= ")";
        if(!$check){
            $sql = "";
        }
        // END GET BY STATUS


        // GET BY FROM AND TO PRICE
        $check = 0;
        if($_GET['fromPrice']) {
            $sql .= " AND (price >= ".$_GET['fromPrice'];
            $check = 1;
        }

        if($_GET['toPrice']) {
            if ($check) $sql .= " AND price <= ".$_GET['toPrice'];
            else $sql .= " AND (price <= ".$_GET['toPrice'];
            $check = 1;
        }

        if($check) $sql .= ")";
        // END GET BY FROM AND TO PRICE



        // COLORS
        if($_GET['colors'] && $_GET['colors'] != null){
            $colors = ' AND (product.id = product_quantity.product_id AND product_quantity.color_id in ('.$_GET['colors'].'))';
        }
        // END COLORS


        // SIZES
        if($_GET['sizes'] && $_GET['sizes'] != null){
            $sizes = ' AND (product.id = product_quantity.product_id AND product_quantity.size_id in ('.$_GET['sizes'].'))';
        }
        // END SIZES



        // ORDER BY
        $sort = "";
        if($_GET['statusSort']) {
            if ($_GET['priceDecrease']) {
               $sort .= "price DESC";
            }elseif ($_GET['priceIncrease']){
               $sort .= "price ASC";
            }
        }
        // END ORDER BY


        $products = Catalog::getProducts($_GET['catalog_id'], $sql, $sort, $colors, $sizes);
        return $this->renderAjax('filter',compact('products'));
    }


    public function actionGetProductByType(){


        if($_GET['type'] == 'popular')  $type = "isPopular = 1";
        elseif($_GET['type'] == 'news')  $type = "isNew = 1";
        elseif($_GET['type'] == 'backInStock')  $type = "isBackInStock = 1";
        elseif($_GET['type'] == 'hit')  $type = "isHit = 1";
        else $type = "";


        if($_GET['block_id'] != null) {
            $block_product = BlockProduct::findAll(['block_id' => $_GET['block_id']]);
            foreach ($block_product as $v){
                $arr[] = $v->product_id;
            }
            if($arr != null) $type = 'product_id in (' . implode(',', $arr) . ')';
        }



        // GET BY STATUS
        $sql = '(';
        $check = 0;
        if($_GET['statusPopular']) {
            $check = 1;
            $sql .= "isPopular = 1";
        }
        if($_GET['statusNew']) {
            if ($check) $sql .= " OR isNew = 1";
            else $sql .= "isNew = 1";
            $check = 1;
        }
        if($_GET['statusDiscount']) {
            if ($check) $sql .= " OR newPrice > 0";
            else $sql .= "newPrice > 0";
            $check = 1;
        }
        $sql .= ")";
        if(!$check){
            $sql = $type;
        }else{
            $sql = $type.' AND '.$sql;
        }
        // END GET BY STATUS


        // GET BY FROM AND TO PRICE
        $check = 0;
        if($_GET['fromPrice']) {
            $sql .= " AND (price >= ".$_GET['fromPrice'];
            $check = 1;
        }
        if($_GET['toPrice']) {
            if ($check) $sql .= " AND price <= ".$_GET['toPrice'];
            else $sql .= " AND (price <= ".$_GET['toPrice'];
            $check = 1;
        }
        if($check) $sql .= ")";
        // END GET BY FROM AND TO PRICE



        // COLORS
        if($_GET['colors'] && $_GET['colors'] != null) $colors = ' AND (product.id = product_quantity.product_id AND product_quantity.color_id in ('.$_GET['colors'].'))';

        // END COLORS


        // SIZES
        if($_GET['sizes'] && $_GET['sizes'] != null) $sizes = ' AND (product.id = product_quantity.product_id AND product_quantity.size_id in ('.$_GET['sizes'].'))';
        // END SIZES



        // ORDER BY
        $sort = "";
        if($_GET['statusSort']) {
            if ($_GET['priceDecrease']) {
                $sort .= "price DESC";
            }elseif ($_GET['priceIncrease']){
                $sort .= "price ASC";
            }
        }
        // END ORDER BY

        $products = Catalog::getProducts($_GET['catalog_id'], $sql, $sort, $colors, $sizes);
        return $this->renderAjax('filter',compact('products'));
    }
}